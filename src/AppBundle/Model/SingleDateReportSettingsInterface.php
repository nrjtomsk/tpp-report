<?php

namespace AppBundle\Model;

interface SingleDateReportSettingsInterface extends BaseReportSettingsInterface
{
    /**
     * Getting date of report (for reports with interval fixed by month)
     *
     * @return \DateTime
     */
    public function getDate();
}