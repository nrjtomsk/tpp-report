<?php

namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints;

class RksEnergoCorporateUsage implements SingleDateReportSettingsInterface
{
    /**
     * @var string
     * @Constraints\NotBlank()
     */
    protected $title;

    /**
     * @var \DateTime
     * @Constraints\NotBlank()
     * @Constraints\DateTime()
     */
    protected $date;

    public function __construct(array $settings)
    {
        $this->title = array_key_exists('title', $settings) ? $settings['title'] : null;

        $this->date = array_key_exists('date', $settings)
            ? \DateTime::createFromFormat(DATE_ISO8601, $settings['date'])
            : null;
        $errors = \DateTime::getLastErrors();
        if ((0 !== $errors['warning_count']) || (0 !== $errors['error_count'])) {
            $this->date = null;
        }
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @return int
     */
    public function getDetailType(): int
    {
        return ReportSettingsInterface::DETAIL_HALF_HOUR;
    }

    /**
     * @return int
     */
    public function getViewType(): int
    {
        return ReportSettingsInterface::VIEW_TYPE_BY_CONSUMPTION;
    }
}