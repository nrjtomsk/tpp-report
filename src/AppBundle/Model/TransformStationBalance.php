<?php

namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints;

class TransformStationBalance implements ReportSettingsInterface
{
    /**
     * @var string
     * @Constraints\NotBlank()
     */
    protected $title;

    /**
     * @var int
     * @Constraints\Choice(
     *     choices={1, 3, 5}
     * )
     */
    protected $detailType;

    /**
     * @var string
     * @Constraints\NotBlank()
     */
    protected $transformerStationId;

    /**
     * @var \DateTime
     * @Constraints\NotBlank()
     * @Constraints\DateTime()
     */
    protected $dateStart;

    /**
     * @var \DateTime
     * @Constraints\NotBlank()
     * @Constraints\DateTime()
     */
    protected $dateEnd;

    /**
     * DeviceUsage constructor.
     *
     * @param array $settings
     */
    public function __construct(array $settings)
    {
        $this->title = array_key_exists('title', $settings) ? $settings['title'] : null;
        $this->detailType = array_key_exists('detail_type', $settings) ? $settings['detail_type'] : null;
        $this->transformerStationId = array_key_exists('transformer_station_id', $settings)
            ? $settings['transformer_station_id']
            : null;

        $this->dateStart = array_key_exists('date_start', $settings)
            ? \DateTime::createFromFormat(DATE_ISO8601, $settings['date_start'])
            : null;
        $errors = \DateTime::getLastErrors();
        if ((0 !== $errors['warning_count']) || (0 !== $errors['error_count'])) {
            $this->dateStart = null;
        }

        $this->dateEnd = array_key_exists('date_end', $settings)
            ? \DateTime::createFromFormat(DATE_ISO8601, $settings['date_end'])
            : null;
        $errors = \DateTime::getLastErrors();
        if ((0 !== $errors['warning_count']) || (0 !== $errors['error_count'])) {
            $this->dateEnd = null;
        }
    }

    /**
     * @return string
     */
    public function getTransformerStationId()
    {
        return $this->transformerStationId;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getDetailType()
    {
        return $this->detailType;
    }

    /**
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    public function getViewType()
    {
        return ReportSettingsInterface::VIEW_TYPE_BY_CONSUMPTION;
    }
}