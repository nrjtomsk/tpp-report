<?php

namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints;

class GroupUsageCosts implements ReportSettingsInterface
{
    /**
     * @var string
     * @Constraints\NotBlank()
     */
    protected $title;

    /**
     * @var \DateTime
     * @Constraints\NotBlank()
     * @Constraints\DateTime()
     */
    protected $dateStart;

    /**
     * @var \DateTime
     * @Constraints\NotBlank()
     * @Constraints\DateTime()
     */
    protected $dateEnd;

    /**
     * @var double
     */
    protected $oneTariffFirst;

    /**
     * @var double
     */
    protected $twoTariffsFirst;

    /**
     * @var double
     */
    protected $twoTariffsSecond;

    /**
     * @var double
     */
    protected $threeTariffsFirst;

    /**
     * @var double
     */
    protected $threeTariffsSecond;

    /**
     * @var double
     */
    protected $threeTariffsThird;

    /**
     * GroupUsageCosts constructor.
     *
     * @param array $settings
     */
    public function __construct(array $settings)
    {
        $this->title = array_key_exists('title', $settings) ? $settings['title'] : null;

        $this->oneTariffFirst = array_key_exists('tp_1_t1', $settings) ? $settings['tp_1_t1'] : null;
        $this->twoTariffsFirst = array_key_exists('tp_2_t1', $settings) ? $settings['tp_2_t1'] : null;
        $this->twoTariffsSecond = array_key_exists('tp_2_t2', $settings) ? $settings['tp_2_t2'] : null;
        $this->threeTariffsFirst = array_key_exists('tp_3_t1', $settings) ? $settings['tp_3_t1'] : null;
        $this->threeTariffsSecond = array_key_exists('tp_3_t2', $settings) ? $settings['tp_3_t2'] : null;
        $this->threeTariffsThird = array_key_exists('tp_3_t3', $settings) ? $settings['tp_3_t3'] : null;

        $this->dateStart = array_key_exists('date_start', $settings)
            ? \DateTime::createFromFormat(DATE_ISO8601, $settings['date_start'])
            : null;
        $errors = \DateTime::getLastErrors();
        if ((0 !== $errors['warning_count']) || (0 !== $errors['error_count'])) {
            $this->dateStart = null;
        }

        $this->dateEnd = array_key_exists('date_end', $settings)
            ? \DateTime::createFromFormat(DATE_ISO8601, $settings['date_end'])
            : null;
        $errors = \DateTime::getLastErrors();
        if ((0 !== $errors['warning_count']) || (0 !== $errors['error_count'])) {
            $this->dateEnd = null;
        }
    }

    /**
     * Getting title of report
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Getting start date of report
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Getting end date of report
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @return float
     */
    public function getOneTariffFirst()
    {
        return $this->oneTariffFirst;
    }

    /**
     * @return float
     */
    public function getTwoTariffsFirst()
    {
        return $this->twoTariffsFirst;
    }

    /**
     * @return float
     */
    public function getTwoTariffsSecond()
    {
        return $this->twoTariffsSecond;
    }

    /**
     * @return float
     */
    public function getThreeTariffsFirst()
    {
        return $this->threeTariffsFirst;
    }

    /**
     * @return float
     */
    public function getThreeTariffsSecond()
    {
        return $this->threeTariffsSecond;
    }

    /**
     * @return float
     */
    public function getThreeTariffsThird()
    {
        return $this->threeTariffsThird;
    }

    /**
     * @return int
     */
    public function getDetailType()
    {
        return self::DETAIL_DAY;
    }

    /**
     * @return int
     */
    public function getViewType(): int
    {
        return self::VIEW_TYPE_BY_INDICATION;
    }
}