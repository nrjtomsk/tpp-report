<?php

namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints;

interface ReportSettingsInterface extends BaseReportSettingsInterface
{
    /**
     * Getting start date of report (for reports with user interval)
     *
     * @return \DateTime
     */
    public function getDateStart();

    /**
     * Getting end date of report (for reports with user interval)
     *
     * @return \DateTime
     */
    public function getDateEnd();
}