<?php

namespace AppBundle\Model;

class TransformerStationRatio
{
    /**
     * @var float
     */
    protected $currentRatio;

    /**
     * @var float
     */
    protected $voltageRatio;

    /**
     * @var float
     */
    protected $lineLossRM;

    /**
     * @var float
     */
    protected $lineLossRP;

    /**
     * TransformerStationRatio constructor.
     *
     * @param $currentRatio
     * @param $voltageRatio
     * @param $lineLossRM
     * @param $lineLossRP
     */
    public function __construct(float $currentRatio, float $voltageRatio, float $lineLossRM, float $lineLossRP)
    {
        $this->currentRatio = $currentRatio;
        $this->voltageRatio = $voltageRatio;
        $this->lineLossRM = $lineLossRM;
        $this->lineLossRP = $lineLossRP;
    }

    /**
     * @return float
     */
    public function getCurrentRatio()
    {
        return $this->currentRatio;
    }

    /**
     * @return float
     */
    public function getVoltageRatio()
    {
        return $this->voltageRatio;
    }

    /**
     * @return float
     */
    public function getLineLossRM()
    {
        return (float)bcadd('1', (string)$this->lineLossRM);
    }

    /**
     * @return float
     */
    public function getLineLossRP()
    {
        return (float)bcadd('1', (string)$this->lineLossRP);
    }

}