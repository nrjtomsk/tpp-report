<?php

namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints;

class PescCorporateXml implements SingleDateReportSettingsInterface
{
    /**
     * @var string
     *
     * @Constraints\NotBlank()
     */
    protected $title;

    /**
     * @var \DateTime
     *
     * @Constraints\NotBlank()
     * @Constraints\DateTime()
     */
    protected $date;

    /**
     * @var integer
     *
     * @Constraints\NotBlank()
     */
    protected $detailType;

    /**
     * PescCorporateXml constructor.
     * @param array $settings
     */
    public function __construct(array $settings)
    {
        $this->title = array_key_exists('title', $settings) ? $settings['title'] : null;
        $this->detailType = array_key_exists('detail_type', $settings)
            ? $settings['detail_type']
            : null;

        $this->date = array_key_exists('date', $settings)
            ? \DateTime::createFromFormat(DATE_ISO8601, $settings['date'])
            : null;
        $errors = \DateTime::getLastErrors();
        if ((0 !== $errors['warning_count']) || (0 !== $errors['error_count'])) {
            $this->date = null;
        }
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return \DateTime|null
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @return int
     */
    public function getDetailType(): int
    {
        return $this->detailType;
    }

    /**
     * @return int
     */
    public function getViewType(): int
    {
        return ReportSettingsInterface::VIEW_TYPE_BY_CONSUMPTION;
    }
}