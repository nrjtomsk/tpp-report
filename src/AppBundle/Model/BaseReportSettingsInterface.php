<?php

namespace AppBundle\Model;

interface BaseReportSettingsInterface
{
    const VIEW_TYPE_BY_CONSUMPTION = 1;
    const VIEW_TYPE_BY_INDICATION = 2;

    const DETAIL_HALF_HOUR = 1;
    const DETAIL_HOUR = 2;
    const DETAIL_DAY = 3;
    const DETAIL_MONTH = 5;

    /**
     * Getting title of report
     *
     * @return string
     */
    public function getTitle();
}