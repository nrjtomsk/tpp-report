<?php

namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints;

class GroupCorporateUsage implements ReportSettingsInterface
{
    /**
     * @var string
     * @Constraints\NotBlank()
     */
    protected $title;

    /**
     * @var int
     * @Constraints\Choice(
     *     choices={1, 2}
     * )
     */
    protected $viewType;

    /**
     * @var int
     * @Constraints\Choice(
     *     choices={1, 3, 5}
     * )
     */
    protected $detailType;

    /**
     * @var \DateTime
     * @Constraints\NotBlank()
     * @Constraints\DateTime()
     */
    protected $dateStart;

    /**
     * @var \DateTime
     * @Constraints\NotBlank()
     * @Constraints\DateTime()
     */
    protected $dateEnd;

    /**
     * GroupCorporateUsage constructor.
     *
     * @param array $settings
     */
    public function __construct(array $settings)
    {
        $this->title = array_key_exists('title', $settings) ? $settings['title'] : null;
        $this->viewType = array_key_exists('view_type', $settings) ? $settings['view_type'] : null;
        $this->detailType = array_key_exists('detail_type', $settings) ? $settings['detail_type'] : null;

        $this->dateStart = array_key_exists('date_start', $settings)
            ? \DateTime::createFromFormat(DATE_ISO8601, $settings['date_start'])
            : null;
        $errors = \DateTime::getLastErrors();
        if ((0 !== $errors['warning_count']) || (0 !== $errors['error_count'])) {
            $this->dateStart = null;
        }

        $this->dateEnd = array_key_exists('date_end', $settings)
            ? \DateTime::createFromFormat(DATE_ISO8601, $settings['date_end'])
            : null;
        $errors = \DateTime::getLastErrors();
        if ((0 !== $errors['warning_count']) || (0 !== $errors['error_count'])) {
            $this->dateEnd = null;
        }
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @return int
     */
    public function getViewType()
    {
        return $this->viewType;
    }

    /**
     * @return int
     */
    public function getDetailType()
    {
        return $this->detailType;
    }
}