<?php

namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints;

class PescCorporateUsage implements SingleDateReportSettingsInterface
{
    /**
     * @var string
     *
     * @Constraints\NotBlank()
     */
    protected $title;

    /**
     * @var \DateTime
     *
     * @Constraints\NotBlank()
     * @Constraints\DateTime()
     */
    protected $date;

    /**
     * PescCorporateUsage constructor.
     *
     * @param array $settings
     */
    public function __construct(array $settings)
    {
        $this->title = array_key_exists('title', $settings) ? $settings['title'] : null;

        $this->date = array_key_exists('date', $settings)
            ? \DateTime::createFromFormat(DATE_ISO8601, $settings['date'])
            : null;
        $errors = \DateTime::getLastErrors();
        if ((0 !== $errors['warning_count']) || (0 !== $errors['error_count'])) {
            $this->date = null;
        }
    }


    /**
     * Getting title of report
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return int
     */
    public function getDetailType()
    {
        return ReportSettingsInterface::DETAIL_HALF_HOUR;
    }

    public function getViewType()
    {
        return ReportSettingsInterface::VIEW_TYPE_BY_CONSUMPTION;
    }
}