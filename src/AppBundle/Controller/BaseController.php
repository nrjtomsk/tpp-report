<?php

namespace AppBundle\Controller;

use AppBundle\Exceptions\ApiException;
use AppBundle\Entity\ReportRequest;
use AppBundle\Entity\ReportResult;
use AppBundle\Exceptions\ValidationException;
use AppBundle\Message\ReportMessage;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class BaseController extends Controller
{
    /**
     * @return \Symfony\Component\Validator\Validator\ValidatorInterface
     */
    protected function validator()
    {
        return $this->get('validator');
    }

    /**
     * @return \AppBundle\Service\ReportRequestManager
     */
    protected function reportRequestManager()
    {
        return $this->get('app.manager.report_request');
    }

    /**
     * @return \AppBundle\Service\ReportRequestFileUploadManager
     */
    protected function reportRequestFileUploadManager()
    {
        return $this->get('app.manager.report_request_file_upload');
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager
     */
    protected function entityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @return \JMS\Serializer\Serializer
     */
    protected function serializer()
    {
        return $this->get('jms_serializer');
    }

    /**
     * @return \AppBundle\Service\ReportResultMapper
     */
    protected function reportResultMapper()
    {
        return $this->get('app.mapper.report_result');
    }

    /**
     * @return \AppBundle\Repository\ReportRequestRepository
     */
    protected function getReportRequestRepository()
    {
        return $this->entityManager()->getRepository(ReportRequest::class);
    }

    /**
     * @param $object
     *
     * @throws ValidationException
     */
    protected function validate($object)
    {
        $violationList = $this->validator()->validate($object);

        if ($violationList->count() !== 0) {
            throw new ValidationException($violationList);
        }
    }

    /**
     * @param $object
     * @param $status
     *
     * @return Response
     */
    protected function createSerializedResponse($object, $status)
    {
        $data = $this->serializer()->serialize($object, 'json');

        $response = new Response($data, $status);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param ReportResult $reportResult
     *
     * @return BinaryFileResponse
     */
    protected function createReportResultFileResponse(ReportResult $reportResult)
    {
        $rootDir = $this->get('kernel')->getRootDir();
        $file = $rootDir
            . DIRECTORY_SEPARATOR . '..'
            . DIRECTORY_SEPARATOR . 'var'
            . DIRECTORY_SEPARATOR . 'results'
            . DIRECTORY_SEPARATOR . $reportResult->getFile();

        $fileName = $this->getReportFileName($reportResult, $file);

        return $this->createFileResponse($file, $fileName);
    }

    /**
     * Files of any report can be renamed in this method while a client requests them.
     *
     * @param ReportResult $reportResult
     * @param $file
     * @return string
     */
    protected function getReportFileName(ReportResult $reportResult, $file)
    {
        $reportCode = $reportResult->getReportRequest()->getCode();
        $resultType = $reportResult->getType();
        $fileName = '';

        switch ($reportCode) {
            case ReportRequest::CODE_PESC_CORPORATE_XML:
                if ($resultType === ReportResult::TYPE_XML) {
                    $fileName = basename($file);
                }
                break;
            case ReportRequest::CODE_PESC_CORPORATE_USAGE:
                if ($resultType === ReportResult::TYPE_ZIP || $resultType === ReportResult::TYPE_JSON) {
                    $fileName = basename($file);
                }
                break;
            case ReportRequest::CODE_RKS_ENERGO_CORPORATE_USAGE;
                $fileName = basename($file);
                break;
            case ReportRequest::CODE_TRANSFORM_STATION_BALANCE;
                $fileName = basename($file);
                break;
            default:
                $title = $reportResult->getReportRequest()->getSettings()->getTitle();
                $fileName = $this->get('app.transliterator')->transliterate($title) . '.' . $reportResult->getType();
                break;
        }

        return $fileName;
    }

    /**
     * @param $file
     * @param $fileName
     *
     * @return BinaryFileResponse
     * @throws ApiException
     */
    protected function createFileResponse($file, $fileName)
    {
        if (!file_exists($file)) {
            throw new ApiException('File not found: ' . $file, 'not_found', 404);
        }

        $response = new BinaryFileResponse($file);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $fileName
        );

        return $response;
    }

    /**
     * @param array $array
     * @param string $key
     *
     * @throws ApiException
     */
    protected function getValueOrThrowException(array $array, string $key)
    {
        if (empty($array[$key])) {
            throw new ApiException('Don\'t have field ' . $key . ' for one of the reports');
        }

        return $array[$key];
    }

    /**
     * @param ReportMessage $message
     */
    protected function sendReportMessage(ReportMessage $message)
    {
        /** @var ProducerInterface $producer */
        $producer = $this->get('app.rabbitmq.proxy');
        $producer->publish($this->serializer()->serialize($message, 'json'));
    }
}