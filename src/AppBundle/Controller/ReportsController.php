<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ReportRequest;
use AppBundle\Entity\ReportResult;
use AppBundle\Exceptions\NoReportsException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReportsController extends BaseController
{
    /**
     * @Route(path="api/v1/reports", name="api_reports_post")
     * @Method(methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        if (! $reports = $request->request->get('reports')) {
            throw new NoReportsException();
        }
        $createdReports = [];

        foreach ($reports as $report) {
            $code = $this->getValueOrThrowException($report, 'report_code');
            $project = $this->getValueOrThrowException($report, 'project_id');
            $externalId = $this->getValueOrThrowException($report, 'id');
            $settings = $this->getValueOrThrowException($report, 'settings');

            $reportRequest = $this->reportRequestManager()->createReportRequest($code, $project, $externalId, $settings);
            $createdReports['reports'][] = [
                'id' => $reportRequest->getId(),
                'status' => $reportRequest->getStatus(),
                'code' => $reportRequest->getCode()
            ];
        }

        /** TODO: use serialized DTO instead */
        return new JsonResponse($createdReports, Response::HTTP_OK);
    }

    /**
     * @Route("api/v1/reports/{id}", name="api_get_report")
     * @Method(methods={"GET"})
     *
     * @param ReportRequest $reportRequest
     *
     * @return JsonResponse
     */
    public function getReportAction(ReportRequest $reportRequest)
    {
        $reportResultDTO = $this->reportResultMapper()->map($reportRequest);

        return $this->createSerializedResponse($reportResultDTO, Response::HTTP_OK);
    }

    /**
     * @Route("api/v1/reports/{report}/files/{file}", name="api_get_report_file")
     * @ParamConverter("reportRequest", class="AppBundle:ReportRequest", options={"id" = "report"})
     * @ParamConverter("reportResult", class="AppBundle:ReportResult", options={"id" = "file"})
     *
     * @param ReportRequest $reportRequest
     * @param ReportResult  $reportResult
     *
     * @return BinaryFileResponse
     */
    public function getReportFile(ReportRequest $reportRequest, ReportResult $reportResult)
    {
        if ($reportResult->getReportRequest()->getId() !== $reportRequest->getId()) {
            throw $this->createNotFoundException();
        }

        return $this->createReportResultFileResponse($reportResult);
    }

    /**
     * @Route("api/v1/reports/{id}/files", name="api_files_upload")
     * @Method(methods={"POST"})
     *
     * @param Request       $request
     * @param ReportRequest $reportRequest
     *
     * @return JsonResponse
     */
    public function uploadAction(Request $request, ReportRequest $reportRequest)
    {
        $file = $request->files->get('file');

        $this->reportRequestFileUploadManager()->handleUpload(
            $reportRequest, $file
        );

        return new JsonResponse([], Response::HTTP_NO_CONTENT);
    }
}