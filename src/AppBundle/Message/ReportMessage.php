<?php

declare(strict_types=1);

namespace AppBundle\Message;

use JMS\Serializer\Annotation\ExclusionPolicy;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class ReportMessage
 * @package src\AppBundle\Message
 *
 * @ExclusionPolicy("none")
 */
class ReportMessage
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $status;

    /**
     * ReportMessage constructor.
     * @param $id
     * @param $status
     */
    public function __construct(int $id, string $status)
    {
        $this->id = $id;
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode([
            'id' => $this->getId(),
            'status' => $this->getStatus()
        ]);
    }

    /**
     * Create ReportMessage from json string
     *
     * @param string $json
     * @return ReportMessage
     */
    public static function createFromString(string $json): self
    {
        $data = json_decode($json, true);
        if (!isset($data['id']) || !isset($data['status']) ) {
            // TODO: specific exception needed
            throw new Exception('Wrong data provided');
        }

        return new self(
            $data['id'],
            $data['status']
        );
    }
}