<?php

namespace AppBundle\Message;

use JMS\Serializer\Annotation\Groups;

/**
 * Class ExceptionMessage
 * @package AppBundle\DTO
 */
class ExceptionMessage
{
    /**
     * @var string
     * @Groups(groups={"Default"})
     */
    protected $message;

    /**
     * @var string
     * @Groups(groups={"Default"})
     */
    protected $shortMessage;

    /**
     * @var int
     * @Groups(groups={"Default"})
     */
    protected $errorCode;

    /**
     * @var string
     * @Groups(groups={"Default", "Validation"})
     */
    protected $errors;

    /**
     * ExceptionMessage constructor.
     *
     * @param string $message
     * @param string $shortMessage
     * @param string $errorCode
     */
    public function __construct($message, $shortMessage, $errorCode)
    {
        $this->message = $message;
        $this->shortMessage = $shortMessage;
        $this->errorCode = $errorCode;
    }

    /**
     * @return mixed
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getShortMessage(): string
    {
        return $this->shortMessage;
    }

    /**
     * @return mixed
     */
    public function getErrorCode(): int
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors(array $errors)
    {
        $this->errors = $errors;
    }
}