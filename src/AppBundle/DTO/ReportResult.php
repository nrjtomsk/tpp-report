<?php

namespace AppBundle\DTO;

use AppBundle\Entity\ReportRequest;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * Class ReportRequest
 * @package AppBundle\DTO
 *
 * @ExclusionPolicy("none")
 */
class ReportResult
{
    private $id;

    private $status;

    private $externalId;

    private $project;

    private $settings;

    private $result;

    /**
     * ReportResult constructor.
     * @param ReportRequest $reportRequest
     * @param ReportResultFiles $result
     */
    public function __construct(ReportRequest $reportRequest, ReportResultFiles $result)
    {
        $this->id = $reportRequest->getId();
        $this->status = $reportRequest->getStatus();
        $this->externalId = $reportRequest->getExternalId();
        $this->project = $reportRequest->getProject();
        $this->settings = $reportRequest->getSettings();
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return array
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return array
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }
}