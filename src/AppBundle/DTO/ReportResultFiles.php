<?php

namespace AppBundle\DTO;

use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * Class ReportRequestFiles
 * @package AppBundle\DTO
 *
 * @ExclusionPolicy("none")
 */
class ReportResultFiles
{
    /**
     * @var array
     */
    private $xls = [];

    /**
     * @var array
     */
    private $json = [];

    /**
     * @var array
     */
    private $zip = [];

    /**
     * @var array
     */
    private $xml = [];

    /**
     * ReportResultFiles constructor.
     * @param array $filesByType
     */
    public function __construct(array $filesByType = [])
    {
        foreach ($filesByType as $type => $files) {
            if (property_exists($this, $type)) {
                $this->$type = $files;
            }
        }
    }

    /**
     * @return array
     */
    public function getXls()
    {
        return $this->xls;
    }

    /**
     * @return array
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * @return array
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @return array
     */
    public function getXml()
    {
        return $this->xml;
    }
}