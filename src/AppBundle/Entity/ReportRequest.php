<?php

namespace AppBundle\Entity;

use AppBundle\Model\BaseReportSettingsInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;

/**
 * Report
 *
 * @ORM\Table(name="report_requests")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReportRequestRepository")
 */
class ReportRequest
{
    const STATUS_WAITING_DATA = 'waiting';
    const STATUS_HAS_DATA = 'has_data';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_ERROR = 'error';
    const STATUS_DONE = 'done';

    const CODE_DEVICE_USAGE = 'device_usage_v1';
    const CODE_GROUP_USAGE = 'group_usage_v2';
    const CODE_GROUP_CORPORATE_USAGE = 'group_corp_usage_v2';
    const CODE_GROUP_USAGE_COSTS = 'group_usage_costs_v1';
    const CODE_TRANSFORM_STATION_BALANCE = 'transformer_station_balance_v1';
    const CODE_PESC_CORPORATE_USAGE = 'pesc_corporate_usage_v2';
    const CODE_PESC_CORPORATE_XML = 'pesc_corporate_xml_80020_v1';
    const CODE_RKS_ENERGO_CORPORATE_USAGE = 'rks_energo_corporate_usage_v1';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50)
     */
    private $status = self::STATUS_WAITING_DATA;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @Constraints\NotBlank()
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="project", type="string", length=50)
     * @Constraints\NotBlank()
     */
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string", length=50)
     * @Constraints\NotBlank()
     */
    private $externalId;

    /**
     * @var BaseReportSettingsInterface
     *
     * @ORM\Column(name="settings", type="object")
     *
     * @Constraints\NotBlank()
     */
    private $settings;

    /**
     * @var ReportFile
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ReportFile", mappedBy="reportRequest")
     */
    private $dataFile;

    /**
     * @var ReportResult
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ReportResult", mappedBy="reportRequest")
     */
    private $results;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->results = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add result
     *
     * @param ReportResult $result
     *
     * @return ReportRequest
     */
    public function addResult(ReportResult $result)
    {
        $this->results[] = $result;

        return $this;
    }

    /**
     * Remove result
     *
     * @param ReportResult $result
     */
    public function removeResult(ReportResult $result)
    {
        $this->results->removeElement($result);
    }

    /**
     * Get results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * Get dataFile
     *
     * @return ReportFile
     */
    public function getDataFile()
    {
        return $this->dataFile;
    }

    /**
     * Set dataFile
     *
     * @param ReportFile $dataFile
     *
     * @return ReportRequest
     */
    public function setDataFile(ReportFile $dataFile = null)
    {
        $this->dataFile = $dataFile;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasData()
    {
        return $this->getStatus() === self::STATUS_HAS_DATA;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return ReportRequest
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return ReportRequest
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param mixed $settings
     */
    public function setSettings(BaseReportSettingsInterface $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @return string
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param string $project
     *
     * @return ReportRequest
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     * @return ReportRequest
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }
}
