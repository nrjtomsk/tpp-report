<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportResult
 *
 * @ORM\Table(name="report_results")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReportResultRepository")
 */
class ReportResult
{
    const TYPE_JSON = 'json';
    const TYPE_XLS = 'xls';
    const TYPE_ZIP = 'zip';
    const TYPE_XML = 'xml';

    const TYPE_ZIP_TITLE = 'ZIP-архив';
    const TYPE_JSON_TITLE = 'JSON';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ReportRequest
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ReportRequest", inversedBy="results")
     */
    private $reportRequest;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=40)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=100)
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", nullable=true)
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="incomplete", type="boolean", nullable=true)
     */
    private $incomplete = false;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ReportResult
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return ReportResult
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get reportRequest
     *
     * @return \AppBundle\Entity\ReportRequest
     */
    public function getReportRequest()
    {
        return $this->reportRequest;
    }

    /**
     * Set reportRequest
     *
     * @param ReportRequest $reportRequest
     *
     * @return ReportResult
     */
    public function setReportRequest(ReportRequest $reportRequest = null)
    {
        $this->reportRequest = $reportRequest;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isIncomplete(): bool
    {
        return $this->incomplete;
    }

    /**
     * @param boolean $isIncomplete
     */
    public function setIncomplete(bool $isIncomplete)
    {
        $this->incomplete = $isIncomplete;
    }
}
