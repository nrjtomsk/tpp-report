<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints;

/**
 * ReportFile
 *
 * @ORM\Table(name="report_files")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReportFileRepository")
 * @Vich\Uploadable
 */
class ReportFile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     *
     * @ORM\Column(name="file", type="string", length=255)
     */
    private $file;

    /**
     * @var
     *
     * @Vich\UploadableField(mapping="report_data_files", fileNameProperty="file")
     * @Constraints\NotNull()
     * @Constraints\File(
     *      mimeTypes={"application/x-gzip", "application/zip"}
     * )
     */
    private $uploadedFile;

    /**
     * @var
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ReportRequest", inversedBy="dataFiles")
     */
    private $reportRequest;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return ReportFile
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return __DIR__.'/../../../var/data/'.$this->getFile();
    }

    /**
     * Set reportRequest
     *
     * @param ReportRequest $reportRequest
     *
     * @return ReportFile
     */
    public function setReportRequest(ReportRequest $reportRequest = null)
    {
        $this->reportRequest = $reportRequest;

        return $this;
    }

    /**
     * Get reportRequest
     *
     * @return ReportRequest
     */
    public function getReportRequest()
    {
        return $this->reportRequest;
    }

    /**
     * @return mixed
     */
    public function getUploadedFile()
    {
        return $this->uploadedFile;
    }

    /**
     * @param mixed $uploadedFile
     */
    public function setUploadedFile(File $uploadedFile = null)
    {
        $this->uploadedFile = $uploadedFile;
    }
}
