<?php

namespace AppBundle\Service;

use AppBundle\Entity\ReportFile;
use AppBundle\Entity\ReportRequest;
use AppBundle\Exceptions\ValidationException;
use AppBundle\Message\ReportMessage;
use AppBundle\Service\Proxy\RabbitMQProxyInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ReportRequestFileUploadManager
{
    /**
     * @var ObjectManager
     */
    private $manager;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var RabbitMQProxyInterface
     */
    private $rabbitMQProxy;

    /**
     * ReportRequestFileUploadManager constructor.
     *
     * @param ObjectManager          $manager
     * @param ValidatorInterface     $validator
     * @param RabbitMQProxyInterface $rabbitMQProxy
     */
    public function __construct(
        ObjectManager $manager,
        ValidatorInterface $validator,
        RabbitMQProxyInterface $rabbitMQProxy
    )
    {
        $this->manager = $manager;
        $this->validator = $validator;
        $this->rabbitMQProxy = $rabbitMQProxy;
    }

    /**
     * @param ReportRequest $reportRequest
     * @param File|null     $file
     *
     * @return bool
     * @throws ValidationException
     */
    public function handleUpload(ReportRequest $reportRequest, File $file = null): bool
    {
        if ($reportRequest->getDataFile() !== null) {
            return true;
        }

        $reportFile = new ReportFile();
        $reportFile->setUploadedFile($file);
        $reportFile->setReportRequest($reportRequest);

        $errors = $this->validator->validate($reportFile);

        if ($errors->count() !== 0) {
            throw new ValidationException($errors);
        }

        $this->manager->persist($reportFile);
        $reportRequest->setStatus(ReportRequest::STATUS_HAS_DATA);
        $this->manager->flush();

        $this->rabbitMQProxy->publish(new ReportMessage(
            $reportRequest->getId(),
            $reportRequest->getStatus()
        ));

        return true;
    }
}