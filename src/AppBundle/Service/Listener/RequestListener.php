<?php

namespace AppBundle\Service\Listener;

use AppBundle\Exceptions\JsonParsingException;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RequestListener
{
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if ($this->isDecodeable($request)) {
            $body = $request->getContent();
            $data = json_decode($body, true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new JsonParsingException(json_last_error_msg());
            }

            $request->request = new ParameterBag($data);
        }
    }

    /**
     * Check if we can decode request
     *
     * @param Request $request
     *
     * @return bool
     */
    protected function isDecodeable(Request $request)
    {
        return in_array($request->getMethod(), ['POST', 'PUT', 'PATCH', 'DELETE'], true)
            && $request->headers->get('Content-Type') === 'application/json';
    }
}