<?php

namespace AppBundle\Service\Listener;

use AppBundle\Exceptions\ApiExceptionInterface;
use AppBundle\Exceptions\ValidationException;
use AppBundle\Message\ExceptionMessage;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\ConstraintViolation;

class ExceptionListener
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ExceptionListener constructor.
     *
     * @param Serializer $serializer
     * @param LoggerInterface $logger
     */
    public function __construct(Serializer $serializer, LoggerInterface $logger)
    {
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof NotFoundHttpException) {
            $response = $this->handleNotFountException($exception);
        } elseif ($exception instanceof ApiExceptionInterface) {
            $response = $this->handleApiProblemException($exception);
        } elseif ($exception instanceof ValidationException) {
            $response = $this->handleValidationException($exception);
        } elseif ($exception instanceof MethodNotAllowedHttpException) {
            $response = $this->handleMethodNotAllowedException($exception);
        } else {
            $response = $this->handleException($exception);
        }

        $event->setResponse($response);
    }

    /**
     * @param NotFoundHttpException $exception
     *
     * @return JsonResponse
     */
    protected function handleNotFountException(NotFoundHttpException $exception): Response
    {
        $message = new ExceptionMessage(
            $exception->getMessage(),
            'not_found',
            404
        );

        return $this->createResponse(
            $this->serializer->serialize(
                $message,
                'json',
                SerializationContext::create()->setGroups(['Default'])
            ),
            $message->getErrorCode()
        );
    }

    /**
     * @param $exception
     *
     * @return JsonResponse
     */
    protected function handleValidationException(ValidationException $exception): Response
    {
        $message = new ExceptionMessage(
            $exception->getMessage(),
            'validation_errors',
            $exception->getErrorCode()
        );

        $validationMessages = $exception->getErrors();

        $errors = [];
        /** @var ConstraintViolation $error */
        foreach ($validationMessages as $validationMessage) {
            $errors[] = [
                'message' => $validationMessage->getMessage(),
                'field' => $validationMessage->getPropertyPath()
            ];
        }

        $message->setErrors($errors);

        return $this->createResponse(
            $this->serializer->serialize(
                $message,
                'json',
                SerializationContext::create()->setGroups(['Default', 'Validation'])
            ),
            $message->getErrorCode()
        );
    }

    /**
     * @param ApiExceptionInterface $exception
     *
     * @return JsonResponse
     */
    protected function handleApiProblemException(ApiExceptionInterface $exception): Response
    {
        $message = new ExceptionMessage(
            $exception->getMessage(),
            $exception->getShortErrorMessage(),
            $exception->getErrorCode()
        );

        return $this->createResponse(
            $this->serializer->serialize(
                $message,
                'json',
                SerializationContext::create()->setGroups(['Default'])
            ),
            $message->getErrorCode()
        );
    }

    /**
     * @param string $body
     *
     * @return Response
     */
    protected function createResponse(string $body, int $statusCode): Response
    {
        $response = new Response();

        $response->setContent($body);
        $response->setStatusCode($statusCode);
        $response->headers->add([
            'Content-Type' => 'application/json',
            'Content-Length' => mb_strlen($body),
        ]);

        return $response;
    }

    /**
     * @param MethodNotAllowedHttpException $exception
     *
     * @return Response
     */
    protected function handleMethodNotAllowedException(MethodNotAllowedHttpException $exception): Response
    {
        $message = new ExceptionMessage(
            $exception->getMessage(),
            'method_not_allowed',
            405
        );

        return $this->createResponse(
            $this->serializer->serialize(
                $message,
                'json',
                SerializationContext::create()->setGroups(['Default'])
            ),
            $message->getErrorCode()
        );
    }

    /**
     * @param \Exception $exception
     *
     * @return Response
     */
    protected function handleException(\Exception $exception): Response
    {
        $message = new ExceptionMessage(
            'Application exception',
            'exception',
            500
        );
        $this->logger->error($exception->getMessage());

        return $this->createResponse(
            $this->serializer->serialize(
                $message,
                'json',
                SerializationContext::create()->setGroups(['Default'])
            ),
            $message->getErrorCode()
        );
    }
}