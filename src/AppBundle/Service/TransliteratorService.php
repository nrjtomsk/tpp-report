<?php

namespace AppBundle\Service;

class TransliteratorService
{
    /**
     * Transliterate a given string and set all the symbols to lowercase, replace the unneeded symbols.
     *
     * @param $string
     *
     * @return string
     */
    public function transliterate($string)
    {
        //cut all the quote symbols
        $string = preg_replace("/['\"`ʹ]/u", "", $string);

        //convert all the non-alphabetic and non-numeric symbols to a hyphen
        $string = preg_replace('/(([^a-zA-Zа-яА-Я0-9])|(\s))/u', '-', $string);

        $listInputSymbols = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я', 'ї', 'є',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я', 'Ї'
        ];

        $listOutputSymbols = [
            'a','b','v','g','d','e','jo','zh','z','i','i','k','l','m','n','o','p',
            'r','s','t','u','f','kh','c','ch','sh','shh','','y','','eh','yu','ya', 'i', 'ie',
            'a','b','v','g','d','e','jo','zh','z','i','i','k','l','m','n','o','p',
            'r','s','t','u','f','kh','c','ch','sh','shh','','y','', 'eh','yu','ya', 'yi', 'ie'
        ];

        $string = str_replace($listInputSymbols, $listOutputSymbols, $string);

        return $string;
    }

    /**
     * Translate an English month string to a Russian one.
     *
     * @param string $month
     * @return string
     */
    public function translateEnglishMonthIntoRussian(string $month) : string
    {
        $month = strtolower($month);

        $monthTable = [
            'january' => 'января',
            'february' => 'февраля',
            'march' => 'марта',
            'april' => 'апреля',
            'may' => 'мая',
            'june' => 'июня',
            'july' => 'июля',
            'august' => 'августа',
            'september' => 'сентября',
            'october' => 'октября',
            'november' => 'ноября',
            'december' => 'декабря'
        ];

        return $monthTable[$month];
    }
}