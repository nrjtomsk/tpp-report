<?php

namespace AppBundle\Service\Generator;

use AppBundle\Entity\ReportRequest;
use AppBundle\Exceptions\ApiException;
use AppBundle\Model\DeviceUsage;
use AppBundle\Model\ReportSettingsInterface;

class DeviceUsageGenerator extends AbstractReportGenerator
{
    const REPORT_TITLE_ENDING = 'Потребление ПУ';

    /**
     * @param $saveDir
     *
     * @param $data
     * @throws \PHPExcel_Exception
     */
    protected function createXLSFiles(string $saveDir, array $data)
    {
        $detailTypeId = $this->reportSettings->getDetailType();

        if ($detailTypeId == ReportSettingsInterface::DETAIL_DAY
            || $detailTypeId == ReportSettingsInterface::DETAIL_MONTH) {

            $this->createDailyOrMonthlyXLSFile($saveDir, $data);
        } elseif ($detailTypeId == ReportSettingsInterface::DETAIL_HALF_HOUR) {
            $this->createHalfHourlyXLSFile($saveDir, $data);
        }
    }

    /**
     *
     */
    private function createHalfHourlyXLSFile(string $saveDir, array $data)
    {
        $phpExcelObject = $this->createPhpExcelObject();
        $sheet = $phpExcelObject->getActiveSheet();

        $sheet->setTitle(self::XLS_DEFAULT_WORKSHEET_TITLE);

        $sheet->freezePane('A8');
        $sheet->mergeCells('A3:H3');

        $this->widthColumnsFromArray($sheet, [
            'A' => 26.0,
            'B' => 18.0,
            'C' => 18.0,
            'D' => 20.0,
            'E' => 22.0,
        ]);
        $this->widthColumnsOnRange($sheet, 'F', 'H', 10.0);

        $tableTitles = [
            'ФИО клиента (название)',
            'адрес установки',
            'серийный номер',
            'MAC адрес',
            'Дата'
        ];

        $sheet->setCellValueByColumnAndRow(0, 1, $data['title']);
        $this->styleForTitleRow($sheet);

        $sheet->setCellValueByColumnAndRow(0, 3, $data['description']['value']);
        $this->styleForSubtitleRow($sheet);

        $sheet->fromArray($tableTitles, null, 'A6');
        $sheet->getStyle('A6:H7')->getFont()->setBold(true);

        $sheet->setCellValue('F6', 'A+', true)->getStyle()->getFont()->setBold(true);
        $sheet->mergeCells('F6:F7');
        $sheet->getStyle('F6:F7')->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);


        foreach (range('a', 'e') as $letter) {
            $range = strtoupper($letter) . '6:' . strtoupper($letter) . '7';
            $sheet->mergeCells($range);
            $sheet->getStyle($range)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);
        }

        $sheet->fromArray($data['info'], null, 'A8');
        $sheet->getStyle('A8:D8')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        foreach (range('A', 'D') as $item) {
            $cellsRange = strtoupper($item) . '8:'
                . strtoupper($item) . (8 + (count($data['data']) - 1));
            $sheet->mergeCells($cellsRange);
            $sheet->getStyle($cellsRange)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
        }

        $row = 8;

        //set the dates column in this way to prevent number formatting (strings are needed)
        foreach ($data['data'] as $date => $value) {
            $dateObject = \DateTime::createFromFormat('d.m.Y H:i', $date);
            $dateObject->modify('+30 minutes');

            $sheet->getCell('E' . $row)->setValue('=TEXT("' . $date . ' - ' . $dateObject->format('H:i') . '", "@")');
            $sheet->getCell('F' . $row)->setValue($value);
            $row += 1;
        }

        $sheet->setSelectedCells('A1');
        $fileName = $this->transliterator->transliterate($data['title']);

        $this->saveXLSFile($phpExcelObject, $saveDir, $data['title'], false, $fileName);
    }

    /**
     *
     */
    private function createDailyOrMonthlyXLSFile(string $saveDir, array $data)
    {
        $phpExcelObject = $this->createPhpExcelObject();
        $sheet = $phpExcelObject->getActiveSheet();

        $sheet->setTitle(self::XLS_DEFAULT_WORKSHEET_TITLE);

        $sheet->freezePane('A8');
        $sheet->mergeCells('A3:H3');

        $this->widthColumnsFromArray($sheet, [
            'A' => 26.0,
            'B' => 18.0,
            'C' => 18.0,
            'D' => 20.0,
            'E' => 12.0,
        ]);
        $this->widthColumnsOnRange($sheet, 'F', 'H', 10.0);

        $tableTitles = [
            'ФИО клиента (название)',
            'адрес установки',
            'серийный номер',
            'MAC адрес',
            'Дата'
        ];

        $sheet->setCellValueByColumnAndRow(0, 1, $data['title']);
        $this->styleForTitleRow($sheet);

        $sheet->setCellValueByColumnAndRow(0, 3, $data['description']['value']);
        $this->styleForSubtitleRow($sheet);

        $sheet->fromArray($tableTitles, null, 'A6');
        $sheet->getStyle('A6:H7')->getFont()->setBold(true);

        $sheet->setCellValue('F6', 'Тариф', true)->getStyle()->getFont()->setBold(true);
        $sheet->mergeCells('F6:H6');
        $sheet->fromArray(['Сумма', 'День', 'Ночь'], null,'F7');

        foreach (range('a', 'e') as $letter) {
            $range = strtoupper($letter) . '6:' . strtoupper($letter) . '7';
            $sheet->mergeCells($range);
            $sheet->getStyle($range)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);
        }

        $sheet->fromArray($data['info'], null, 'A8');
        $sheet->getStyle('A8:D8')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        if (!empty($data['data'])) {
            foreach (range('A', 'D') as $item) {
                $cellsRange = strtoupper($item) . '8:'
                    . strtoupper($item) . (8 + (count($data['data']) - 1));
                $sheet->mergeCells($cellsRange);
                $sheet->getStyle($cellsRange)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
            }

            $dates = [];

            foreach ($data['data'] as $elementKey => $element) {
                if ($this->reportSettings->getDetailType() == ReportSettingsInterface::DETAIL_DAY) {
                    $dateObject = \DateTime::createFromFormat('d.m.Y', $element[0]);
                    $fullMonthRussian = $this->transliterator->translateEnglishMonthIntoRussian($dateObject->format('F'));
                    $dayInMonth = $dateObject->format('d');
                    $dates[] = $dayInMonth . ' ' . $fullMonthRussian;
                } else {
                    $dates[] = $element[0];
                }

                unset($data['data'][$elementKey][0]);
            }
        }

        $startRow = 8;

        //set the dates column in this way to prevent number formatting (strings are needed)
        for ($i = 0; $i < count($dates); $i++) {
            $sheet->getCell('E' . ($startRow + $i))->setValue('=TEXT("' . $dates[$i] . '", "@")');
        }

        $sheet->fromArray($data['data'], null, 'F8', true);
        $sheet->setSelectedCells('A1');
        $fileName = $this->transliterator->transliterate($data['title']);

        $this->saveXLSFile($phpExcelObject, $saveDir, $data['title'], false, $fileName);
    }

    /**
     * @param $deviceId
     *
     * @return array
     */
    protected function prepareMeteringData($deviceId)
    {
        $result = [];

        if (array_key_exists($deviceId, $this->usageData)) {
            $data = $this->usageData[$deviceId];
        } else {
            $data = [];
        }

        foreach ($data as $date => $item) {
            $row = [];
            $row[] = \DateTime::createFromFormat(DATE_ISO8601, $date)->format('d.m.Y');
            for ($i = 0; $i < 3; $i++) {
                $row[] = isset($item[0]['values'][$i])
                    ? self::convertWattsToKilowatts($item[0]['values'][$i])
                    : 0;
            }

            $result[] = $row;
        }

        return $result;
    }

    /**
     * @param int $deviceId
     * @throws ApiException
     * @return array
     */
    protected function prepareMeteringDataHalfHourly($deviceId)
    {
        if ($this->reportSettings->getViewType() == ReportSettingsInterface::VIEW_TYPE_BY_INDICATION) {
            throw new ApiException(self::ERROR_UNSUPPORTED_HALF_HOUR_REPORT, self::ERROR_UNSUPPORTED_HALF_HOUR_REPORT);
        }

        $result = [];
        $halfHourDates = $this->createHalfHourDatesRange();

        $data = $this->usageData[$deviceId];
        $halfHoursDailyStart = 0;

        foreach ($data as $dayDate => $item) {

            for ($i = 0; $i < 48; $i++) {
                $handledValue = isset($item[0]['values'][$i]) ? $this::convertWattsToKilowatts($item[0]['values'][$i]) : 0;

                if ($halfHoursDailyStart == count($halfHourDates)) {
                    $result[$halfHourDates[$halfHoursDailyStart - 1]] = $handledValue;
                } elseif ($halfHoursDailyStart > count($halfHourDates)) {
                    break;
                } else {
                    $result[$halfHourDates[$halfHoursDailyStart + $i]] = $handledValue;
                }

                if ($i === 47) {
                    $halfHoursDailyStart += 48;
                }
            }
        }

        return $result;
    }

    /**
     * Prepare metering data for the months by given days.
     *
     * @param int $deviceId
     * @return array
     */
    protected function prepareMeteringDataMonthlyByDays($deviceId)
    {
        $result = [];

        $data = $this->usageData[$deviceId];
        $isFirstItem = true;

        foreach ($data as $date => $item) {
            $row = [];

            $monthYearDate = \DateTime::createFromFormat(DATE_ISO8601, $date)->format('m.Y');
            $row[] = $monthYearDate;

            for ($i = 0; $i < 3; $i++) {
                $row[] = isset($item[0]['values'][$i])
                    ? $this::convertWattsToKilowatts($item[0]['values'][$i])
                    : 0;
            }

            //convert the daily data into a monthly report
            if ($isFirstItem) {
                $result[] = $row;
                $isFirstItem = false;
                continue;
            }

            $isNewMonth = false;

            foreach ($result as $key => $element) {
                if ($element[0] == $monthYearDate) {
                    for ($i = 1; $i <= 3; $i++) {
                        $element[$i] += $row[$i];
                    }

                    $result[$key] = $element;
                    $isNewMonth = false;
                    break;
                } else {
                    $isNewMonth = true;
                }
            }

            if ($isNewMonth) {
                $result[] = $row;
            }
        }

        return $result;
    }

    /**
     * @return array
     *
     * TODO: instead of array return DTO?
     */
    protected function prepareData()
    {
        $deviceId = $this->reportSettings->getDeviceId();

        $reportTitle = $this->reportSettings->getTitle() . ' – ' . self::REPORT_TITLE_ENDING;

        $viewType = $this->viewTypeNames[$this->reportSettings->getViewType()];
        $detailTypeId = $this->reportSettings->getDetailType();
        $detailType = $this->detailTypeNames[$detailTypeId];
        $serialNumber = $this->devicesData[$deviceId]['settings']['serial_number'];
        $dateStart = $this->reportSettings->getDateStart()->format('d.m.Y');
        $dateEnd = $this->reportSettings->getDateEnd()->format('d.m.Y');
        $pu = $this->devicesData[$deviceId]['settings']['address'];

        $reportDescription = sprintf(
            'Тип данных: %s; Детализация: %s; ПУ: %s, серийный номер: %s; Дата с %s по %s',
            $viewType, $detailType, $pu, $serialNumber, $dateStart, $dateEnd
        );

        if ($detailTypeId == ReportSettingsInterface::DETAIL_HALF_HOUR) {
            $data = $this->prepareMeteringDataHalfHourly($deviceId);
        } elseif ($detailTypeId == ReportSettingsInterface::DETAIL_MONTH) {
            $data = $this->prepareMeteringDataMonthlyByDays($deviceId);
        } elseif ($detailTypeId == ReportSettingsInterface::DETAIL_DAY) {
            $data = $this->prepareMeteringData($deviceId);
        }

        return [
            'title' => $reportTitle,
            'description' => [
                'value' => $reportDescription,
                'items' => [
                    'view_type' => $viewType,
                    'detail_type' => $detailType,
                    'pu' => $pu,
                    'serial_number' => $serialNumber,
                    'date_start' => $dateStart,
                    'date_end' => $dateEnd
                ]
            ],
            'info' => [
                'fio' => array_key_exists('name', $this->devicesData[$deviceId]['settings'])
                    ? $this->devicesData[$deviceId]['settings']['name']
                    : '-',
                'address' => $this->devicesData[$deviceId]['settings']['address'],
                'serial_number' => $this->devicesData[$deviceId]['settings']['serial_number'],
                'zigbee_mac' => $this->devicesData[$deviceId]['settings']['zigbee_mac'],
            ],
            'data' => $data
        ];
    }
}
