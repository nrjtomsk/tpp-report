<?php

namespace AppBundle\Service\Generator;

use AppBundle\Entity\ReportResult;
use AppBundle\Exceptions\ApiException;
use AppBundle\Model\ReportSettingsInterface;

class PescCorporateUsageXMLGenerator extends AbstractReportGenerator
{
    const PESC_XML_NAME_PREFIX = '80020_';

    /**
     * @var int
     */
    private $detailTypeId;

    /**
     * @var string
     */
    private $reportMonth;

    /**
     * @var string|int
     */
    private $reportInn;

    /**
     * @var int
     */
    private $xmlFileNumberPointer = 1;

    /**
     * Preparing data for report.
     *
     * @return array
     */
    protected function prepareData()
    {
        $this->detailTypeId = $this->reportSettings->getDetailType();
        $this->reportMonth = $this->reportSettings->getDate()->format('Ym');

        $this->initializeIncompleteFlags();
        $data = $this->prepareAllMeteringData();

        $project = reset($this->projectData);
        $this->reportInn = $project['settings']['company']['inn'];

        return [
            'title' => $project['title'],
            'inn' => $project['settings']['company']['inn'],
            'company_name' => $project['settings']['company']['name'],
            'address' => $project['settings']['company']['address'],
            'data' => $data,
        ];
    }

    /**
     * @return array
     */
    protected function prepareAllMeteringData()
    {
        $result = [];

        foreach ($this->usageData as $deviceId => $values) {
            $result[$deviceId] = [];

            if ($this->detailTypeId == ReportSettingsInterface::DETAIL_HOUR) {
                $values = $this->mergeHalfHoursIntoHours($values);
            }

            foreach ($values as $date => $value) {
                $date = \DateTime::createFromFormat(DATE_ISO8601, $date)->format('d.m.Y');
                foreach ($value as $item) {
                    $result[$deviceId][$item['type']][$date] = [];

                    if ($this->detailTypeId == ReportSettingsInterface::DETAIL_HOUR) {
                        $data = $this->prepareHourData($date, $item);
                    } else {
                        $data = $this->prepareHalfHourData($date, $item);
                    }

                    $result[$deviceId][$item['type']][$date] = $data;
                }
            }


        }

        return $result;
    }

    /**
     * @param $item
     *
     * @return array
     */
    protected function prepareHalfHourData($date, array $item): array
    {
        $result = [];
        $date = \DateTime::createFromFormat('d.m.Y', $date);
        $hasMissingData = false;


        if (!empty($item['values'])) {
            for ($i = 0; $i < 48; $i++) {
                if (!isset($item['values'][$i])) {
                    $hasMissingData = true;
                    $result[$i] = '-';
                } else {
                    $result[$i] = self::convertWattsToKilowatts($item['values'][$i]);
                }
            }
        }

        if ($hasMissingData) {
            $this->setDayReportAsIncomplete($date);
        }


        return $result;
    }

    /**
     * @param $item
     *
     * @return array
     */
    protected function prepareHourData($date, array $item): array
    {
        $result = [];
        $date = \DateTime::createFromFormat($date, 'd.m.Y');
        $hasMissingData = false;

        if (!empty($item['values'])) {
            for ($i = 0; $i < 24; $i++) {
                if (!isset($item['values'][$i])) {
                    $hasMissingData = true;
                    $result[$i] = '-';
                } else {
                    $result[$i] = self::convertWattsToKilowatts($item['values'][$i]);
                }
            }
        }

        if ($hasMissingData) {
            $this->setDayReportAsIncomplete($date);
        }

        return $result;
    }

    /**
     * Merge all half hour data it into the hours data.
     *
     * @param $values
     * @throws ApiException
     * @return array
     */
    protected function mergeHalfHoursIntoHours(array $values): array
    {
        $result = [];

        $previousHour = null;

        foreach ($values as $date => $value) {

            $result[$date] = [];

            //merge all values with same hour
            foreach ($value as $typeKey => $typeData) {

                $result[$date][$typeKey] = [];
                $result[$date][$typeKey]['values'] = [];
                $result[$date][$typeKey]['type'] = $typeData['type'];

                if (count($typeData['values']) == 48) {
                    $valueKey = 0;

                    for ($i = 0; $i < 48; $i = $i + 2) {
                        $result[$date][$typeKey]['values'][$valueKey] =  $typeData['values'][$i] + $typeData['values'][$i + 1];
                        $valueKey++;
                    }

                }
            }
        }

        return $result;
    }

    /**
     * Create XML (not XLS in this case!) file from data and save it to directory
     *
     * @param string $saveDir
     * @param array $rawData
     */
    protected function createXLSFiles(string $saveDir, array $rawData)
    {
        $this->setIncompleteFlagForZipAndJson();

        $dataForXmlByDate = [];

        $timeRanges = $this->generateTimeRanges();

        foreach ($rawData['data'] as $deviceId => $items) {

            foreach ($items['A+'] as $key => $item) {
                $dataForXmlByDate[$key][$deviceId] = $item;
            }
        }

        foreach ($dataForXmlByDate as $date => $data) {
            $xml = $this->createHeaderForXML(
                \DateTime::createFromFormat('d.m.Y', $date)->format('Ymd'),
                $rawData['inn'],
                $rawData['company_name']
            );

            $areaElement = $xml->addChild('area');
            $areaElement->addChild('inn', $rawData['inn']);
            $areaElement->addChild('name', $rawData['company_name']);

            foreach ($data as $deviceId => $measures) {
                $device = $this->devicesData[$deviceId];
                $messuringPoint = $areaElement->addChild('measuringpoint');
                /** TODO: we need to get this from device settings */

                $deviceNumber = isset($device['settings']['electricity_provider_device_number'])
                    ? $device['settings']['electricity_provider_device_number']
                    : '';

                $messuringPoint->addAttribute(
                    'code',
                    $deviceNumber
                );

                $title = isset($device['settings']['title']) ? $device['settings']['title'] : '';
                $serialNumber = isset($device['settings']['serial_number']) ? $device['settings']['serial_number'] : '';

                $messuringPoint->addAttribute(
                    'name',
                    $title . ', ' . $serialNumber
                );

                $messuringChannel = $messuringPoint->addChild('measuringchannel');
                $messuringChannel->addAttribute('code', '01');
                $messuringChannel->addAttribute('name', 'счетчик, акт. прием');

                foreach ($measures as $time => $value) {
                    $periodElement = $messuringChannel->addChild('period');
                    $periodElement->addAttribute('start', $timeRanges[$time]['start']);
                    $periodElement->addAttribute('end', $timeRanges[$time]['end']);

                    $valueElement = $periodElement->addChild('value', $value);
                    $valueElement->addAttribute('status', 0);
                }
            }

            $fileName = '80020_'
                . $rawData['inn']
                . '_'
                . \DateTime::createFromFormat('d.m.Y', $date)->format('Ymd')
                . '_' . $this->xmlFileNumberPointer
                . '.' . ReportResult::TYPE_XML;

            $this->xmlFileNumberPointer++;

            $day = \DateTime::createFromFormat('d.m.Y', $date)->format('j');

            $this->saveXMLFile($xml, $fileName, $saveDir, $this->incompleteFlags[$day]);
        }
    }

    /**
     * Initialize the inactivity flags for each day in period (month).
     */
    private function initializeIncompleteFlags()
    {
        $daysInMonth = $this->reportSettings->getDate()->format('t');

        for ($i = 1; $i <= $daysInMonth; $i++) {
            $this->incompleteFlags[$i] = false;
        }
    }

    /**
     * Set the incomplete flags for day report.
     * @param \DateTime $day
     */
    private function setDayReportAsIncomplete(\DateTime $day)
    {
        $day = $day->format('j');

        $this->incompleteFlags[$day] = true;
    }

    /**
     * @inheritdoc
     */
    protected function setIncompleteFlagForZipAndJson()
    {
        if (in_array(true, $this->incompleteFlags)) {
            $this->incompleteFlagZipJson = true;
        }
    }

    /**
     * @param string $date
     * @param string $inn
     * @param string $companyName
     * @return \SimpleXMLElement
     */
    protected function createHeaderForXML(string $date, string $inn, string $companyName): \SimpleXMLElement
    {
        $xml = new \SimpleXMLElement('<message class="80020" number="1" version="2"></message>');

        $dateTimeElement = $xml->addChild('datetime');
        $dateTimeElement->addChild('day', $date);
        $dateTimeElement->addChild('timestamp', date('YmdHis'));
        $dateTimeElement->addChild('daylightsavingtime', 0);

        $senderElement = $xml->addChild('sender');
        $senderElement->addChild('inn', $inn);
        $senderElement->addChild('name', $companyName);

        return $xml;
    }

    /**
     * @return array
     */
    protected function generateTimeRanges(): array
    {
        $result = [];
        $tempResult = [];

        $start = new \DateTime('today midnight');
        $end = new \DateTime('tomorrow midnight');

        if ($this->detailTypeId == ReportSettingsInterface::DETAIL_HOUR) {
            $interval = new \DateInterval('PT1H');
        } else {
            $interval = new \DateInterval('PT30M');
        }

        $dayRange = new \DatePeriod($start, $interval, $end);

        /**
         * @var integer $index
         * @var \DateTime $time
         */
        foreach ($dayRange as $index => $time) {
            if ($this->detailTypeId == ReportSettingsInterface::DETAIL_HOUR) {
                $tempResult[$index] = $time->format('H') . '00';
            } else {
                $tempResult[$index] = $time->format('Hi');
            }
        }

        $intervalsAmount = 48;

        if ($this->detailTypeId == ReportSettingsInterface::DETAIL_HOUR) {
            $intervalsAmount = 24;
        }

        for ($i = 0; $i < $intervalsAmount; $i++) {
            $startTime = $tempResult[$i];

            if ($i === $intervalsAmount - 1) {
                $endTime = '0000';
            } else {
                $endTime = $tempResult[$i+1];
            }

            $result[$i]['start'] = $startTime;
            $result[$i]['end'] = $endTime;
        }

        return $result;
    }


    /**
     * @param $xml
     * @param $fileName
     * @param $saveDir
     * @param bool $isIncomplete
     */
    protected function saveXMLFile(\SimpleXMLElement $xml, $fileName, $saveDir, bool $isIncomplete = false)
    {
        $xml->saveXML($saveDir . '/' . $fileName);

        $reportResult = $this->createReportResult(ReportResult::TYPE_XML, $fileName, $fileName, $isIncomplete);
        $this->manager->persist($reportResult);
        $this->resultXlsFiles[] = $reportResult;
    }

    /**
     * @inheritdoc
     */
    protected function assignJsonFileName()
    {
        //here we use the same "_1" ending because there's
        //no risk of overriding (we use the saveDir with unique name in our system)
        return self::PESC_XML_NAME_PREFIX . $this->reportInn . '_' . $this->reportMonth . '_1.' . ReportResult::TYPE_JSON;
    }

    /**
     * @inheritdoc
     */
    protected function assignZipFileName()
    {
        //here we use the same "_1" ending because there's
        //no risk of overriding (we use the saveDir with unique name in our system)
        return self::PESC_XML_NAME_PREFIX . $this->reportInn . '_' . $this->reportMonth . '_1.' . ReportResult::TYPE_ZIP;
    }

    /**
     * Prepare data from metering devices
     * @deprecated
     * Use prepareAllMeteringData instead
     *
     * @param $deviceId
     * @return array
     */
    protected function prepareMeteringData($deviceId)
    { }
}