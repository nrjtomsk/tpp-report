<?php

namespace AppBundle\Service\Generator;

use AppBundle\Entity\ReportRequest;
use AppBundle\Message\ReportMessage;
use AppBundle\Service\GenerateReportService;
use Doctrine\Common\Persistence\ObjectManager;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class ReportGeneratorConsumer implements ConsumerInterface
{
    /**
     * @var ObjectManager
     */
    private $manager;
    /**
     * @var GenerateReportService
     */
    private $reportGenerator;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ReportGeneratorConsumer constructor.
     *
     * @param ObjectManager         $manager
     * @param GenerateReportService $reportGenerator
     * @param LoggerInterface       $logger
     */
    public function __construct(
        ObjectManager $manager,
        GenerateReportService $reportGenerator,
        LoggerInterface $logger
    )
    {
        $this->manager = $manager;
        $this->reportGenerator = $reportGenerator;
        $this->logger = $logger;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg): bool
    {
        $message = ReportMessage::createFromString($msg->getBody());

        $reportRequest = $this->manager
            ->getRepository(ReportRequest::class)
            ->find($message->getId());

        // TODO: This is workaround. We need this to not generate report with errors
        if ($reportRequest->getStatus() == ReportRequest::STATUS_ERROR) {
            return true;
        }

        // We need to check if we have status that we need
        // If not, return false and w8
        if ($reportRequest->getStatus() !== ReportRequest::STATUS_HAS_DATA) {
            return false;
        }

        try {
            // If all OK start to generate report
            $this->reportGenerator->generate($reportRequest);
        } catch (\Throwable $exception) {
            $messageFormat = 'Error: %s. Project: %s. Report code: %s. In system id: %s. ';

            $this->logger->error(
                sprintf(
                    $messageFormat,
                    $exception->getMessage(),
                    $reportRequest->getProject(),
                    $reportRequest->getCode(),
                    $reportRequest->getId()
                )
            );
        }

        return true;
    }
}