<?php

namespace AppBundle\Service\Generator;

class GroupUsageCostsGenerator extends AbstractReportGenerator
{
    const REPORT_TITLE_ENDING = 'Ведомость потребления';

    /**
     * Preparing data for report.
     *
     * @return array
     */
    protected function prepareData()
    {
        $reportTitle = $this->reportSettings->getTitle() . ' – ' . self::REPORT_TITLE_ENDING;

        $customerTitle = reset($this->projectData)['title'];
        $dateStart = $this->reportSettings->getDateStart()->format('d.m.Y');
        $dateEnd = $this->reportSettings->getDateEnd()->format('d.m.Y');

        $description = sprintf(
            'Абонент: %s; Дата с %s по %s; 1 тарифный, Т1: %s руб.; 2 тарифный, Т1: %s руб., Т2: %s руб.; 3 тарифный, Т1: %s руб., Т2: %s руб., Т3: %s руб.',
            $customerTitle, $dateStart, $dateEnd,
            $this->reportSettings->getOneTariffFirst(),
            $this->reportSettings->getTwoTariffsFirst(),
            $this->reportSettings->getTwoTariffsSecond(),
            $this->reportSettings->getThreeTariffsFirst(),
            $this->reportSettings->getThreeTariffsSecond(),
            $this->reportSettings->getThreeTariffsThird()
        );

        $data = $this->prepareAllMetering();

        //sort the report data by device addresses
        uksort($data, function($a, $b) {
            $firstAddress = $this->devicesData[$a]['settings']['address'];
            $secondAddress = $this->devicesData[$b]['settings']['address'];
            $placeNumberPrefix = 'Участок №';

            if (strpos($firstAddress, $placeNumberPrefix) !== false && strpos($secondAddress, $placeNumberPrefix) !== false) {
                $a = (int) trim($firstAddress, $placeNumberPrefix);
                $b = (int) trim($secondAddress, $placeNumberPrefix);
            }

            return $a <=> $b;
        });

        return [
            'title' => $reportTitle,
            'description' => [
                'value' => $description,
                'items' => [
                    'date_start' => $dateStart,
                    'date_end' => $dateEnd,
                    'tp_1_t1' => $this->reportSettings->getOneTariffFirst(),
                    'tp_2_t1' => $this->reportSettings->getTwoTariffsFirst(),
                    'tp_2_t2' => $this->reportSettings->getTwoTariffsSecond(),
                    'tp_3_t1' => $this->reportSettings->getThreeTariffsFirst(),
                    'tp_3_t2' => $this->reportSettings->getThreeTariffsSecond(),
                    'tp_3_t3' => $this->reportSettings->getThreeTariffsThird(),
                ]
            ],
            'data' => $data
        ];
    }

    /**
     * Create XLS file from data and save it to directory
     *
     * @param string $saveDir
     * @param array  $data
     */
    protected function createXLSFiles(string $saveDir, array $data)
    {
        $phpExcelObject = $this->createPhpExcelObject();
        $sheet = $phpExcelObject->getActiveSheet();
        $sheet->setTitle(self::XLS_DEFAULT_WORKSHEET_TITLE);
        $this->autoSizeColumnsOnRange($sheet, 'A', 'B');

        $dateStart = $this->reportSettings->getDateStart();
        $dateStartInIso = substr($dateStart->format(DATE_ISO8601), 0, -2) . ':00';
        $dateStartString = $dateStart->format('d.m.Y');

        $dateEnd = $this->reportSettings->getDateEnd();
        $dateEndInIso = substr($dateEnd->format(DATE_ISO8601), 0, -2) . ':00';
        $dateEndString = $dateEnd->format('d.m.Y');

        $this->setTitleRow($sheet, $data['title']);
        $this->setSubtitleRow($sheet, $data['description']['value']);
        $this->setTableHeaderRows($sheet, $dateStartString, $dateEndString);
        //remove the selection caused by header's bold text (a PHPExcel bug)
        $sheet->setSelectedCells('A1');

        $currentRow = 0;

        $row = 9;
        $counter = 0;
        foreach ($data['data'] as $deviceId => $item) {
            $valueSetter = function ($itemIndex, $valueIndex) use ($item) {
                if ($valueIndex === 4) {
                    $valueIndex = 0;
                }

                //check for empty values in the data and show empty consumption/cost if they exist
                foreach ($item as $nodeKey => $values) {
                    if (count($values) !== 3 || in_array("", $values, true)) {
                        return '-';
                    }
                }

                return isset($item[$itemIndex][$valueIndex])
                    ? $item[$itemIndex][$valueIndex]
                    : '-';
            };

            $currentRow = $row + $counter;
            $sheet->setCellValueByColumnAndRow(0, $currentRow, ++$counter);
            $sheet->setCellValueByColumnAndRow(1, $currentRow,
                $this->devicesData[$deviceId]['settings']['address']
            );
            for ($i = 0; $i < 4; $i++) {
                $sheet->setCellValueByColumnAndRow($i + 2, $currentRow, round($valueSetter($dateStartInIso, $i + 1), 0));
                $sheet->setCellValueByColumnAndRow($i + 6, $currentRow, round($valueSetter($dateEndInIso, $i + 1), 0));
                $sheet->setCellValueByColumnAndRow($i + 10, $currentRow, round($valueSetter('usage', $i + 1), 0));
            }

            $sheet->setCellValueByColumnAndRow(
                14, $currentRow, bcmul($valueSetter('usage', 0), $this->reportSettings->getOneTariffFirst())
            );
            $sheet->setCellValueByColumnAndRow(
                15, $currentRow, $tariffTwoA = bcmul($valueSetter('usage', 1), $this->reportSettings->getTwoTariffsFirst())
            );
            $sheet->setCellValueByColumnAndRow(
                16, $currentRow, $tariffTwoB = bcmul($valueSetter('usage', 2), $this->reportSettings->getTwoTariffsSecond())
            );
            $sheet->setCellValueByColumnAndRow(
                17, $currentRow, bcadd($tariffTwoA, $tariffTwoB)
            );
            $sheet->setCellValueByColumnAndRow(
                18, $currentRow, $tariffThreeA = bcmul($valueSetter('usage', 1), $this->reportSettings->getThreeTariffsFirst())
            );
            $sheet->setCellValueByColumnAndRow(
                19, $currentRow, $tariffThreeB = bcmul($valueSetter('usage', 2), $this->reportSettings->getThreeTariffsSecond())
            );
            $sheet->setCellValueByColumnAndRow(
                20, $currentRow, '-'
            );
            $sheet->setCellValueByColumnAndRow(
                21, $currentRow, bcadd($tariffThreeA, $tariffThreeB)
            );
        }

        $lastDataRow = $currentRow;
        $currentRow++;
        $sheet->setCellValueByColumnAndRow(1, $currentRow, 'Сумма');

        for ($i = 2; $i <= 21; $i++) {
            $columnLetter = \PHPExcel_Cell::stringFromColumnIndex($i);
            $sheet->setCellValueByColumnAndRow($i, $currentRow, '=SUM(' . $columnLetter . '9:' . $columnLetter . $lastDataRow . ')');
        }

        $fileName = $this->transliterator->transliterate($data['title']);

        $this->saveXLSFile($phpExcelObject, $saveDir, $data['title'], false, $fileName);
    }

    /**
     * Prepare data from metering devices
     *
     * @param $deviceId
     *
     * @return array
     */
    protected function prepareMeteringData($deviceId) { }

    /**
     * @return array
     */
    protected function prepareAllMetering()
    {
        $startDate = $this->reportSettings->getDateStart()->format(DATE_ISO8601);
        $endDate = $this->reportSettings->getDateEnd()->format(DATE_ISO8601);

        $startDate = substr($startDate, 0, -2) . ':00';
        $endDate = substr($endDate, 0, -2) . ':00';

        $result = [];
        foreach ($this->usageData as $deviceId => $values) {
            $startDateValues = isset($values[$startDate][0]['values'])
                ? $values[$startDate][0]['values']
                : [];
            $endDateValues = isset($values[$endDate][0]['values'])
                ? $values[$endDate][0]['values']
                : [];

            $inStartOfPeriod = self::convertArrayOfWattsToKilowatts($startDateValues);
            $inEndOfPeriod = self::convertArrayOfWattsToKilowatts($endDateValues);

            $result[$deviceId][$startDate] = $inStartOfPeriod;
            $result[$deviceId][$endDate] = $inEndOfPeriod;

            $usage = [];

            if (count($inStartOfPeriod) === 0) {
                $usage = $inEndOfPeriod;
            }

            foreach ($inStartOfPeriod as $key => $value) {
                if (array_key_exists($key, $inEndOfPeriod)) {
                    $usage[$key] = bcsub($inEndOfPeriod[$key], $value);
                } else {
                    $usage[$key] = $value;
                }
            }

            $result[$deviceId]['usage'] = $usage;
        }

        return $result;
    }

    /**
     * @param \PHPExcel_Worksheet $sheet
     * @param string $dateStartString
     * @param string $dateEndString
     * @throws \PHPExcel_Exception
     */
    private function setTableHeaderRows(\PHPExcel_Worksheet $sheet, string $dateStartString, string $dateEndString)
    {
        $tableTitles1 = [
            '№ п/п',
            '№ участка',
            'Показание на начало периода', '', '', '',
            'Показание на конец периода', '', '', '',
            'Расход'
        ];
        $tableTitles2 = [
            '', '',
            $dateStartString,
            '', '', '',
            $dateEndString,
        ];
        $tableTitles3 = [
            '', '',
            'T1, кВт*ч', 'T2, кВт*ч', 'T3, кВт*ч', 'T0, кВт*ч',
            'T1, кВт*ч', 'T2, кВт*ч', 'T3, кВт*ч', 'T0, кВт*ч',
            'T1, кВт*ч', 'T2, кВт*ч', 'T3, кВт*ч', 'T0, кВт*ч'
        ];

        $sheet->fromArray($tableTitles1, null, 'A6');
        $sheet->fromArray($tableTitles2, null, 'A7');
        $sheet->fromArray($tableTitles3, null, 'A8');

        // № п\п
        $this->mergeAndVerticalAlignStyle($sheet, 'A', 6, 8);
        // № участка
        $this->mergeAndVerticalAlignStyle($sheet, 'B', 6, 8);
        // Показания на начало периода
        $sheet->mergeCells('C6:F6');
        $sheet->mergeCells('C7:F7');
        // Показания на конец периода
        $sheet->mergeCells('G6:J6');
        $sheet->mergeCells('G7:J7');
        // Расход
        $sheet->mergeCells('K6:N6');
        $sheet->mergeCells('K7:N7');

        $sheet->getCell('O6')->setValue('Однотарифный учет');
        $sheet->getCell('O7')->setValue('К оплате, руб');
        $sheet->getCell('O8')->setValue('Т0');

        $sheet->getCell('P6')->setValue('Двухтарифный учет');
        $sheet->getCell('P7')->setValue('К оплате, руб');
        $sheet->getCell('P8')->setValue('Т1');
        $sheet->getCell('Q8')->setValue('Т2');
        $sheet->getCell('R8')->setValue('Т0');
        $sheet->mergeCells('P6:R6');
        $sheet->mergeCells('P7:R7');

        $sheet->getCell('S6')->setValue('Трехтарифный учет');
        $sheet->getCell('S7')->setValue('К оплате, руб');
        $sheet->getCell('S8')->setValue('Т1');
        $sheet->getCell('T8')->setValue('Т2');
        $sheet->getCell('U8')->setValue('Т3');
        $sheet->getCell('V8')->setValue('Т0');
        $sheet->mergeCells('S6:V6');
        $sheet->mergeCells('S7:V7');

        $sheet->getStyle('A6:AA8')->getFont()->setBold(true);
    }
}