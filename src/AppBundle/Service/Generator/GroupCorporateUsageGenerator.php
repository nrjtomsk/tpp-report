<?php

namespace AppBundle\Service\Generator;

use AppBundle\Exceptions\ApiException;
use AppBundle\Model\ReportSettingsInterface;
use AppBundle\Model\TransformerStationRatio;
use PHPExcel_Worksheet;

class GroupCorporateUsageGenerator extends AbstractReportGenerator
{
    const REPORT_TITLE_ENDING = 'Потребление ПУ';

    /**
     * Preparing data for report.
     *
     * @return array
     */
    protected function prepareData()
    {
        $reportTitle = $this->reportSettings->getTitle() . ' – ' . self::REPORT_TITLE_ENDING;

        $viewType = $this->viewTypeNames[$this->reportSettings->getViewType()];
        $detailTypeId = $this->reportSettings->getDetailType();
        $detailType = $this->detailTypeNames[$detailTypeId];
        $dateStart = $this->reportSettings->getDateStart()->format('d.m.Y');
        $dateEnd = $this->reportSettings->getDateEnd()->format('d.m.Y');

        $reportDescription = sprintf(
            'Тип данных: %s; Детализация: %s; Дата с %s по %s',
            $viewType, $detailType, $dateStart, $dateEnd
        );

        if ($detailTypeId == ReportSettingsInterface::DETAIL_HALF_HOUR) {
            $data = $this->prepareAllMeteringDataHalfHourly();
        } elseif ($detailTypeId == ReportSettingsInterface::DETAIL_MONTH) {
            $data = $this->prepareAllMeteringDataMonthlyByDays();
        } elseif ($detailTypeId == ReportSettingsInterface::DETAIL_DAY) {
            $data = $this->prepareAllMeteringDataDaily();
        }

        return [
            'title' => $reportTitle,
            'description' => [
                'value' => $reportDescription,
                'items' => [
                    'view_type' => $viewType,
                    'detail_type' => $detailType,
                    'date_start' => $dateStart,
                    'date_end' => $dateEnd
                ]
            ],
            'data' => $data
        ];
    }

    /**
     * Create XLS file from data and save it to directory
     *
     * @param string $saveDir
     * @param array  $data
     */
    protected function createXLSFiles(string $saveDir, array $data)
    {
        $detailTypeId = $this->reportSettings->getDetailType();

        if ($detailTypeId == ReportSettingsInterface::DETAIL_DAY
            || $detailTypeId == ReportSettingsInterface::DETAIL_MONTH) {

            $this->createDailyOrMonthlyXLSFile($saveDir, $data);
        } elseif ($detailTypeId == ReportSettingsInterface::DETAIL_HALF_HOUR) {
            $this->createHalfHourlyXLSFile($saveDir, $data);
        }
    }

    /**
     *
     */
    protected function createDailyOrMonthlyXLSFile(string $saveDir, array $data)
    {
        $phpExcelObject = $this->createPhpExcelObject();
        $sheet = $phpExcelObject->getActiveSheet();

        $this->widthColumnsFromArray($sheet, [
            'A' => 24.0,
            'B' => 19.0,
            'C' => 19.0,
            'D' => 10.5,
            'E' => 10.0,
            'F' => 10.0,
            'G' => 10.0,
            'H' => 10.0,
        ]);

        $this->setTitleRow($sheet, $data['title']);
        $this->setSubtitleRow($sheet, $data['description']['value']);
        $this->setTableHeaders($sheet);

        $row = 10;
        foreach ($data['data'] as $deviceId => $values) {
            $startRow = $row;
            $deviceSettings = $this->devicesData[$deviceId]['settings'];

            $sheet->setCellValueByColumnAndRow(0, $row, $deviceSettings['address']);
            $sheet->getCellByColumnAndRow(1, $row)->setValueExplicit($deviceSettings['imei'], \PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->setCellValueByColumnAndRow(2, $row, $deviceSettings['serial_number']);

            foreach ($values as $date => $value) {
                $sheet->setCellValueByColumnAndRow(3, $row, '=TEXT("' . $date . '", "@")');

                $indices = ['A+', 'R+', 'R-', 'A-'];

                for ($i = 4; $i <= 7; $i++) {
                    if (array_key_exists($indices[$i - 4], $value)) {
                        $sheet->setCellValueByColumnAndRow($i, $row, $value[$indices[$i - 4]]);
                    }
                }
                $row++;
            }
            $this->mergeAndVerticalAlignStyle($sheet, 'A', $startRow, $row - 1);
            $this->mergeAndVerticalAlignStyle($sheet, 'B', $startRow, $row - 1);
            $this->mergeAndVerticalAlignStyle($sheet, 'C', $startRow, $row - 1);
        }

        $sheet->setSelectedCells('A1');
        $fileName = $this->transliterator->transliterate($data['title']);

        $this->saveXLSFile($phpExcelObject, $saveDir, $data['title'], false, $fileName);
    }

    /**
     *
     */
    protected function createHalfHourlyXLSFile(string $saveDir, array $data)
    {
        $phpExcelObject = $this->createPhpExcelObject();
        $sheet = $phpExcelObject->getActiveSheet();

        $this->widthColumnsFromArray($sheet, [
            'A' => 24.0,
            'B' => 19.0,
            'C' => 19.0,
            'D' => 22.8,
            'E' => 10.0,
            'F' => 10.0,
            'G' => 10.0,
            'H' => 10.0,
        ]);

        $this->setTitleRow($sheet, $data['title']);
        $this->setSubtitleRow($sheet, $data['description']['value']);
        $this->setTableHeaders($sheet);

        $row = 8;
        foreach ($data['data'] as $deviceId => $values) {
            $startRow = $row;
            $deviceSettings = $this->devicesData[$deviceId]['settings'];

            $sheet->setCellValueByColumnAndRow(0, $row, $deviceSettings['address']);
            $sheet->getCellByColumnAndRow(1, $row)->setValueExplicit($deviceSettings['imei'], \PHPExcel_Cell_DataType::TYPE_STRING);
            $sheet->getCellByColumnAndRow(2, $row)->setValueExplicit($deviceSettings['serial_number'], \PHPExcel_Cell_DataType::TYPE_STRING);

            foreach ($values as $date => $value) {
                $dateObject = \DateTime::createFromFormat('d.m.Y H:i', $date);
                $dateObject->modify('+30 minutes');

                $sheet->setCellValueByColumnAndRow(3, $row, '=TEXT("' . $date . ' - ' . $dateObject->format('H:i') . '", "@")');

                $indices = ['A+', 'A-', 'R+', 'R-'];

                for ($i = 4; $i <= 7; $i++) {
                    if (array_key_exists($indices[$i - 4], $value)) {
                        $sheet->setCellValueByColumnAndRow($i, $row, $value[$indices[$i - 4]]);
                    }
                }
                $row++;
            }
            $this->mergeAndVerticalAlignStyle($sheet, 'A', $startRow, $row - 1);
            $this->mergeAndVerticalAlignStyle($sheet, 'B', $startRow, $row - 1);
            $this->mergeAndVerticalAlignStyle($sheet, 'C', $startRow, $row - 1);
        }

        $sheet->setSelectedCells('A1');
        $fileName = $this->transliterator->transliterate($data['title']);

        $this->saveXLSFile($phpExcelObject, $saveDir, $data['title'], false, $fileName);
    }

    /**
     * Prepare data from metering devices
     *
     * @param $deviceId
     *
     * @return array
     */
    protected function prepareMeteringData($deviceId)
    {

    }

    /**
     *
     */
    protected function prepareAllMeteringDataHalfHourly()
    {
        if ($this->reportSettings->getViewType() == ReportSettingsInterface::VIEW_TYPE_BY_INDICATION) {
            throw new ApiException(self::ERROR_UNSUPPORTED_HALF_HOUR_REPORT, self::ERROR_UNSUPPORTED_HALF_HOUR_REPORT);
        }

        $result = [];
        $halfHourDates = $this->createHalfHourDatesRange();

        foreach ($this->usageData as $deviceId => $values) {
            $ratios = null;
            if (in_array(self::TRANSFORMER_STATION, $this->devicesData[$deviceId]['base_types'])) {
                $ratios = $this->createTransformerStationRatios($deviceId);
            }

            $halfHoursDailyStart = 0;

            foreach ($values as $dayDate => $dataSets) {
                for ($i = 0; $i < 48; $i++) {
                    foreach ($dataSets as $dataSet) {
                        $rawHalfHourValue = $dataSet['values'][$i];

                        if ($ratios instanceof TransformerStationRatio) {
                            $value = $this->changeTransformerStationMeteringForSingleValue($rawHalfHourValue, $ratios);
                        }

                        $handledValue = isset($value) ? $this::convertWattsToKilowatts($value) : 0;

                        if ($halfHoursDailyStart == count($halfHourDates)) {
                            $result[$deviceId][$halfHourDates[$halfHoursDailyStart - 1]][$dataSet['type']] = $handledValue;
                        } elseif ($halfHoursDailyStart > count($halfHourDates)) {
                            break;
                        } else {
                            $result[$deviceId][$halfHourDates[$halfHoursDailyStart + $i]][$dataSet['type']] = $handledValue;
                        }
                    }

                    if ($i === 47) {
                        $halfHoursDailyStart += 48;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Prepare all metering data for a daily report.
     *
     * @return array
     */
    protected function prepareAllMeteringDataDaily()
    {
        $result = [];

        foreach ($this->usageData as $deviceId => $values) {
            $ratios = null;
            if (in_array(self::TRANSFORMER_STATION, $this->devicesData[$deviceId]['base_types'])) {
                $ratios = $this->createTransformerStationRatios($deviceId);
            }

            foreach ($values as $date => $item) {
                $date = \DateTime::createFromFormat(DATE_ISO8601, $date)->format('d.m.Y');

                $tmp = [];
                foreach ($item as $value) {
                    $valueData = $value['values'];

                    if ($ratios instanceof TransformerStationRatio) {
                        $valueData = $this->changeTransformerStationMeteringData($valueData, $ratios);
                    }

                    $tmp[$value['type']] = isset($valueData[0])
                        ? $this::convertWattsToKilowatts($valueData[0])
                        : 0;
                }
                $result[$deviceId][$date] = $tmp;
            }
        }

        return $result;
    }

    /**
     * Prepare all metering data for the months by given days.
     *
     * @return array
     */
    protected function prepareAllMeteringDataMonthlyByDays()
    {
        $result = [];

        foreach ($this->usageData as $deviceId => $values) {
            $ratios = null;

            if (in_array(self::TRANSFORMER_STATION, $this->devicesData[$deviceId]['base_types'])) {
                $ratios = $this->createTransformerStationRatios($deviceId);
            }

            foreach ($values as $date => $item) {
                $monthYearDate = \DateTime::createFromFormat(DATE_ISO8601, $date)->format('m.Y');

                $tmp = [];

                foreach ($item as $value) {
                    $valueData = $value['values'];

                    if ($ratios instanceof TransformerStationRatio) {
                        $valueData = $this->changeTransformerStationMeteringData($valueData, $ratios);
                    }

                    $tmp[$value['type']] = isset($valueData[0])
                        ? $this::convertWattsToKilowatts($valueData[0])
                        : 0;
                }

                if (!array_key_exists($deviceId, $result)) {
                    $result[$deviceId] = [];
                }

                if (array_key_exists($monthYearDate, $result[$deviceId])) {
                    $indices = ['A+', 'A-', 'R+', 'R-'];
                    foreach ($indices as $index) {
                        if (array_key_exists($index, $result[$deviceId][$monthYearDate]) && array_key_exists($index, $tmp)) {
                            $result[$deviceId][$monthYearDate][$index] += $tmp[$index];
                        } else {
                            $result[$deviceId][$monthYearDate][$index] = 0;
                        }
                    }
                } else {
                    $result[$deviceId][$monthYearDate] = $tmp;
                }
            }


        }

        return $result;
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     * @throws \PHPExcel_Exception
     */
    private function setTableHeaders(PHPExcel_Worksheet $sheet)
    {
        $tableTitles1 = [
            'Название организации',
            'IMEI',
            'Серийный номер',
            'Дата',
            'Тип'
        ];

        if ($this->reportSettings->getDetailType() == ReportSettingsInterface::DETAIL_HALF_HOUR) {
            $tableTitles2 = [
                '','','','',
                'A+', 'A-', 'R+', 'R-'
            ];

            $sheet->fromArray($tableTitles1, null, 'A6');
            $sheet->fromArray($tableTitles2, null, 'A7');

            $this->mergeAndVerticalAlignStyle($sheet, 'A', 6, 7);
            $this->mergeAndVerticalAlignStyle($sheet, 'B', 6, 7);
            $this->mergeAndVerticalAlignStyle($sheet, 'C', 6, 7);
            $this->mergeAndVerticalAlignStyle($sheet, 'D', 6, 7);

            $sheet->mergeCells('E6:H6');
        } else {
            $tableTitles2 = [
                '','','','',
                'A+', 'R+', 'R-', 'A-'
            ];

            $tableTitles3 = [
                '', '', '', '',
                'Тариф', 'Тариф', 'Тариф', 'Тариф',
            ];
            $tableTitles4 = [
                '', '', '', '',
                'Сумма', 'Сумма', 'Сумма', 'Сумма',
            ];

            $sheet->fromArray($tableTitles1, null, 'A6');
            $sheet->fromArray($tableTitles2, null, 'A7');
            $sheet->fromArray($tableTitles3, null, 'A8');
            $sheet->fromArray($tableTitles4, null, 'A9');

            $this->mergeAndVerticalAlignStyle($sheet, 'A', 6, 9);
            $this->mergeAndVerticalAlignStyle($sheet, 'B', 6, 9);
            $this->mergeAndVerticalAlignStyle($sheet, 'C', 6, 9);
            $this->mergeAndVerticalAlignStyle($sheet, 'D', 6, 9);

            $sheet->mergeCells('E6:H6');
            $sheet->getStyle('A6:H9')->getFont()->setBold(true);
        }
    }
}
