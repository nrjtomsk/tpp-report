<?php

namespace AppBundle\Service\Generator;

use AppBundle\Entity\ReportRequest;
use AppBundle\Service\TransliteratorService;
use Doctrine\Common\Persistence\ObjectManager;
use Liuggio\ExcelBundle\Factory as PHPExcelFactory;
use Symfony\Component\Filesystem\Filesystem;

interface ReportGeneratorInterface
{
    const ROOT_DIR = 'data/api/v1';
    const PROJECTS_FILE_NAME = 'projects.json';
    const DEVICES_DIRECTORY = 'projects/%s';
    const DEVICES_FILE_NAME = 'devices.json';
    const STATIONS_FILE_NAME = 'transformer_stations.json';
    const METERING_DIRECTORY = 'projects/%s/devices/%s/capabilities/electricity_metering/data';

    /**
     * ReportGeneratorInterface constructor.
     *
     * @param Filesystem $filesystem
     * @param PHPExcelFactory $phpExcelFactory
     * @param ObjectManager $manager
     * @param TransliteratorService $transliterator
     */
    public function __construct(
        Filesystem $filesystem,
        PHPExcelFactory $phpExcelFactory,
        ObjectManager $manager,
        TransliteratorService $transliterator
    );

    /**
     * Generates report files based on Report
     *
     * @param ReportRequest $reportRequest
     *
     * @return mixed
     */
    public function generate(ReportRequest $reportRequest);
}