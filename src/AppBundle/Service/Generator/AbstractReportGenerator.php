<?php

declare(strict_types = 1);

namespace AppBundle\Service\Generator;

use AppBundle\Entity\ReportRequest;
use AppBundle\Entity\ReportResult;
use AppBundle\Exceptions\ErrorOnSaveDirectoryCreationException;
use AppBundle\Exceptions\NoSuchDetailTypeException;
use AppBundle\Exceptions\NoSuchFileOrDirException;
use AppBundle\Model\ReportSettingsInterface;
use AppBundle\Model\SingleDateReportSettingsInterface;
use AppBundle\Model\TransformerStationRatio;
use AppBundle\Service\TransliteratorService;
use Doctrine\Common\Persistence\ObjectManager;
use Liuggio\ExcelBundle\Factory as PHPExcelFactory;
use PHPExcel;
use PHPExcel_Worksheet;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Filesystem\Filesystem;
use ZipArchive;

abstract class AbstractReportGenerator implements ReportGeneratorInterface
{
    const TRANSFORMER_STATION = 'transformer_el_meter';
    const XLS_DEFAULT_WORKSHEET_TITLE = 'Лист1';

    const ERROR_UNSUPPORTED_HALF_HOUR_REPORT = 'Half hourly report with view by indication is not supported.';

    /**
     * @var array
     */
    protected $projectData;

    /**
     * @var array
     */
    protected $stationData;

    /**
     * @var array
     */
    protected $devicesData;

    /**
     * @var array
     */
    protected $usageData;

    /**
     * @var ReportSettingsInterface
     */
    protected $reportSettings;

    /**
     * @var string
     */
    protected $dataDirectory;

    /**
     * @var ReportRequest
     */
    protected $reportRequest;

    /**
     * @var string
     */
    protected $saveDirectoryName;

    /**
     * @var string
     */
    protected $saveDirectoryPath;

    /**
     * @var array
     */
    protected $resultXlsFiles;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var PHPExcelFactory
     */
    protected $phpExcelFactory;

    /**
     * @var ObjectManager
     */
    protected $manager;

    /**
     * @var TransliteratorService
     */
    protected $transliterator;

    /**
     * @var array
     */
    protected $incompleteFlags = [];

    /**
     * @var bool
     */
    protected $incompleteFlagZipJson = false;

    /**
     * @var array
     */
    protected $detailsFiles = [
        ReportSettingsInterface::VIEW_TYPE_BY_CONSUMPTION => 'half_hour_data.json',
        ReportSettingsInterface::VIEW_TYPE_BY_INDICATION => 'daily_data.json'
    ];

    /**
     * @var array
     */
    protected $detailTypeNames = [
        ReportSettingsInterface::DETAIL_HALF_HOUR => 'по 30 минут',
        ReportSettingsInterface::DETAIL_HOUR => 'по 60 минут',
        ReportSettingsInterface::DETAIL_DAY => 'по дням',
        ReportSettingsInterface::DETAIL_MONTH => 'по месяцам',
    ];
    /**
     * @var array
     */
    protected $viewTypeNames = [
        ReportSettingsInterface::VIEW_TYPE_BY_CONSUMPTION => 'по потреблению',
        ReportSettingsInterface::VIEW_TYPE_BY_INDICATION => 'по показанию',
    ];

    /**
     * @var array
     */
    protected $lettersRange;

    /**
     * ReportGeneratorInterface constructor.
     *
     * @param Filesystem $filesystem
     * @param PHPExcelFactory $phpExcelFactory
     * @param ObjectManager $manager
     * @param TransliteratorService $transliterator
     */
    public function __construct(
        Filesystem $filesystem,
        PHPExcelFactory $phpExcelFactory,
        ObjectManager $manager,
        TransliteratorService $transliterator
    ) {
        $this->filesystem = $filesystem;
        $this->phpExcelFactory = $phpExcelFactory;
        $this->manager = $manager;
        $this->transliterator = $transliterator;

        $this->lettersRange = range('A', 'Z');
    }

    /**
     * Generates report files based on Report
     *
     * @param ReportRequest $reportRequest
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function generate(ReportRequest $reportRequest)
    {
        $this->reportRequest = $reportRequest;
        $this->reportSettings = $reportRequest->getSettings();

        $this->setReportStatus(ReportRequest::STATUS_IN_PROGRESS);

        $pathInfo = pathinfo($reportRequest->getDataFile()->getFilePath());
        $saveDir = $this->prepareSaveDirectory($pathInfo['dirname'], $pathInfo['filename']);

        try {
            $this->setMathScaleForReportByReportCode($reportRequest->getCode());
            $this->parseDataFiles($pathInfo);
            $data = $this->prepareData();
            $this->createXLSFiles($saveDir, $data);
            $this->createJsonFiles($saveDir, $data);
            $this->createZipFile($saveDir);

            $this->setReportStatus(ReportRequest::STATUS_DONE);
        } catch (\Throwable $e) {
            /** TODO: Add and handle specific exceptions on reports creation */
            $this->setReportStatus(ReportRequest::STATUS_ERROR);
            throw $e;
        } finally {
            $this->manager->flush();
        }
    }

    /**
     * Call the bcscale() with a correct argument
     * depending on the report code.
     *
     * @param string $reportCode
     * @return void
     */
    private final function setMathScaleForReportByReportCode(string $reportCode)
    {
        switch ($reportCode) {
            case ReportRequest::CODE_PESC_CORPORATE_USAGE:
                bcscale(3);
                break;
            case ReportRequest::CODE_RKS_ENERGO_CORPORATE_USAGE:
                bcscale(3);
                break;
            case ReportRequest::CODE_DEVICE_USAGE:
                bcscale(3);
                break;
            case ReportRequest::CODE_GROUP_USAGE:
                bcscale(3);
                break;
            default:
                bcscale(2);
                break;
        }
    }

    /**
     * @param $status
     */
    protected function setReportStatus($status)
    {
        $this->reportRequest->setStatus($status);
        $this->manager->persist($this->reportRequest);
    }

    /**
     * @param $dataDir
     * @param $name
     * @return string
     * @throws \Exception
     */
    protected function prepareSaveDirectory($dataDir, $name)
    {
        $baseSaveDirectory = $dataDir.DIRECTORY_SEPARATOR
            .'..'.DIRECTORY_SEPARATOR
            .'results';

        $saveDirectory = $baseSaveDirectory.DIRECTORY_SEPARATOR.$name;

        $this->saveDirectoryName = $name;

        try {
            $this->filesystem->mkdir($saveDirectory);
        } catch (\Exception $e) {
            throw new ErrorOnSaveDirectoryCreationException($e);
        }

        return realpath($saveDirectory);
    }

    /**
     * @param $pathInfo
     * @throws \Exception
     */
    protected function parseDataFiles($pathInfo)
    {
        $this->dataDirectory = $pathInfo['dirname'].DIRECTORY_SEPARATOR.$pathInfo['filename'];
        $this->checkIsExists($this->dataDirectory);
        $this->projectData = $this->getProjectData();
        $this->stationData = $this->getStationData();
        $this->devicesData = $this->getDeviceData();
        $this->usageData = $this->getUsageData();
    }

    /**
     * @param $dirOrFile
     *
     * @throws \Exception
     */
    protected function checkIsExists($dirOrFile)
    {
        if (!$this->filesystem->exists($dirOrFile)) {
            throw new NoSuchFileOrDirException($dirOrFile);
        }
    }

    /**
     * @return array
     *
     * @throws \Exception
     */
    protected function getProjectData()
    {
        $result = [];

        $projectFile = $this->dataDirectory
            .DIRECTORY_SEPARATOR
            .self::ROOT_DIR
            .DIRECTORY_SEPARATOR
            .self::PROJECTS_FILE_NAME;
        $this->checkIsExists($projectFile);

        $reportData = $this->readFile($projectFile);

        foreach ($reportData['projects'] as $project) {
            $result[$project['id']] = $project;
        }

        return $result;
    }

    /**
     * @param $file
     *
     * @return array
     */
    protected function readFile($file)
    {
        $handle = fopen($file, 'r');
        $contents = fread($handle, filesize($file));
        fclose($handle);

        return json_decode($contents, true);
    }

    /**
     * This method is need for reports with transformer stations data
     *
     * @return array
     */
    protected function getStationData()
    {
        return [];
    }

    /**
     * @throws \Exception
     * TODO: May be we need to validate each device before save it.
     */
    protected function getDeviceData()
    {
        $result = [];

        foreach ($this->projectData as $project) {
            $devicesFile = $this->dataDirectory
                .DIRECTORY_SEPARATOR
                .self::ROOT_DIR
                .DIRECTORY_SEPARATOR
                .sprintf(self::DEVICES_DIRECTORY, $project['id'])
                .DIRECTORY_SEPARATOR
                .self::DEVICES_FILE_NAME;
            $this->checkIsExists($devicesFile);

            $devices = $this->readFile($devicesFile);

            foreach ($devices['devices'] as $device) {
                $result[$device['id']] = $device;
            }
        }

        return $result;
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getUsageData(): array
    {
        $result = [];

        foreach ($this->devicesData as $device) {
            // Some of devices is internet_gateway. For now we don't need them
            // And now we know, that we need only el_meter devices
            if (!in_array('el_meter', $device['base_types'])) {
                continue;
            }

            $deviceId = $device['id'];
            $dataFile = $this->dataDirectory
                .DIRECTORY_SEPARATOR
                .self::ROOT_DIR
                .DIRECTORY_SEPARATOR
                .sprintf(self::METERING_DIRECTORY, $device['project_id'], $deviceId)
                .DIRECTORY_SEPARATOR
                .$this->detailsFiles[$this->reportSettings->getViewType()];

            $this->checkIsExists($dataFile);

            $meteringData = $this->readFile($dataFile);

            foreach ($meteringData['items'] as $item) {
                $dateObject = \DateTime::createFromFormat(DATE_ISO8601, $item['date']);

                if ($this->reportSettings instanceof SingleDateReportSettingsInterface) {
                    $dateOfRequestedMonth = $this->reportSettings->getDate();
                    if ($dateObject->format('m.Y') === $dateOfRequestedMonth->format('m.Y')) {
                        $result[$deviceId][$item['date']] = $this->prepareRawMeteringData($item['data']);
                    }
                } elseif ($this->reportSettings instanceof ReportSettingsInterface) {
                    if ($dateObject >= $this->reportSettings->getDateStart() && $dateObject <= $this->reportSettings->getDateEnd()) {
                        $result[$deviceId][$item['date']] = $this->prepareRawMeteringData($item['data']);
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param array $rawData
     *
     * @return array
     * @throws \Exception
     */
    protected function prepareRawMeteringData(array $rawData): array
    {
        switch ($this->reportSettings->getDetailType()) {
            case ReportSettingsInterface::DETAIL_HALF_HOUR:
                return $this->prepareRawMeteringDataForHalfHour($rawData);
                break;
            case ReportSettingsInterface::DETAIL_HOUR:
                //here we use the same logic as in DETAIL_HALF_HOUR, there's no custom input data for a hour
                return $this->prepareRawMeteringDataForHalfHour($rawData);
                break;
            case ReportSettingsInterface::DETAIL_DAY:
                return $this->prepareRawMeteringDataForDay($rawData);
                break;
            case ReportSettingsInterface::DETAIL_MONTH:
                //here we use the same logic as in DETAIL_DAY, there's no custom input data for a month
                return $this->prepareRawMeteringDataForDay($rawData);
                break;
            default:
                throw new NoSuchDetailTypeException(
                    $this->reportSettings->getDetailType(),
                    $this->reportRequest
                );
                break;
        }
    }

    /**
     * @param array $rawData
     *
     * @return array
     */
    protected function prepareRawMeteringDataForHalfHour(array $rawData): array
    {
        return $rawData;
    }

    /**
     * We need this method to calculate data based on tariffs.
     * Tariffs ranges for two tariffs:
     *  * День (T1): (7, 22)
     *  * Ночь (T2): (23, 24), (0, 6)
     * Tariffs ranges for three tariffs:
     *  * День (T1): (10, 16), (21, 22)
     *  * Ночь (T2): (0, 6), (23, 24)
     *  * Пик (T3): (7, 9), (17, 20)
     *
     * NOTE: For now we use only two tariffs ranges.
     *
     * @param array $rawData
     *
     * @return array
     */
    protected function prepareRawMeteringDataForDay(array $rawData): array
    {
        if ($this->reportSettings->getViewType() === ReportSettingsInterface::VIEW_TYPE_BY_INDICATION) {
            return $rawData;
        }

        $result = [];
        $dayRange = range(7, 22);

        foreach ($rawData as $item) {
            if (!empty($item['values'])) {
                $values = [
                    0 => '0',
                    1 => '0',
                    2 => '0',
                ];

                for ($i = 0; $i < 24; $i++) {
                    $numberGetter = function (array $values, int $index): string {
                        return array_key_exists($index, $values)
                            ? (string)$values[$index]
                            : '0';
                    };
                    $dataValues = $item['values'];

                    $hourSum = bcadd(
                        $numberGetter($dataValues, $i * 2),
                        $numberGetter($dataValues, $i * 2 + 1)
                    );

                    $tariffIndex = in_array($i, $dayRange) ? 1 : 2;
                    $values[$tariffIndex] = bcadd($values[$tariffIndex], $hourSum);
                    $values[0] = bcadd($values[0], $hourSum);
                }

                $result[] = [
                    'type' => $item['type'],
                    'values' => $values,
                ];
            }
        }

        return $result;
    }

    /**
     * Preparing data for report.
     *
     * @return array
     */
    abstract protected function prepareData();

    /**
     * Create XLS file from data and save it to directory
     *
     * @param string $saveDir
     * @param array $data
     */
    abstract protected function createXLSFiles(string $saveDir, array $data);

    /**
     * Save generated XLS file
     *
     * @param PHPExcel $phpExcelObject
     * @param string $saveDir
     * @param string|null $title
     * @param bool $isIncomplete
     * @param string $fileName
     */
    protected function saveXLSFile(
        PHPExcel $phpExcelObject,
        string $saveDir,
        string $title = null,
        bool $isIncomplete = false,
        string $fileName = null
    )
    {
        if (!$fileName) {
            $fileName = uniqid();
        }

        $fileName = $fileName . '.' . ReportResult::TYPE_XLS;

        $this->phpExcelFactory->createWriter($phpExcelObject)->save(
            $saveDir . DIRECTORY_SEPARATOR . $fileName
        );

        $reportResult = $this->createReportResult(ReportResult::TYPE_XLS, $fileName, $title, $isIncomplete);
        $this->manager->persist($reportResult);
        $this->resultXlsFiles[] = $reportResult;
    }

    /**
     * @param $saveDir
     * @param $data
     */
    protected function createJsonFiles($saveDir, $data)
    {
        $fileName = $this->assignJsonFileName();
        $jsonString = json_encode($data, JSON_UNESCAPED_UNICODE);

        $this->filesystem->dumpFile(
            $saveDir.DIRECTORY_SEPARATOR.$fileName,
            $jsonString
        );

        $reportResult = $this->createReportResult(
            ReportResult::TYPE_JSON,
            $fileName,
            ReportResult::TYPE_JSON_TITLE,
            $this->incompleteFlagZipJson
        );

        $this->manager->persist($reportResult);
    }

    /**
     * Override this method in a custom Generator class
     * to set the custom name for the report's JSON file.
     *
     * @return string
     */
    protected function assignJsonFileName()
    {
        return uniqid() . '.' . ReportResult::TYPE_JSON;
    }

    /**
     * @param $type
     * @param $fileName
     * @param null $title
     * @param bool $isIncomplete
     *
     * @return ReportResult
     */
    protected function createReportResult($type, $fileName, $title = null, $isIncomplete = false): ReportResult
    {
        $reportResult = new ReportResult();

        $reportResult->setType($type);
        $reportResult->setFile($this->saveDirectoryName.DIRECTORY_SEPARATOR.$fileName);
        $reportResult->setTitle($title);
        $reportResult->setIncomplete($isIncomplete);

        $reportResult->setReportRequest($this->reportRequest);

        return $reportResult;
    }

    /**
     * Prepare data from metering devices
     * @deprecated
     * Use prepareAllMeteringData instead
     *
     * @param $deviceId
     * @return array
     */
    abstract protected function prepareMeteringData($deviceId);

    /**
     * Prepare all metering data for reports
     *
     * @return array
     */
    protected function prepareAllMeteringData()
    {

    }

    /**
     * Creating ZIP file with XLS files
     *
     * @param $saveDir
     */
    protected function createZipFile($saveDir)
    {
        if ($this->resultXlsFiles !== null) {
            $tmpDir = $saveDir . DIRECTORY_SEPARATOR . 'tmp';
            $this->filesystem->mkdir($tmpDir);

            $this->copyResultFilesToTempDirectory($tmpDir, $saveDir);
            $zip = $this->createZipArchive($saveDir);
            $this->addFilesToZipArchive($tmpDir, $zip);

            $reportResult = $this->createReportResult(
                ReportResult::TYPE_ZIP,
                basename($zip->filename),
                ReportResult::TYPE_ZIP_TITLE,
                $this->incompleteFlagZipJson
            );

            $this->manager->persist($reportResult);

            $zip->close();
            $this->filesystem->remove($tmpDir);
        }
    }

    /**
     * Copy all xls files from save directory to temp directory
     *
     * @param string $tmpDir
     * @param string $saveDir
     */
    protected function copyResultFilesToTempDirectory(string $tmpDir, string $saveDir)
    {
        /** @var ReportResult $file */
        foreach ($this->resultXlsFiles as $file) {
            $fileName = substr($file->getFile(), strpos($file->getFile(), '/') + 1);
            $this->filesystem->copy(
                $saveDir . DIRECTORY_SEPARATOR . $fileName,
                $tmpDir . DIRECTORY_SEPARATOR . $fileName
            );
        }
    }

    /**
     * Create empty zip archive
     *
     * @param string $saveDir
     *
     * @return ZipArchive
     */
    private function createZipArchive(string $saveDir): ZipArchive
    {
        $zipFileName = $this->assignZipFileName();

        $zipFile = $saveDir . DIRECTORY_SEPARATOR . $zipFileName;
        $zip = new ZipArchive();
        $zip->open($zipFile, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        return $zip;
    }

    /**
     * Override this method in a custom Generator class
     * to set the custom name for the report's ZIP file.
     *
     * @return string
     */
    protected function assignZipFileName()
    {
        return uniqid() . '.' . ReportResult::TYPE_ZIP;
    }

    /**
     * Add files from temp directory to zip archive
     *
     * @param string     $tmpDir
     * @param ZipArchive $zip
     */
    private function addFilesToZipArchive(string $tmpDir, ZipArchive $zip)
    {
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($tmpDir),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file) {
            if (!$file->isDir()) {
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($tmpDir) + 1);
                $zip->addFile($filePath, $relativePath);
            }
        }
    }

    /**
     * @param float $watts
     * @return string
     */
    protected static function convertWattsToKilowatts(float $watts = null): string
    {
        if ($watts === null) {
            return '';
        }

        return bcdiv((string)$watts, (string)1000);
    }

    /**
     * @param array $arrayOfWatts
     * @return array
     */
    protected static function convertArrayOfWattsToKilowatts(array $arrayOfWatts): array
    {
        return array_map(
            [AbstractReportGenerator::class, 'convertWattsToKilowatts'],
            $arrayOfWatts
        );
    }

    /**
     * Create PHPExcel object with some setup
     *
     * @return PHPExcel
     * @throws \PHPExcel_Exception
     */
    protected function createPhpExcelObject(): PHPExcel
    {
        $phpExcelObject = $this->phpExcelFactory->createPHPExcelObject();

        $phpExcelObject->getDefaultStyle()
            ->getFont()
            ->setName('Arial')
            ->setSize(12);

        return $phpExcelObject;
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     *
     * @throws \PHPExcel_Exception
     */
    protected function styleForTitleRow(PHPExcel_Worksheet $sheet)
    {
        $sheet->mergeCells('A1:E1');
        $sheet->getStyle('A1:E1')->getFont()->setSize(15);
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     * @param string $value
     */
    protected function setTitleRow(PHPExcel_Worksheet $sheet, string $value)
    {
        $sheet->setCellValueByColumnAndRow(0, 1, $value);
        $this->styleForTitleRow($sheet);
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     * @param string $value
     */
    protected function setSubtitleRow(PHPExcel_Worksheet $sheet, string $value)
    {
        $sheet->setCellValueByColumnAndRow(0, 3, $value);
        $this->styleForSubtitleRow($sheet);
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     *
     * @throws \PHPExcel_Exception
     */
    protected function styleForSubtitleRow(PHPExcel_Worksheet $sheet)
    {
        $sheet->mergeCells('A3:E3');
        $sheet->getStyle('A3:E3')->getFont()->setItalic(true);
    }

    /**
     * @param \PHPExcel_Worksheet $sheet
     * @param string              $columnLetter
     * @param int                 $start
     * @param int                 $end
     *
     * @throws \PHPExcel_Exception
     */
    protected function mergeAndVerticalAlignStyle(\PHPExcel_Worksheet $sheet, string $columnLetter, int $start, int $end)
    {
        $format = strtoupper($columnLetter) . '%d:' . strtoupper($columnLetter) . '%d';
        $range = sprintf($format, $start, $end);

        $sheet->mergeCells($range);
        $sheet->getStyle($range)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     * @param string $start
     * @param string $end
     */
    protected function autoSizeColumnsOnRange(PHPExcel_Worksheet $sheet, string $start, string $end)
    {
        foreach (range($start, $end) as $item) {
            $sheet->getColumnDimension(strtoupper($item))->setAutoSize(true);
        }
    }

    /**
     * Set width for column range.
     * NOTE: width in real excel document will be width * 7. Works for Numbers on OSX
     *
     * @param PHPExcel_Worksheet $sheet
     * @param string $start
     * @param string $end
     * @param float $width
     */
    protected function widthColumnsOnRange(PHPExcel_Worksheet $sheet, string $start, string $end, float $width)
    {
        foreach (range($start, $end) as $item) {
            $sheet->getColumnDimension(strtoupper($item))->setWidth($width);
        }
    }

    /**
     * Set width from array.
     * Array value need to be with `column letter => width` value pairs
     *
     * NOTE: width in real excel document will be width * 7. Works for Numbers on OSX
     *
     * @param PHPExcel_Worksheet $sheet
     * @param array $values
     */
    protected function widthColumnsFromArray(\PHPExcel_Worksheet $sheet, array $values)
    {
        foreach ($values as $column => $width) {
            $sheet->getColumnDimension(strtoupper($column))->setWidth($width);
        }
    }

    /**
     * @param string $deviceId
     *
     * @return TransformerStationRatio
     */
    protected function createTransformerStationRatios(string $deviceId): TransformerStationRatio
    {
        $settings = $this->devicesData[$deviceId]['settings'];

        return new TransformerStationRatio(
            $settings['current_transformation_ratio'],
            $settings['voltage_transformation_ratio'],
            $settings['percent_of_line_loss_rm'],
            $settings['percent_of_line_loss_rp']
        );
    }

    /**
     * We need to change transformer station data based on ratios
     *
     * @param $valueData
     * @param $ratios
     *
     * @return array
     */
    protected function changeTransformerStationMeteringData($valueData, TransformerStationRatio $ratios)
    {
        $result = [];
        foreach ($valueData as $index => $value) {
            $result[$index] = $this->bcMultiplyAll(
                (float)$value,
                (float)$ratios->getCurrentRatio(),
                (float)$ratios->getVoltageRatio(),
                (float)$ratios->getLineLossRM(),
                (float)$ratios->getLineLossRP()
            );
        }

        return $result;
    }

    /**
     * This is the same method as changeTransformerStationMeteringData()
     * but this one returns a single value, not an array.
     *
     * @param int $value
     * @param $ratios
     *
     * @return array
     */
    protected function changeTransformerStationMeteringForSingleValue($value, TransformerStationRatio $ratios)
    {
        $value = $this->bcMultiplyAll(
                (float)$value,
                (float)$ratios->getCurrentRatio(),
                (float)$ratios->getVoltageRatio(),
                (float)$ratios->getLineLossRM(),
                (float)$ratios->getLineLossRP()
            );

        return $value;
    }

    /**
     * @param float[] ...$numbers
     *
     * @return string
     */
    protected function bcMultiplyAll(float ...$numbers): string
    {
        $result = '1';

        foreach ($numbers as $number) {
            $result = bcmul($result, (string)$number);
        }

        return $result;
    }

    /**
     * Set the incomplete flags for ZIP/JSON files
     * using the incomplete flags of summary reports.
     */
    protected function setIncompleteFlagForZipAndJson()
    {
        //implement this logic in the child classes
        return null;
    }

    /**
     * Create an array with dates by report's detail type.
     *
     * @return array
     */
    protected function createHalfHourDatesRange()
    {
        if (!$this->reportSettings->getDateStart() || !$this->reportSettings->getDateEnd()) {
            return null;
        }

        $dateStart = $this->reportSettings->getDateStart();
        $dateEnd = $this->reportSettings->getDateEnd();

        //added to prevent the empty sheet while the same dates are at start and end
        if ($dateStart == $dateEnd) {
            $dateEnd->modify('+1 day');
        }

        $dateInterval = new \DatePeriod($dateStart, new \DateInterval('PT30M'), $dateEnd);
        $dates = [];

        /** @var \DateTime $date */
        foreach ($dateInterval as $date) {
            $dates[] = $date->format('d.m.Y H:i');
        }

        return $dates;
    }
}