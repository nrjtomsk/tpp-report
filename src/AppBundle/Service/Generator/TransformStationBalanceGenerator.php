<?php

namespace AppBundle\Service\Generator;

use AppBundle\Entity\ReportResult;
use AppBundle\Model\ReportSettingsInterface;
use PHPExcel_Worksheet;

class TransformStationBalanceGenerator extends AbstractReportGenerator
{
    const REPORT_TITLE_ENDING = 'Баланс подстанции';

    /**
     * @var int
     */
    private $detailTypeId;

    /**
     * @var int
     */
    private $additionalTariffsInSelectionNum;

    /**
     * {@inheritdoc}
     */
    protected function prepareData()
    {
        $station = current($this->projectData)['title'];

        $reportTitle = $this->reportSettings->getTitle();

        $this->detailTypeId = $this->reportSettings->getDetailType();
        $detailType = $this->detailTypeNames[$this->detailTypeId];
        $dateStart = $this->reportSettings->getDateStart()->format('d.m.Y');
        $dateEnd = $this->reportSettings->getDateEnd()->format('d.m.Y');

        $reportDescription = sprintf(
            'Дата с %s по %s; Детализация: %s; Подстанция: %s',
            $dateStart, $dateEnd, $detailType, $station
        );

        $data = $this->prepareAllMeteringData();

        return [
            'title' => $reportTitle,
            'description' => [
                'value' => $reportDescription,
                'items' => [
                    'date_start' => $dateStart,
                    'date_end' => $dateEnd,
                    'detail_type' => $detailType,
                    'station' => $station
                ]
            ],
            'data' => $data
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function createXLSFiles(string $saveDir, array $data)
    {
        if ($this->detailTypeId === ReportSettingsInterface::DETAIL_DAY) {
            $this->createDaysXLSFile($saveDir, $data);
        } elseif ($this->detailTypeId === ReportSettingsInterface::DETAIL_HALF_HOUR) {
            $this->createHalfHoursXLSFile($saveDir, $data);
        }
    }

    /**
     * Create XLS report detailed by days.
     *
     * @param string $saveDir
     * @param array $data
     * @throws \PHPExcel_Exception
     */
    private function createDaysXLSFile(string $saveDir, array $data)
    {
        $phpExcelObject = $this->createPhpExcelObject();
        $sheet = $phpExcelObject->getActiveSheet();

        $sheet->getDefaultColumnDimension()->setWidth(25.67);
        $sheet->getColumnDimension('A')->setWidth(28.33);
        $sheet->getColumnDimension('B')->setWidth(18.67);
        $sheet->getColumnDimension('C')->setWidth(21.33);
        $sheet->getColumnDimension('D')->setWidth(10.67);

        $sheet->freezePane('A7');

        foreach (range('a', 'd') as $item) {
            $sheet->getColumnDimension(strtoupper($item))->setAutoSize(true);
        }

        $dates = $this->createDatesRange();

        $tableTitles = [
            'адрес установки',
            'серийный номер',
            'MAC адрес',
            'Тариф'
        ];

        $sheet->setCellValueByColumnAndRow(0, 1, $data['title'] . ' - ' . self::REPORT_TITLE_ENDING);
        $this->styleForTitleRow($sheet);

        $sheet->setCellValueByColumnAndRow(0, 3, $data['description']['value']);
        $this->styleForSubtitleRow($sheet);

        $sheet->fromArray($tableTitles, null, 'A6');
        $sheet->getStyle('A6:D6')->getFont()->setBold(true);

        $colNumber = 4;
        $rowNumber = 6;
        foreach ($dates as $date) {
            $dateObject = \DateTime::createFromFormat('d.m.Y', $date);
            $fullMonthRussian = $this->transliterator->translateEnglishMonthIntoRussian($dateObject->format('F'));
            $dayInMonth = $dateObject->format('d');

            $cell = $sheet->setCellValueByColumnAndRow($colNumber, $rowNumber, $dayInMonth . ' ' . $fullMonthRussian, $returnCell = true);
            $cell->getStyle()->getFont()->setBold(true);
            $colNumber++;
        }

        $rowNumber = 7;
        $startCol = 4;
        $station = current($this->stationData);

        $rowNumberIncrement = function () use (&$rowNumber): int {
            return $rowNumber += (3 + $this->additionalTariffsInSelectionNum);
        };

        $this->writeDayDataRow($sheet, $rowNumber, $startCol, 'Суммарное потребление', '', '', $data['data']['sum']);
        $this->writeDayDataRow(
            $sheet,
            $rowNumberIncrement(),
            $startCol,
            $station['title'],
            $this->devicesData[$station['device_id']]['settings']['serial_number'],
            $this->devicesData[$station['device_id']]['settings']['zigbee_mac'],
            $data['data']['station']
        );
        $this->writeDayDataRow($sheet, $rowNumberIncrement(), $startCol, 'Небаланс', 'Фактический', '', $data['data']['balance']);
        $this->writeDayDataRow($sheet, $rowNumberIncrement(), $startCol, 'Небаланс', '%', '', $data['data']['balance_percents']);

        foreach ($data['data']['values'] as $deviceId => $value) {
            $this->writeDayDataRow(
                $sheet,
                $rowNumberIncrement(),
                $startCol,
                $this->devicesData[$deviceId]['settings']['address'],
                $this->devicesData[$deviceId]['settings']['serial_number'],
                $this->devicesData[$deviceId]['settings']['zigbee_mac'],
                $value
            );
        }

        $fileName = $this->transliterator->transliterate($data['title']);

        $sheet->setSelectedCells('A1');
        $this->saveXLSFile($phpExcelObject, $saveDir, $data['title'] . '.' . ReportResult::TYPE_XLS, false, $fileName);
    }

    /**
     * Create XLS report detailed by half hours.
     *
     * @param string $saveDir
     * @param array $data
     * @throws \PHPExcel_Exception
     */
    private function createHalfHoursXLSFile(string $saveDir, array $data)
    {
        $phpExcelObject = $this->createPhpExcelObject();
        $sheet = $phpExcelObject->getActiveSheet();

        $sheet->getDefaultColumnDimension()->setWidth(25.67);
        $sheet->getColumnDimension('A')->setWidth(28.33);
        $sheet->getColumnDimension('B')->setWidth(18.67);
        $sheet->getColumnDimension('C')->setWidth(21.33);
        $sheet->getColumnDimension('D')->setWidth(10.67);

        $sheet->freezePane('A7');

        foreach (range('a', 'd') as $item) {
            $sheet->getColumnDimension(strtoupper($item))->setAutoSize(true);
        }

        $dates = $this->createDatesRange();

        $tableTitles = [
            'адрес установки',
            'серийный номер',
            'zigbee mac-адрес',
            'тип'
        ];

        $sheet->setCellValueByColumnAndRow(0, 1, $data['title'] . ' - ' . self::REPORT_TITLE_ENDING);
        $this->styleForTitleRow($sheet);

        $sheet->setCellValueByColumnAndRow(0, 3, $data['description']['value']);
        $this->styleForSubtitleRow($sheet);

        $sheet->fromArray($tableTitles, null, 'A6');
        $sheet->getStyle('A6:D6')->getFont()->setBold(true);

        $colNumber = 4;
        $rowNumber = 6;
        foreach ($dates as $date) {
            $dateEndObject = \DateTime::createFromFormat('d.m.Y H:i', $date);
            $dateEndObject->modify('+30 minutes');
            $dateLabel = $date . ' - ' . $dateEndObject->format('H:i');

            $cell = $sheet->setCellValueByColumnAndRow($colNumber, $rowNumber, $dateLabel, $returnCell = true);
            $cell->getStyle()->getFont()->setBold(true);
            $colNumber++;
        }

        $rowNumber = 7;
        $startCol = 4;
        $station = current($this->stationData);

        $rowNumberIncrement = function () use (&$rowNumber): int {
            return $rowNumber += 1;
        };

        $this->writeHalfHourDataRow($sheet, $rowNumber, $startCol, 'Суммарное потребление', '', '', $data['data']['sum']);
        $this->writeHalfHourDataRow(
            $sheet,
            $rowNumberIncrement(),
            $startCol,
            $station['title'],
            $this->devicesData[$station['device_id']]['settings']['serial_number'],
            $this->devicesData[$station['device_id']]['settings']['zigbee_mac'],
            $data['data']['station']
        );
        $this->writeHalfHourDataRow($sheet, $rowNumberIncrement(), $startCol, 'Небаланс', 'Фактический', '', $data['data']['balance']);
        $this->writeHalfHourDataRow($sheet, $rowNumberIncrement(), $startCol, 'Небаланс', '%', '', $data['data']['balance_percents']);

        foreach ($data['data']['values'] as $deviceId => $value) {
            $this->writeHalfHourDataRow(
                $sheet,
                $rowNumberIncrement(),
                $startCol,
                $this->devicesData[$deviceId]['settings']['address'],
                $this->devicesData[$deviceId]['settings']['serial_number'],
                $this->devicesData[$deviceId]['settings']['zigbee_mac'],
                $value
            );
        }

        $fileName = $this->transliterator->transliterate($data['title']);

        $this->saveXLSFile($phpExcelObject, $saveDir, $data['title'] . '.' . ReportResult::TYPE_XLS, false, $fileName);
    }

    /**
     * @inheritdoc
     */
    protected function assignZipFileName()
    {
        return $this->transliterator->transliterate($this->reportSettings->getTitle()) . '.' . ReportResult::TYPE_ZIP;
    }

    /**
     * @inheritdoc
     */
    protected function assignJsonFileName()
    {
        return $this->transliterator->transliterate($this->reportSettings->getTitle()) . '.' . ReportResult::TYPE_JSON;
    }

    /**
     * TODO: for this class we dont need this method. See prepareAllMeteringData below.
     *       may be we need to refactor this method in other generators
     *
     * @param $deviceId
     * @return array|void
     */
    protected function prepareMeteringData($deviceId) { }

    /**
     * {@inheritdoc}
     */
    protected function getStationData()
    {
        $result = [];

        foreach ($this->projectData as $project) {
            $stationFile = $this->dataDirectory
                .DIRECTORY_SEPARATOR
                .self::ROOT_DIR
                .DIRECTORY_SEPARATOR
                .sprintf(self::DEVICES_DIRECTORY, $project['id'])
                .DIRECTORY_SEPARATOR
                .self::STATIONS_FILE_NAME;
            $this->checkIsExists($stationFile);

            $stations = $this->readFile($stationFile);

            foreach ($stations['transformer_stations'] as $station) {
                $result[$station['id']] = $station;
            }
        }

        return $result;
    }

    /**
     * Preparing data for all devices and other moments (like stations)
     *
     * @return array
     */
    protected function prepareAllMeteringData()
    {
        $station = current($this->stationData);
        $stationId = $station['device_id'];
        $dates = $this->createDatesRange();
        $datasets = $this->devicesData[$stationId]['settings']['electricity_metering_dataset'];

        $stationDataResult = $this->prepareStationData($stationId, $datasets, $dates);

        if ($this->detailTypeId === ReportSettingsInterface::DETAIL_HALF_HOUR) {
            list($sum, $deviceDataResult) = $this->prepareSumAndDeviceHalfHourData($dates, $datasets);
        } else {
            list($sum, $deviceDataResult) = $this->prepareSumAndDeviceDailyData($dates, $datasets);
        }

        list($balance, $balancePercents) = $this->prepareCalculatedBalanceData($stationDataResult, $sum);

        uksort($deviceDataResult, function($a, $b) {
            $a = $this->devicesData[$a]['settings']['address'];
            $b = $this->devicesData[$b]['settings']['address'];
            return $a <=> $b;
        });

        return [
            'sum' => $sum,
            'station' => $stationDataResult,
            'balance' => $balance,
            'balance_percents' => $balancePercents,
            'values' => $deviceDataResult,
        ];
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     * @param integer            $row
     * @param integer            $col
     * @param string             $addressValue
     * @param string             $serialNumberValue
     * @param string             $zigbeeValue
     * @param array              $dataValues
     */
    protected function writeDayDataRow(
        PHPExcel_Worksheet $sheet,
        $row,
        $col,
        $addressValue,
        $serialNumberValue,
        $zigbeeValue,
        $dataValues
    ) {
        $sheet->setCellValueByColumnAndRow(0, $row, $addressValue);
        $this->mergeAndVerticalAlignStyle($sheet, $this->lettersRange[0], $row, $row + 2);

        $sheet->setCellValueByColumnAndRow(1, $row, $serialNumberValue);
        $this->mergeAndVerticalAlignStyle($sheet, $this->lettersRange[1], $row, $row + 2);
        $sheet->getCellByColumnAndRow(1, $row)->getStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        $sheet->setCellValueByColumnAndRow(2, $row, $zigbeeValue);
        $this->mergeAndVerticalAlignStyle($sheet, $this->lettersRange[2], $row, $row + 2);

        $tariffTitles = [
            'Сумма',
            'День',
            'Ночь',
        ];

        if ($this->additionalTariffsInSelectionNum === 1) {
            $tariffTitles[] = 'Пик';
        } elseif ($this->additionalTariffsInSelectionNum === 2) {
            $tariffTitles[] = 'Полупик';
        }

        for ($i = 0; $i < count($tariffTitles); $i++) {
            $sheet->setCellValueByColumnAndRow(3, $row + $i, $tariffTitles[$i]);
        }
        foreach ($dataValues as $type) {
            foreach ($type as $item) {
                array_walk(
                    $item,
                    function ($v, $k) use ($row, $sheet, $col) {
                        $sheet->setCellValueByColumnAndRow($col, $row + $k, $v);
                    }
                );
            }
            $col++;
        }
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     * @param integer            $row
     * @param integer            $col
     * @param string             $addressValue
     * @param string             $serialNumberValue
     * @param string             $zigbeeValue
     * @param array              $dataValues
     */
    protected function writeHalfHourDataRow(
        PHPExcel_Worksheet $sheet,
        $row,
        $col,
        $addressValue,
        $serialNumberValue,
        $zigbeeValue,
        $dataValues
    ) {
        $sheet->setCellValueByColumnAndRow(0, $row, $addressValue);
        $sheet->setCellValueByColumnAndRow(1, $row, $serialNumberValue);
        $sheet->setCellValueByColumnAndRow(2, $row, $zigbeeValue);

        $sheet->getCellByColumnAndRow(1, $row)->getStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        foreach ($dataValues as $dateKey => $dayValues) {
            foreach ($dayValues as $energyKey => $item) {
                $sheet->setCellValueByColumnAndRow(3, $row, $energyKey);
                array_walk(
                    $item,
                    function ($v, $k) use ($row, $sheet, $col) {
                        $sheet->setCellValueByColumnAndRow($col + $k, $row, $v);
                    }
                );
            }
            $col++;
        }
    }

    /**
     * Create an array with dates by report's detail type.
     *
     * @return array
     */
    protected function createDatesRange()
    {
        $dateStart = $this->reportSettings->getDateStart();
        $dateEnd = clone $this->reportSettings->getDateEnd();

        //modifying end date to add the last day in the report
        $dateEnd->modify('+1 day');

        $dates = [];

        if ($this->detailTypeId == ReportSettingsInterface::DETAIL_DAY) {
            $dateInterval = new \DatePeriod($dateStart, new \DateInterval('P1D'), $dateEnd);

            foreach ($dateInterval as $date) {
                $dates[] = $date->format('d.m.Y');
            }

        } elseif ($this->detailTypeId == ReportSettingsInterface::DETAIL_HALF_HOUR) {
            $dateInterval = new \DatePeriod($dateStart, new \DateInterval('PT30M'), $dateEnd);

            /** @var \DateTime $date */
            foreach ($dateInterval as $date) {
                $dates[] = $date->format('d.m.Y H:i');
            }
        }

        return $dates;
    }

    /**
     * @param $stationId
     * @param $datasets
     * @param $dates
     * @return array
     */
    private function prepareStationData($stationId, $datasets, $dates)
    {
        $result = [];

        if (array_key_exists($stationId, $this->usageData)) {
            $stationData = $this->usageData[$stationId];
        } else {
            $stationData = [];
        }

        $ratios = $this->createTransformerStationRatios($stationId);

        foreach ($stationData as $date => $data) {
            if ($this->detailTypeId === ReportSettingsInterface::DETAIL_HALF_HOUR) {
                $date = \DateTime::createFromFormat(DATE_ISO8601, $date)->format('d.m.Y H:i');
            } else {
                $date = \DateTime::createFromFormat(DATE_ISO8601, $date)->format('d.m.Y');
            }

            if (!in_array($date, $dates, true)) {
                continue;
            }

            $data = array_filter(
                $data,
                function ($set) use ($datasets) {
                    if (in_array($set['type'], $datasets, true)) {
                        return $set;
                    }
                    return null;
                }
            );

            $values = $this->changeTransformerStationMeteringData($data[0]['values'], $ratios);

            $this->findAdditionalTariffs($values);

            $result[$date][$data[0]['type']] = self::convertArrayOfWattsToKilowatts($values);
        }

        unset($this->usageData[$stationId]);

        return $result;
    }

    /**
     * This method works only for DETAIL_DAY type.
     *
     * Set the number of additional tariffs (min=1, max=2) in the report.
     * This value helps to show all the required rows or to hide the unused.
     * This value should affect all the table, not only this row(s).
     *
     * @param array $values
     */
    private function findAdditionalTariffs(array $values)
    {
        if ($this->detailTypeId === ReportSettingsInterface::DETAIL_DAY) {
            if (count($values) == 4) {
                $this->additionalTariffsInSelectionNum++;
            } elseif (count($values) == 5) {
                $this->additionalTariffsInSelectionNum += 2;
            }
        }
    }

    /**
     * @param $dates
     * @param $datasets
     * @return array
     */
    private function prepareSumAndDeviceDailyData($dates, $datasets)
    {
        $result = [];
        $sum = [];

        foreach ($this->usageData as $deviceId => $deviceData) {
            $device = [];
            foreach ($deviceData as $date => $item) {
                $date = \DateTime::createFromFormat(DATE_ISO8601, $date)->format('d.m.Y');

                if (!in_array($date, $dates, true)) {
                    continue;
                }

                $item = array_filter(
                    $item,
                    function ($set) use ($datasets) {
                        if (in_array($set['type'], $datasets, true)) {
                            return $set;
                        }

                        return null;
                    }
                );

                if (empty($item)) {
                    $device[$date] = [];

                    continue;
                }

                $rawValues = $item[0]['values'];
                $convertedValues = self::convertArrayOfWattsToKilowatts($rawValues);
                $this->findAdditionalTariffs($convertedValues);
                $device[$date][$item[0]['type']] = $convertedValues;

                if (!isset($sum[$date][$item[0]['type']])) {
                    for ($i = 0; $i < count($convertedValues); $i++) {
                        $sum[$date][$item[0]['type']][$i] = 0;
                    }
                }

                foreach ($convertedValues as $key => $value) {
                    $sum[$date][$item[0]['type']][$key] = bcadd($sum[$date][$item[0]['type']][$key], $value);
                }
            }
            $result[$deviceId] = $device;
        }

        return [$sum, $result];
    }

    /**
     * @param $dates
     * @param $datasets
     * @return array
     */
    private function prepareSumAndDeviceHalfHourData($dates, $datasets)
    {
        $result = [];

        //convert all the device values to kilowatts
        foreach ($this->usageData as $deviceId => $listDeviceData) {
            $device = [];

            foreach ($listDeviceData as $dayDate => $item) {
                $dayFormattedDate = \DateTime::createFromFormat(DATE_ISO8601, $dayDate)->format('d.m.Y H:i');

                //check if this day exists in the period
                if (!in_array($dayFormattedDate, $dates, true)) {
                    continue;
                }

                //filter the requested data sets
                $item = array_filter($item, function ($set) use ($datasets) {
                    if (array_key_exists('type', $set)) {
                        if (in_array($set['type'], $datasets, true)) {
                            return $set;
                        }
                    }

                    return null;
                    }
                );

                //create an empty array nest if there no types were found
                if (empty($item)) {
                    $device[$dayFormattedDate] = [];
                    continue;
                }

                foreach ($item as $typeData) {
                    $rawValues = $typeData['values'];
                    $convertedValues = self::convertArrayOfWattsToKilowatts($rawValues);

                    foreach ($convertedValues as $halfHourKey => $value) {
                        $device[$dayFormattedDate][$typeData['type']] = $convertedValues;
                    }
                }
            }
            $result[$deviceId] = $device;
        }

        $sum = $this->calculateHalfHourSum($result);

        return [$sum, $result];
    }

    /**
     * Calculate a sum data for half hour report.
     *
     * @param array $result
     * @return array
     */
    private function calculateHalfHourSum(array $result)
    {
        $sum = [];

        foreach ($result as $device) {
            foreach ($device as $dayDate => $dayData) {
                if (!array_key_exists($dayDate, $sum)) {
                    $sum[$dayDate] = [];
                }

                foreach ($dayData as $energyType => $energyTypeData) {
                    if (!array_key_exists($energyType, $sum[$dayDate])) {
                        $sum[$dayDate][$energyType] = [];
                    }

                    for ($i = 0; $i < 48; $i++) {
                        //skip the empty half hour data in device
                        if (!array_key_exists($i, $device[$dayDate][$energyType])) {
                            continue;
                        }

                        //add or create the values
                        if (!array_key_exists($i, $sum[$dayDate][$energyType])) {
                            $sum[$dayDate][$energyType][$i] = $device[$dayDate][$energyType][$i];
                        } else {
                            $sum[$dayDate][$energyType][$i] = bcadd(
                                $sum[$dayDate][$energyType][$i],
                                $device[$dayDate][$energyType][$i]
                            );
                        }
                    }
                }
            }
        }

        //sort the sum data by key's date
        uksort($sum, function($a, $b) {
            $a = \DateTime::createFromFormat('d.m.Y H:i', $a);
            $b = \DateTime::createFromFormat('d.m.Y H:i', $b);
            return $a <=> $b;
        });

        return $sum;
    }

    /**
     * @param $stationDataResult
     * @param $sum
     *
     * @return array
     */
    private function prepareCalculatedBalanceData($stationDataResult, $sum)
    {
        $balance = [];
        $balancePercents = [];

        foreach ($stationDataResult as $date => $stationData) {
            $sumData = $sum[$date];
            foreach ($stationData as $type => $values) {
                foreach ($values as $key => $value) {
                    $balance[$date][$type][$key] = bcsub($value, $sumData[$type][$key]);
                    $balancePercents[$date][$type][$key] = bcdiv(bcmul($sumData[$type][$key], '100'), $value);
                }
            }
        }

        return [$balance, $balancePercents];
    }
}