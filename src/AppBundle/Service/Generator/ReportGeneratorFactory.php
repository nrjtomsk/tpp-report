<?php

namespace AppBundle\Service\Generator;

use AppBundle\Entity\ReportRequest;
use AppBundle\Exceptions\NoSuchTypeException;
use AppBundle\Service\TransliteratorService;
use Doctrine\Common\Persistence\ObjectManager;
use Liuggio\ExcelBundle\Factory as PHPExcel;
use Symfony\Component\Filesystem\Filesystem;

class ReportGeneratorFactory
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var PHPExcel
     */
    private $phpExcel;

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var TransliteratorService
     */
    private $transliterator;

    /**
     * ReportGeneratorFactory constructor.
     * @param Filesystem $filesystem
     * @param PHPExcel $phpExcel
     * @param ObjectManager $manager
     * @param TransliteratorService $transliterator
     */
    public function __construct(Filesystem $filesystem, PHPExcel $phpExcel, ObjectManager $manager, TransliteratorService $transliterator)
    {
        $this->filesystem = $filesystem;
        $this->phpExcel = $phpExcel;
        $this->manager = $manager;
        $this->transliterator = $transliterator;
    }

    /**
     * @param $type
     * @return ReportGeneratorInterface
     * @throws \Exception
     */
    public function create($type)
    {
        $types = [
            ReportRequest::CODE_DEVICE_USAGE => DeviceUsageGenerator::class,
            ReportRequest::CODE_GROUP_USAGE => GroupUsageGenerator::class,
            ReportRequest::CODE_TRANSFORM_STATION_BALANCE => TransformStationBalanceGenerator::class,
            ReportRequest::CODE_GROUP_USAGE_COSTS => GroupUsageCostsGenerator::class,
            ReportRequest::CODE_GROUP_CORPORATE_USAGE => GroupCorporateUsageGenerator::class,
            ReportRequest::CODE_PESC_CORPORATE_USAGE => PescCorporateUsageGenerator::class,
            ReportRequest::CODE_PESC_CORPORATE_XML => PescCorporateUsageXMLGenerator::class,
            ReportRequest::CODE_RKS_ENERGO_CORPORATE_USAGE => RksEnergoCorporateUsageGenerator::class,
        ];

        if (!array_key_exists($type, $types)) {
            throw new NoSuchTypeException($type);
        }

        $generator = new $types[$type]($this->filesystem, $this->phpExcel, $this->manager, $this->transliterator);

        return $generator;
    }
}