<?php

namespace AppBundle\Service\Generator;

use AppBundle\Entity\ReportResult;

class PescCorporateUsageGenerator extends AbstractReportGenerator
{
    const FLOAT_ZERO_INIT_VALUE = 0.000;
    const INCOMPLETE_FLAGS_SUMMARY_KEY = 'summary';

    const TOTAL_REPORT_KEY_FIRST_HOUR = 'first_hour';
    const TOTAL_REPORT_KEY_LAST_HOUR = 'last_hour';
    const TOTAL_REPORT_KEY_TOTAL = 'total';

    /**
     * @var array
     */
    protected $energyTypes = [
        'A+' => 'Активная энергия прием',
        'R+' => 'Реактивная энергия прием',
        'R-' => 'Реактивная энергия отдача',
    ];

    protected $bordersStyle = [
        'top' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
        'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
        'right' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
        'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
    ];

    /**
     * @var string
     */
    private $preparedCompanyTitle = '';

    /**
     * @var array
     */
    private $totalReportData = [];

    /**
     * @var array
     */
    private $devicesWithoutCoefficients = [];

    /**
     * This method finds the devices without coefficients to change the behaviour of them in sheets.
     */
    private function detectDevicesWithoutCoefficients()
    {
        $coefficientKeys = [
            'percent_of_line_loss_am',
            'percent_of_line_loss_ap',
            'percent_of_line_loss_rm',
            'percent_of_line_loss_rp',
        ];

        foreach ($this->devicesData as $key => $device) {
            foreach ($coefficientKeys as $coefficientKey) {
                if (!array_key_exists($coefficientKey, $device['settings'])) {
                    $this->devicesWithoutCoefficients[] = $key;
                    break;
                }
            }
        }
    }

    /**
     * Preparing data for report.
     *
     * @return array
     */
    protected function prepareData()
    {
        $data = $this->prepareAllMeteringData();
        $deviceSummaryData = $this->prepareDeviceSummaryData($data);
        $project = reset($this->projectData);

        $this->preparedCompanyTitle = $this->prepareCompanyTitle($project['settings']['company']['name']);
        $this->detectDevicesWithoutCoefficients();

        return [
            'title' => $project['title'],
            'address' => $project['settings']['company']['address'],
            'data' => $data,
            'device_summary_data' => $deviceSummaryData,
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function prepareAllMeteringData()
    {
        $result = [];

        $dateStart = $this->reportSettings->getDate();
        //here we use the 't' to get the latest day of the month
        $dateEnd = \DateTime::createFromFormat('d.m.Y', $this->reportSettings->getDate()->format('t.m.Y'));

        foreach ($this->usageData as $deviceId => $values) {
            $result[$deviceId] = [];
            foreach ($values as $date => $value) {
                $parsedDate = \DateTime::createFromFormat(DATE_ISO8601, $date);

                //check if the given date belongs to requested month's period
                if ($parsedDate >= $dateStart && $parsedDate <= $dateEnd) {
                    $this->prepareDailyData($value, $result, $deviceId, $parsedDate->format('d.m.Y'));
                }
            }
        }

        $this->setIncompleteFlagsForSummaryReports();

        return $result;
    }

    /**
     * @param string $rawTitle
     * @return string
     */
    private function prepareCompanyTitle($rawTitle)
    {
        return substr($this->transliterator->transliterate($rawTitle), 0, 25);
    }

    /**
     * @inheritdoc
     */
    protected function assignZipFileName()
    {
        return $this->preparedCompanyTitle . '.' . ReportResult::TYPE_ZIP;
    }

    /**
     * @inheritdoc
     */
    protected function assignJsonFileName()
    {
        return $this->preparedCompanyTitle . '.' . ReportResult::TYPE_JSON;
    }

    /**
     * Set the incomplete flags for all the summary reports.
     * Needs to be called after the data preparation for device reports.
     */
    private function setIncompleteFlagsForSummaryReports()
    {
        if (empty($this->incompleteFlags)) {
            foreach ($this->energyTypes as $energyKey => $energyTitle) {
                $this->incompleteFlags[self::INCOMPLETE_FLAGS_SUMMARY_KEY][$energyKey] = true;
            }
        } else {
            foreach ($this->energyTypes as $energyKey => $energyTitle) {
                $this->incompleteFlags[self::INCOMPLETE_FLAGS_SUMMARY_KEY][$energyKey] = false;

                foreach ($this->incompleteFlags as $deviceId => $flagValues) {
                    if ($deviceId !== self::INCOMPLETE_FLAGS_SUMMARY_KEY) {
                        $this->incompleteFlags[self::INCOMPLETE_FLAGS_SUMMARY_KEY][$energyKey] |= $flagValues[$energyKey];
                    }
                }
            }
        }

        $this->setIncompleteFlagForZipAndJson();
    }

    /**
     * @inheritdoc
     */
    protected function setIncompleteFlagForZipAndJson()
    {
        foreach ($this->incompleteFlags[self::INCOMPLETE_FLAGS_SUMMARY_KEY] as $flag) {
            if ($flag == true) {
                $this->incompleteFlagZipJson = true;
                break;
            }
        }
    }

    /**
     * Merge the prepared data of all devices
     * to a three (A+, R+, R-) separate set of sums.
     *
     * @param array $preparedData
     * @return array
     */
    protected function prepareDeviceSummaryData(array $preparedData)
    {
        $deviceSummaryData = [];

        //add the defined energy types in results array
        foreach ($this->energyTypes as $typeKey => $title) {
            $deviceSummaryData[$typeKey] = [];
        }

        foreach ($preparedData as $deviceId => $device) {
            foreach ($device as $typeKey => $daysData) {
                //skip the iteration if the energy type was not previously defined
                if (!array_key_exists($typeKey, $this->energyTypes)) {
                    unset($preparedData[$deviceId][$typeKey]);
                    continue;
                }

                //create an energy type entry if it hasn't been created
                if (empty($deviceSummaryData[$typeKey])) {
                    $deviceSummaryData[$typeKey] = $preparedData[$deviceId][$typeKey];

                //or add the data to existing one
                } else {
                    foreach ($daysData as $date => $values) {
                        if (!array_key_exists($date, $deviceSummaryData[$typeKey])) {
                            $deviceSummaryData[$typeKey][$date] = [];
                        }

                        foreach ($values as $hour => $value) {

                            //initialize the empty hours data in result array with zero
                            if (!array_key_exists($hour, $deviceSummaryData[$typeKey][$date])) {
                                $deviceSummaryData[$typeKey][$date][$hour] = (float) self::FLOAT_ZERO_INIT_VALUE;
                            }

                            $hourAdditionalValue = self::FLOAT_ZERO_INIT_VALUE;

                            //check if the hour data exists in the preparedData array (or add a zero)
                            if (array_key_exists($date, $preparedData[$deviceId][$typeKey])
                                && array_key_exists($hour, $preparedData[$deviceId][$typeKey][$date])) {

                                $hourAdditionalValue = $preparedData[$deviceId][$typeKey][$date][$hour];
                            }

                            $deviceSummaryData[$typeKey][$date][$hour] += $hourAdditionalValue;
                        }
                    }
                }
            }
        }

        return $deviceSummaryData;
    }

    /**
     * Create XLS file from data and save it to directory
     *
     * @param string $saveDir
     * @param array $data
     */
    protected function createXLSFiles(string $saveDir, array $data)
    {
        $this->createDeviceXLSFiles($saveDir, $data);
        $this->createDeviceSummaryXLSFiles($saveDir, $data);
        $this->createTotalXLSFile($saveDir, $data);
    }

    /**
     * @param string $saveDir
     * @param array $data
     * @throws \PHPExcel_Exception
     */
    private function createTotalXLSFile(string $saveDir, array $data)
    {
        $project = reset($this->projectData);
        $contractNumber = $project['settings']['electricity_provider']['contract_date'];

        $phpExcelObject = $this->createPhpExcelObject();
        $sheet = $phpExcelObject->getActiveSheet();
        $sheet->setTitle(self::XLS_DEFAULT_WORKSHEET_TITLE);

        $sheet->getDefaultColumnDimension()->setWidth(13.17);
        $sheet->getColumnDimension('B')->setWidth(24.17);

        $sheet->getCell('G1')->setValue('Приложение №4');
        $sheet->mergeCells('G1:I1');
        $sheet->getCell('G2')->setValue('к договору №' . $contractNumber);
        $sheet->mergeCells('G2:I2');
        $sheet->getCell('G3')->setValue('от');
        $sheet->mergeCells('G3:I3');
        $sheet->mergeCells('G4:I4');
        $sheet->getStyle('G1:I4')->applyFromArray(['font' => ['italic' => true]]);

        $sheet->getCell('B5')->setValue('ПОТРЕБИТЕЛЬ:');
        $sheet->mergeCells('B5:I5');
        $sheet->getCell('B6')->setValue('Энергоснабжаемый объект: ' . $data['title']);
        $sheet->mergeCells('B6:I6');
        $sheet->getCell('B7')->setValue('Код энергоснабжаемого объекта: ' . $contractNumber);
        $sheet->mergeCells('B7:I7');
        $sheet->getCell('B8')->setValue('Адрес энергоснабжаемого объекта: ' . $data['address']);
        $sheet->mergeCells('B8:I8');
        $sheet->getStyle('B5:I8')->applyFromArray(['font' => ['italic' => true, 'bold' => true]]);
        $sheet->mergeCells('B9:I9');
        $sheet->mergeCells('B10:I10');

        $sheet->getCell('B11')->setValue('О Т Ч Е Т');
        $sheet->getRowDimension(11)->setRowHeight(25);
        $sheet->mergeCells('B11:I11');
        $sheet->getStyle('B11:I11')->applyFromArray([
            'font' => ['bold' => true],
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
            ],
        ]);

        $sheet->getRowDimension(12)->setRowHeight(25);
        $sheet->getCell('B12')->setValue('об электропотреблении за ');
        $sheet->mergeCells('B12:D12');
        $sheet->getCell('E12')->setValue($this->reportRequest->getSettings()->getDate()->format('m.Y'));
        $sheet->getStyle('B12:E12')->applyFromArray(['alignment' => ['vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER]]);
        $sheet->getStyle('E12')->applyFromArray(['font' => ['bold' => true]]);

        $sheet->getRowDimension(13)->setRowHeight(25);
        $sheet->getCell('B13')->setValue('1. Активная электроэнергия');
        $sheet->getStyle('B13')->applyFromArray(['alignment' => ['vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER]]);
        $sheet->mergeCells('B13:I13');
        $sheet->getStyle('B13:I13')->applyFromArray(['font' => ['bold' => true]]);

        $sheet->getRowDimension(14)->setRowHeight(17);
        $sheet->getRowDimension(15)->setRowHeight(67);

        $sheet->getCell('B14')->setValue("№ точки учета по договору");
        $sheet->getCell('C14')->setValue("№ счетчика сумматора");
        $sheet->getCell('D14')->setValue("Показания счетчиков на");
        $sheet->getCell('D15')->setValue("0 час. первого числа периода");
        $sheet->getCell('E15')->setValue("24 час. последнего числа периода");
        $sheet->getCell('F14')->setValue("Разность показаний");
        $sheet->getCell('G14')->setValue("Расчетный коэффициент");
        $sheet->getCell('H14')->setValue("Потери %");
        $sheet->getCell('I14')->setValue("Расход кВтч. с учетом потерь");
        $sheet->mergeCells('B14:B15');
        $sheet->mergeCells('C14:C15');
        $sheet->mergeCells('D14:E14');
        $sheet->mergeCells('D14:E14');
        $sheet->mergeCells('F14:F15');
        $sheet->mergeCells('G14:G15');
        $sheet->mergeCells('H14:H15');
        $sheet->mergeCells('I14:I15');

        $sheet->getStyle('B14:I15')->getAlignment()->setWrapText(true);
        $sheet->getStyle('B14:I15')->applyFromArray([
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
        ]);

        $sheet->getStyle('B14:I15')->applyFromArray([
            'borders' => [
                'outline' => [
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
                'inside' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                ]
            ],
        ]);

        $row = 16;
        $totalValueAP = 0;

        foreach($this->totalReportData['A+'] as $deviceId => $values) {
            $sheet->getCell('C' . $row)->setValue($this->devicesData[$deviceId]['settings']['serial_number']);
            $sheet->getCell('D' . $row)->setValue($values['first_hour']);
            $sheet->getCell('E' . $row)->setValue($values['last_hour']);
            $sheet->getCell('F' . $row)->setValue('=E' . $row . '-' . 'D' . $row);
            $sheet->getCell('I' . $row)->setValue($values['total']);

            if (!in_array($deviceId, $this->devicesWithoutCoefficients)) {
                $sheet->getCell('H' . $row)->setValue($this->devicesData[$deviceId]['settings']['percent_of_line_loss_ap']);
                $sheet->getCell('G' . $row)->setValue('=ROUND(' . 'I' . $row . '*'
                    . $this->devicesData[$deviceId]['settings']['voltage_transformation_ratio']
                    * $this->devicesData[$deviceId]['settings']['current_transformation_ratio']
                    * $this->devicesData[$deviceId]['settings']['percent_of_line_loss_ap'] . ', 3)');
            } else {
                $sheet->getCell('H' . $row)->setValue('-');
                $sheet->getCell('G' . $row)->setValue('-');
            }

            $sheet->getStyle('B' . $row . ':' . 'I' . $row)->applyFromArray([
                'borders' => [
                    'left' => [
                        'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                    ],
                    'right' => [
                        'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                    ],
                    'bottom' => [
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    ],
                    'vertical' => [
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    ],
                ],
                'alignment' => [
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ]
            ]);

            $totalValueAP += $values['total'];
            $row++;
        }

        $sheet->getCell('B' . $row)->setValue('Всего: ');
        $sheet->getStyle('B' . $row)->applyFromArray(['font' => ['bold' => true]]);
        $sheet->getCell('C' . $row)->setValue('X');
        $sheet->getCell('D' . $row)->setValue('X');
        $sheet->getCell('E' . $row)->setValue('X');
        $sheet->getCell('F' . $row)->setValue('X');
        $sheet->getCell('G' . $row)->setValue('X');
        $sheet->getCell('H' . $row)->setValue('X');
        $sheet->getCell('I' . $row)->setValue($totalValueAP);
        $sheet->getStyle('I' . $row)->applyFromArray(['font' => ['bold' => true]]);

        $sheet->getStyle('B' . $row . ':' . 'I' . $row)->applyFromArray([
            'borders' => [
                'outline' => [
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
                'inside' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                ]
            ],
        ]);
        $sheet->getStyle('C' . $row . ':' . 'I' . $row)->applyFromArray([
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ]);

        $row += 2;
        $totalValueRP = 0;

        $sheet->getRowDimension($row)->setRowHeight(25);
        $sheet->getCell('B' . $row)->setValue('2. Реактивная электроэнергия');
        $sheet->getStyle('B' . $row)->applyFromArray([
            'alignment' => [
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
            'font' => [
                'bold' => true,
            ]
        ]);
        $sheet->mergeCells('B' . $row . ':' . 'I' . $row);

        $row++;

        foreach($this->totalReportData['R+'] as $deviceId => $values) {
            $sheet->getCell('C' . $row)->setValue($this->devicesData[$deviceId]['settings']['serial_number']);
            $sheet->getCell('D' . $row)->setValue($values['first_hour']);
            $sheet->getCell('E' . $row)->setValue($values['last_hour']);
            $sheet->getCell('F' . $row)->setValue('=E' . $row . '-' . 'D' . $row);
            $sheet->getCell('I' . $row)->setValue($values['total']);
            if (!in_array($deviceId, $this->devicesWithoutCoefficients)) {
                $sheet->getCell('G' . $row)->setValue('=ROUND(' . 'I' . $row . '*'
                    . $this->devicesData[$deviceId]['settings']['voltage_transformation_ratio']
                    * $this->devicesData[$deviceId]['settings']['current_transformation_ratio']
                    * $this->devicesData[$deviceId]['settings']['percent_of_line_loss_rp'] . ', 3)');
                $sheet->getCell('H' . $row)->setValue($this->devicesData[$deviceId]['settings']['percent_of_line_loss_rp']);
            } else {
                $sheet->getCell('G' . $row)->setValue('-');
                $sheet->getCell('H' . $row)->setValue('-');

            }

            $sheet->getStyle('B' . $row . ':' . 'I' . $row)->applyFromArray([
                'borders' => [
                    'outline' => [
                        'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                    ],
                    'inside' => [
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    ],
                ],
                'alignment' => [
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ]
            ]);

            $totalValueRP += $values['total'];
            $row++;
        }

        $sheet->getCell('B' . $row)->setValue('Всего: ');
        $sheet->getStyle('B' . $row)->applyFromArray(['font' => ['bold' => true]]);
        $sheet->getCell('C' . $row)->setValue('X');
        $sheet->getCell('D' . $row)->setValue('X');
        $sheet->getCell('E' . $row)->setValue('X');
        $sheet->getCell('F' . $row)->setValue('X');
        $sheet->getCell('G' . $row)->setValue('X');
        $sheet->getCell('H' . $row)->setValue('X');
        $sheet->getCell('I' . $row)->setValue($totalValueRP);
        $sheet->getStyle('I' . $row)->applyFromArray(['font' => ['bold' => true]]);

        $sheet->getStyle('B' . $row . ':' . 'I' . $row)->applyFromArray([
            'borders' => [
                'outline' => [
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
                'inside' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                ]
            ],
        ]);
        $sheet->getStyle('C' . $row . ':' . 'I' . $row)->applyFromArray([
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ]);

        $row += 2;
        $totalValueRM = 0;

        $sheet->getRowDimension($row)->setRowHeight(25);
        $sheet->getCell('B' . $row)->setValue('3. Генерация реактивной электроэнергии в сеть энергосистемы');
        $sheet->getStyle('B' . $row)->applyFromArray([
            'alignment' => [
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
            'font' => [
                'bold' => true,
            ]
        ]);
        $sheet->mergeCells('B' . $row . ':' . 'I' . $row);

        $row++;

        foreach ($this->totalReportData['R-'] as $deviceId => $values) {
            $sheet->getCell('C' . $row)->setValue($this->devicesData[$deviceId]['settings']['serial_number']);
            $sheet->getCell('D' . $row)->setValue($values['first_hour']);
            $sheet->getCell('E' . $row)->setValue($values['last_hour']);
            $sheet->getCell('F' . $row)->setValue('=E' . $row . '-' . 'D' . $row);
            $sheet->getCell('I' . $row)->setValue($values['total']);
            if (!in_array($deviceId, $this->devicesWithoutCoefficients)) {
                $sheet->getCell('H' . $row)->setValue($this->devicesData[$deviceId]['settings']['percent_of_line_loss_rm']);
                $sheet->getCell('G' . $row)->setValue('=ROUND(' . 'I' . $row . '*'
                    . $this->devicesData[$deviceId]['settings']['voltage_transformation_ratio']
                    * $this->devicesData[$deviceId]['settings']['current_transformation_ratio']
                    * $this->devicesData[$deviceId]['settings']['percent_of_line_loss_rm'] . ', 3)');
            } else {
                $sheet->getCell('H' . $row)->setValue('-');
                $sheet->getCell('G' . $row)->setValue('-');
            }

            $sheet->getStyle('B' . $row . ':' . 'I' . $row)->applyFromArray([
                'borders' => [
                    'outline' => [
                        'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                    ],
                    'inside' => [
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    ],
                ],
                'alignment' => [
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ]
            ]);

            $totalValueRM += $values['total'];
            $row++;
        }

        $sheet->getCell('B' . $row)->setValue('Всего: ');
        $sheet->getStyle('B' . $row)->applyFromArray(['font' => ['bold' => true]]);
        $sheet->getCell('C' . $row)->setValue('X');
        $sheet->getCell('D' . $row)->setValue('X');
        $sheet->getCell('E' . $row)->setValue('X');
        $sheet->getCell('F' . $row)->setValue('X');
        $sheet->getCell('G' . $row)->setValue('X');
        $sheet->getCell('H' . $row)->setValue('X');
        $sheet->getCell('I' . $row)->setValue($totalValueRM);
        $sheet->getStyle('I' . $row)->applyFromArray(['font' => ['bold' => true]]);

        $sheet->getStyle('B' . $row . ':' . 'I' . $row)->applyFromArray([
            'borders' => [
                'outline' => [
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
                'inside' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                ]
            ],
        ]);
        $sheet->getStyle('C' . $row . ':' . 'I' . $row)->applyFromArray([
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ]);

        $row += 2;

        $sheet->getRowDimension($row)->setRowHeight(25);
        $sheet->getCell('B' . $row)->setValue('4. Максимальная активная и реактивная мощность в часы пиковой нагрузки энергосистемы');
        $sheet->getStyle('B' . $row)->applyFromArray([
            'alignment' => [
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
            'font' => [
                'bold' => true,
            ]
        ]);
        $sheet->mergeCells('B' . $row . ':' . 'I' . $row);

        $row++;

        $sheet->getRowDimension($row)->setRowHeight(34);
        $sheet->getCell('B' . $row)->setValue('Вид энергии');
        $sheet->mergeCells('B' . $row . ':' . 'B' . ($row + 1));
        $sheet->getCell('C' . $row)->setValue('Тип системы учета');
        $sheet->mergeCells('C' . $row . ':' . 'D' . ($row + 1));
        $sheet->getCell('E' . $row)->setValue('Кол-во делений, зафиксированных приборами');
        $sheet->mergeCells('E' . $row . ':' . 'F' . ($row + 1));
        $sheet->getCell('G' . $row)->setValue('Расчетный коэффициент');
        $sheet->mergeCells('G' . $row . ':' . 'G' . ($row + 1));
        $sheet->getCell('H' . $row)->setValue('Максимальная нагрузка: кВт, кВар.');
        $sheet->mergeCells('H' . $row . ':' . 'I' . ($row + 1));

        $sheet->getStyle('B' . $row . ':' . 'I' . ($row + 1))->applyFromArray([
            'borders' => [
                'outline' => [
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
                'inside' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                ]
            ],
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
        ]);
        $sheet->getStyle('B' . $row . ':' . 'I' . ($row + 1))->getAlignment()->setWrapText(true);

        $row += 2;

        $sheet->getCell('B' . $row)->setValue('Акт.(P)');
        $sheet->getStyle('B' . $row)->getFont()->setBold(true);
        $sheet->mergeCells('C' . $row . ':' . 'D' . $row);
        $sheet->mergeCells('E' . $row . ':' . 'F' . $row);
        $sheet->mergeCells('H' . $row . ':' . 'I' . $row);

        $row++;

        $sheet->getCell('B' . $row)->setValue('Реакт(Q)');
        $sheet->getStyle('B' . $row)->getFont()->setBold(true);
        $sheet->mergeCells('C' . $row . ':' . 'D' . $row);
        $sheet->mergeCells('E' . $row . ':' . 'F' . $row);
        $sheet->mergeCells('H' . $row . ':' . 'I' . $row);

        $sheet->getStyle('B' . ($row - 1) . ':I' . $row)->applyFromArray([
            'borders' => [
                'outline' => [
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
                'horizontal' => [
                    'style' => \PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
                'vertical' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                ],
            ],
        ]);

        $row += 2;

        $sheet->getCell('C' . $row)->setValue('Ответственный за электрохозяйство ___________________________________');
        $sheet->mergeCells('C' . $row . ':' . 'H' . $row);

        $row++;

        $sheet->getCell('F' . $row)->setValue('(подпись)');
        $sheet->getCell('G' . $row)->setValue('Ф.И.О.');
        $sheet->getStyle('F' . $row . ':' . 'G' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $row++;

        $sheet->getCell('C' . $row)->setValue('Дата ___________________________________');
        $sheet->mergeCells('C' . $row . ':' . 'H' . $row);

        $row += 2;

        $sheet->getCell('B' . $row)->setValue('«Гарантирующий поставщик»');
        $sheet->mergeCells('B' . $row . ':' . 'E' . $row);
        $sheet->getStyle('B' . $row . ':' . 'E' . $row)->getFont()->setBold(true);
        $sheet->getStyle('B' . $row . ':' . 'E' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $sheet->getCell('F' . $row)->setValue('«Потребитель»');
        $sheet->mergeCells('F' . $row . ':' . 'I' . $row);
        $sheet->getStyle('F' . $row . ':' . 'I' . $row)->getFont()->setBold(true);
        $sheet->getStyle('F' . $row . ':' . 'I' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $row++;

        $sheet->getCell('B' . $row)->setValue('г.');
        $sheet->mergeCells('B' . $row . ':' . 'E' . $row);
        $sheet->getStyle('B' . $row . ':' . 'E' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $sheet->getCell('F' . $row)->setValue('г.');
        $sheet->mergeCells('F' . $row . ':' . 'I' . $row);
        $sheet->getStyle('F' . $row . ':' . 'I' . $row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $row++;

        $sheet->getCell('C' . $row)->setValue('М.П.');
        $sheet->mergeCells('C' . $row . ':' . 'E' . $row);

        $sheet->getCell('G' . $row)->setValue('М.П.');
        $sheet->mergeCells('G' . $row . ':' . 'I' . $row);

        $fileTitle = $data['title']
            . ' '
            . 'отчет.'
            . ReportResult::TYPE_XLS;

        $fileName = $this->preparedCompanyTitle
            . $this->transliterator->transliterate(' отчет');

        $sheet->setSelectedCells('A1');
        $this->saveXLSFile(
            $phpExcelObject,
            $saveDir,
            $fileTitle,
            //for this report we use the same incompleteness flag as for zip/json one
            $this->incompleteFlagZipJson,
            $fileName
        );

        unset($phpExcelObject);
    }

    /**
     * @param string $saveDir
     * @param array $data
     * @throws \PHPExcel_Exception
     */
    private function createDeviceXLSFiles(string $saveDir, array $data)
    {
        foreach ($data['data'] as $deviceId => $items) {
            $device = $this->devicesData[$deviceId];
            $dataSets = $device['settings']['electricity_metering_dataset'];

            if(($setKey = array_search('A-', $dataSets)) !== false) {
                unset($dataSets[$setKey]);
            }

            //generate an empty array to generate device reports even if the data period is empty
            if (empty($items)) {
                foreach ($this->energyTypes as $key => $type) {
                    $items[$key] = [];
                    $this->incompleteFlags[$deviceId][$key] = true;
                }
            }

            $deviceName = array_key_exists('name', $device['settings']) ? ' ' . $device['settings']['name'] : '';

            foreach ($items as $type => $values) {
                if (!in_array($type, $dataSets)) {
                    continue;
                }
                $phpExcelObject = $this->createPhpExcelObject();
                $sheet = $phpExcelObject->getActiveSheet();
                $sheet->setTitle(self::XLS_DEFAULT_WORKSHEET_TITLE);

                $sheet->setCellValueByColumnAndRow(0, 1, 'Наименование потребителя: ' . $data['title']);
                $sheet->mergeCells('A1:Z1');

                $sheet->setCellValueByColumnAndRow(0, 2, 'Адрес потребителя: ' . $data['address']);
                $sheet->mergeCells('A2:Z2');

                $sheet->setCellValueByColumnAndRow(0, 3, 'Группа точек: ' . $deviceName);
                $sheet->mergeCells('A3:Z3');

                $sheet->setCellValueByColumnAndRow(0, 4, 'Адрес: ' . $device['settings']['address']);
                $sheet->mergeCells('A4:Z4');

                $sheet->setCellValueByColumnAndRow(0, 5, 'Прибор учета: ' . $device['settings']['serial_number']);
                $sheet->mergeCells('A5:Z5');

                $sheet->mergeCells('A6:Z6');

                $sheet->mergeCells('A7:Z7');

                //add percents of loss in the worksheet (on top)

                if (!in_array($device['id'], $this->devicesWithoutCoefficients)) {
                    $sheet->setCellValueByColumnAndRow(0, 8, 'Потери, %: '
                        . 'A+ ' . number_format($device['settings']['percent_of_line_loss_ap'], 2, ',', '')
                        . ', A- ' . number_format($device['settings']['percent_of_line_loss_am'], 2, ',', '')
                        . ', R+ ' . number_format($device['settings']['percent_of_line_loss_rp'], 2, ',', '')
                        . ', R- ' . number_format($device['settings']['percent_of_line_loss_rm'], 2, ',', '')
                    );
                } else {
                    $sheet->setCellValueByColumnAndRow(0, 8, 'Потери, %: -');
                }

                $sheet->mergeCells('A7:Z7');

                $sheet->mergeCells('A8:Z8');
                $sheet->mergeCells('A9:Z9');

                $sheet->setCellValueByColumnAndRow(0, 10, 'Фактический почасовой объем потребления электрической энергии потребителем');
                $sheet->mergeCells('A10:Z10');
                $sheet->getStyle('A10:Z10')->getFont()->setBold(true);
                $sheet->getStyle('A10:Z10')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                $sheet->mergeCells('A11:T11');

                $sheet->getCell('U11')->setValue($this->energyTypes[$type])->getStyle()->getFont()->setBold(true);
                $sheet->mergeCells('U11:Z11');

                $sheet->setCellValueByColumnAndRow(0, 12, 'с ' . $this->reportSettings->getDate()->format('d.m.Y') . ' по ' . $this->reportSettings->getDate()->format('t.m.Y'));

                $sheet->mergeCells('A12:Z12');
                $sheet->getStyle('A12:Z12')->getFont()->setBold(true);
                $sheet->getStyle('A12:Z12')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                $sheet->getRowDimension(13)->setRowHeight(9);

                $sheet->getCell('Z14')->setValue('Итого за расчетный день (формула)');
                $sheet->mergeCells('Z14:Z15');
                $sheet->getStyle('Z14:Z15')
                    ->applyFromArray([
                        'borders' => [
                            'top' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                            'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                            'right' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                            'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        ],
                        'alignment' => [
                            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
                        ],
                    ]);

                $sheet->getColumnDimension('Z')->setWidth(45);

                $row = 14;
                $sheet->setCellValueByColumnAndRow(0, $row, 'Дата');
                $sheet->getColumnDimension('A')->setWidth(13);
                $sheet->getStyle('A14:A15')
                    ->applyFromArray([
                        'borders' => [
                            'top' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                            'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                            'right' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                            'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        ],
                        'font' => ['bold' => true],
                        'alignment' => [
                            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
                        ],
                    ]);
                $sheet->mergeCells('A14:A15');

                $sheet->setCellValueByColumnAndRow(1, $row, 'Почасовые объемы потребления электроэнергии в кВтч');
                $sheet->getStyle('B14:Y14')
                    ->applyFromArray([
                        'borders' => [
                            'top' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                            'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                            'right' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                            'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                        ],
                        'font' => ['bold' => true],
                        'alignment' => ['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER],
                    ]);
                $sheet->mergeCells('B14:Y14');

                $row++;
                $sheet->setCellValueByColumnAndRow(0, $row, '');
                for ($i = 0; $i < 24; $i++) {
                    $this->createDataCell($sheet, $i + 1, $row, $i.'-'.($i + 1));
                }
                $row++;
                foreach ($values as $date => $value) {
                    $this->createDataCell($sheet, 0, $row, $date);
                    for ($i = 0; $i < 24; $i++) {
                        if (isset($value[$i])) {
                            $this->createDataCell($sheet, $i + 1, $row, $value[$i]);
                        }
                    }

                    $sheet->getCellByColumnAndRow($i + 1, $row)->getStyle()->applyFromArray([
                        'borders' => [
                            'top' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                            'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                            'right' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                            'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        ],
                    ]);

                    //set the SUM() formula
                    $dateFirstCell = $sheet->getCellByColumnAndRow(1, $row)->getCoordinate();
                    $dateLastCell = $sheet->getCellByColumnAndRow($i, $row)->getCoordinate();

                    $sheet->getCell('Z' . $row)->setValue('=SUM(' . $dateFirstCell . ':' . $dateLastCell . ')');

                    $row++;
                }

                //set data of the first period's hour for a total report
                $this->modifyTotalReportData(
                    $type,
                    $deviceId,
                    self::TOTAL_REPORT_KEY_FIRST_HOUR,
                    $sheet->getCell('B16')->getFormattedValue()
                );

                //set data of the last period's hour for a total report
                $this->modifyTotalReportData(
                    $type,
                    $deviceId,
                    self::TOTAL_REPORT_KEY_LAST_HOUR,
                    $sheet->getCell('Y' . ($row - 1))->getFormattedValue()
                );

                $sheet->setCellValueByColumnAndRow(0, $row, 'ВСЕГО за период');
                $range = 'A' . $row . ':B' . $row;
                $sheet->mergeCells($range);
                $sheet->getStyle($range)->applyFromArray([
                    'borders' => [
                        'top' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                        'left' => ['style' => \PHPExcel_Style_Border::BORDER_NONE],
                        'right' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                    ],
                    'font' => ['bold' => true],
                    'alignment' => ['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT],
                ]);

                $totalCell = 'Z' . $row;

                if (!in_array($device['id'], $this->devicesWithoutCoefficients)) {
                    $sheet->setCellValueByColumnAndRow(0, 7,
                        '=TEXT("Коэффициент трансформации: ", "#")&'
                        . $this->generateTransformCoefficientFormula($type, $totalCell, $device)
                    );
                } else {
                    $sheet->setCellValueByColumnAndRow(0, 7, '=TEXT("Коэффициент трансформации: -", "#")');
                }

                $range = 'C' . $row . ':Y' . $row;
                $sheet->mergeCells($range);
                $sheet->getStyle($range)->applyFromArray([
                    'borders' => [
                        'top' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                        'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        'right' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                    ],
                ]);

                $sheet->getStyle('Z16:Z' . $row)->applyFromArray([
                    'borders' => [
                        'top' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        'right' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                        'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                    ],
                    'alignment' => ['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER],
                ]);

                $sheet->getStyle('Z' . $row)->applyFromArray([
                    'borders' => [
                        'top' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                        'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        'right' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                        'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                    ],
                    'alignment' => ['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER],
                    'font' => ['bold' => true],
                ]);

                //set the total sum formula
                $sheet->getCell('Z' . $row)->setValue('=SUM(Z16:' . 'Z' . ($row - 1) . ')');

                //set the total value of period's data for a total report
                $this->modifyTotalReportData(
                    $type,
                    $deviceId,
                    self::TOTAL_REPORT_KEY_TOTAL,
                    $sheet->getCell('Z' . $row)->getFormattedValue()
                );

                $row += 2;

                $sheet->getCell('A' . $row)->setValue('«Гарантирующий поставщик»')
                    ->getStyle()->getFont()->setBold(true);
                $sheet->mergeCells('A' . $row . ':I' . $row);
                $sheet->getCell('J' . $row)->setValue('«Потребитель» («Покупатель»)')
                    ->getStyle()->getFont()->setBold(true);
                $sheet->mergeCells('J' . $row . ':R' . $row);

                $row += 3;

                $sheet->getCell('A' . $row)->setValue('М.П.       ________________');
                $sheet->mergeCells('A' . $row . ':I' . $row);
                $sheet->getCell('J' . $row)->setValue('М.П.       ________________');
                $sheet->mergeCells('J' . $row . ':R' . $row);

                $fileTitle = $data['title']
                    . ' '
                    . $deviceName
                    . $device['settings']['serial_number']
                    . ' ' . $this->energyTypes[$type] . '.' . ReportResult::TYPE_XLS;

                $fileName = $this->preparedCompanyTitle
                    . $this->transliterator->transliterate(' ' . $device['settings']['serial_number'] . ' '. $this->energyTypes[$type]);

                $sheet->setSelectedCells('A1');
                $this->saveXLSFile(
                    $phpExcelObject,
                    $saveDir,
                    $fileTitle,
                    $this->incompleteFlags[$deviceId][$type],
                    $fileName
                );

                unset($phpExcelObject);
            }
        }
    }

    /**
     * @param $energyType
     * @param $deviceId
     * @param $valueType
     * @param $value
     */
    private function modifyTotalReportData($energyType, $deviceId, $valueType, $value)
    {
        if (!array_key_exists($energyType, $this->totalReportData)) {
            $this->totalReportData[$energyType] = [];
        } elseif (!array_key_exists($deviceId, $this->totalReportData[$energyType])) {
            $this->totalReportData[$energyType][$deviceId] = [];
        }

        $this->totalReportData[$energyType][$deviceId][$valueType] = $value;
    }

    /**
     * Create the Excel formula to calculate a coefficient of transformation.
     *
     * @var string $type
     * @var string $totalCell
     * @var array $device
     *
     * @return string
     */
    private function generateTransformCoefficientFormula($type, $totalCell, $device)
    {
        $lineLossPercent = null;

        switch ($type) {
            case 'A+':
                $lineLossPercent = $device['settings']['percent_of_line_loss_ap'];
                break;
            case 'A-':
                $lineLossPercent = $device['settings']['percent_of_line_loss_am'];
                break;
            case 'R+':
                $lineLossPercent = $device['settings']['percent_of_line_loss_rp'];
                break;
            case 'R-':
                $lineLossPercent = $device['settings']['percent_of_line_loss_ap'];
                break;
        }

        return 'ROUND(' . $totalCell . '*' . $device['settings']['voltage_transformation_ratio'] * $device['settings']['current_transformation_ratio']  * $lineLossPercent . ", 3)";
    }

    /**
     * @param string $saveDir
     * @param array $data
     * @throws \PHPExcel_Exception
     */
    private function createDeviceSummaryXLSFiles(string $saveDir, array $data)
    {
        foreach ($this->energyTypes as $energyKey => $energyType) {
            $phpExcelObject = $this->createPhpExcelObject();
            $sheet = $phpExcelObject->getActiveSheet();
            $sheet->setTitle(self::XLS_DEFAULT_WORKSHEET_TITLE);

            $sheet->setCellValueByColumnAndRow(0, 1, 'Наименование потребителя: ' . $data['title']);
            $sheet->mergeCells('A1:Z1');

            $sheet->setCellValueByColumnAndRow(0, 2, 'Адрес потребителя: ' . $data['address']);
            $sheet->mergeCells('A2:Z2');

            $devicesNumbersListString = '';
            $devicesNamesListString = '';
            $i = 0;

            //concatenate all the serial numbers to a single string
            foreach ($data['data'] as $deviceId => $device) {
                $devicesNumbersListString .= $this->devicesData[$deviceId]['settings']['serial_number'];

                if (array_key_exists('name', $this->devicesData[$deviceId]['settings'])) {
                    $devicesNamesListString .= $this->devicesData[$deviceId]['settings']['name'];
                }

                if ($i != count($data['data']) - 1) {
                    $devicesNumbersListString .= ', ';

                    if ($devicesNamesListString) {
                        $devicesNamesListString .= ', ';
                    }
                }



                $i++;
            }

            $sheet->setCellValueByColumnAndRow(0, 3, 'Группа точек: ' . $devicesNamesListString);
            $sheet->mergeCells('A3:Z3');

            $sheet->setCellValueByColumnAndRow(0, 4, 'Адрес: ' . $data['address']);
            $sheet->mergeCells('A4:Z4');

            $sheet->getColumnDimension('Z')->setWidth(45);

            $sheet->setCellValueByColumnAndRow(0, 5, 'Прибор учета: ' . $devicesNumbersListString);
            $sheet->mergeCells('A5:Z5');
            $sheet->mergeCells('A6:Z6');

            $sheet->setCellValueByColumnAndRow(0, 7, 'Фактический почасовой объем потребления электрической энергии потребителем');
            $sheet->mergeCells('A7:Z7');
            $sheet->getStyle('A7:Z7')->getFont()->setBold(true);
            $sheet->getStyle('A7:Z7')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $sheet->mergeCells('A8:T8');

            $sheet->getCell('U8')->setValue($energyType)->getStyle()->getFont()->setBold(true);
            $sheet->mergeCells('U8:Z8');

            $sheet->setCellValueByColumnAndRow(0, 9, 'с ' . $this->reportSettings->getDate()->format('d.m.Y') . ' по ' . $this->reportSettings->getDate()->format('t.m.Y'));
            $sheet->getStyle('A9:Z9')->getFont()->setBold(true);
            $sheet->getStyle('A9:Z9')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->mergeCells('A9:Z9');

            $sheet->getRowDimension(10)->setRowHeight(9);

            $row = 11;
            $sheet->setCellValueByColumnAndRow(0, $row, 'Дата');
            $sheet->getStyle('A11:A12')
                ->applyFromArray([
                    'borders' => [
                        'top' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                        'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        'right' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                    ],
                    'font' => ['bold' => true],
                    'alignment' => [
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
                    ],
                ]);
            $sheet->mergeCells('A11:A12');

            $sheet->getCell('Z11')->setValue('Итого за расчетный день (формула)');
            $range = 'Z11:Z12';
            $sheet->mergeCells($range);
            $sheet->getStyle($range)->applyFromArray([
                'borders' => [
                    'top' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                    'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                    'right' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                    'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                ],
                'alignment' => [
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
                ],
            ]);


            $sheet->setCellValueByColumnAndRow(1, $row, 'Почасовые объемы потребления электроэнергии в кВтч');
            $sheet->getStyle('B11:Y11')
                ->applyFromArray([
                    'borders' => [
                        'top' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                        'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        'right' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                    ],
                    'font' => ['bold' => true],
                    'alignment' => ['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER],
                ]);
            $sheet->mergeCells('B11:Y11');

            $row++;
            $sheet->setCellValueByColumnAndRow(0, $row, '');
            for ($i = 0; $i < 24; $i++) {
                $this->createDataCell($sheet, $i + 1, $row, $i . '-' . ($i + 1));
            }
            $row++;
            foreach ($data['device_summary_data'][$energyKey] as $date => $value) {
                $this->createDataCell($sheet, 0, $row, $date);
                for ($i = 0; $i < 24; $i++) {
                    if (isset($value[$i])) {
                        $this->createDataCell($sheet, $i + 1, $row, $value[$i]);
                    }
                }

                $sheet->getCellByColumnAndRow($i + 1, $row)->getStyle()->applyFromArray([
                    'borders' => [
                        'top' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        'right' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                        'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                    ],
                ]);

                //set the SUM() formula
                $dateFirstCell = $sheet->getCellByColumnAndRow(1, $row)->getCoordinate();
                $dateLastCell = $sheet->getCellByColumnAndRow($i, $row)->getCoordinate();

                $sheet->getCell('Z' . $row)->setValue('=SUM(' . $dateFirstCell . ':' . $dateLastCell . ')');

                $row++;
            }

            $sheet->setCellValueByColumnAndRow(0, $row, 'ВСЕГО за период');
            $range = 'A' . $row . ':B' . $row;
            $sheet->mergeCells($range);
            $sheet->getStyle($range)->applyFromArray([
                'borders' => [
                    'top' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                    'left' => ['style' => \PHPExcel_Style_Border::BORDER_NONE],
                    'right' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                    'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                ],
                'font' => ['bold' => true],
                'alignment' => ['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT],
            ]);

            $range = 'C' . $row . ':Y' . $row;
            $sheet->mergeCells($range);
            $sheet->getStyle($range)->applyFromArray([
                'borders' => [
                    'top' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                    'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                    'right' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                    'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                ],
            ]);

            $sheet->getStyle('Z13:Z' . $row)->applyFromArray([
                'borders' => [
                    'top' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                    'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                    'right' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                    'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                ],
                'alignment' => ['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER],
            ]);

            $sheet->getStyle('Z' . $row)->applyFromArray([
                'borders' => [
                    'top' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                    'left' => ['style' => \PHPExcel_Style_Border::BORDER_THIN],
                    'right' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                    'bottom' => ['style' => \PHPExcel_Style_Border::BORDER_MEDIUM],
                ],
                'font' => ['bold' => true],
                'alignment' => ['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER],
            ]);

            //set the total sum formula
            $sheet->getCell('Z' . $row)->setValue('=SUM(Z13:' . 'Z' . ($row - 1) . ')');

            $row += 2;

            $sheet->getCell('A' . $row)->setValue('«Гарантирующий поставщик»')
                ->getStyle()->getFont()->setBold(true);
            $sheet->mergeCells('A' . $row . ':I' . $row);
            $sheet->getCell('J' . $row)->setValue('«Потребитель» («Покупатель»)')
                ->getStyle()->getFont()->setBold(true);
            $sheet->mergeCells('J' . $row . ':R' . $row);

            $row += 2;

            $sheet->getCell('A' . $row)->setValue('М.П.       ________________');
            $sheet->mergeCells('A' . $row . ':I' . $row);
            $sheet->getCell('J' . $row)->setValue('М.П.       ________________');
            $sheet->mergeCells('J' . $row . ':R' . $row);

            $fileTitle = $data['title']
                . ' '
                . 'сумма'
                . ' ' . $energyType . '.' . ReportResult::TYPE_XLS;

            $fileName = $this->preparedCompanyTitle . $this->transliterator->transliterate(' ' . 'сумма' . ' '. $energyType);

            $sheet->setSelectedCells('A1');
            $this->saveXLSFile(
                $phpExcelObject,
                $saveDir,
                $fileTitle,
                $this->incompleteFlags[self::INCOMPLETE_FLAGS_SUMMARY_KEY][$energyKey],
                $fileName
            );

            unset($phpExcelObject);
        }
    }

    /**
     * Prepare data from metering devices
     *
     * @param $deviceId
     * @return array
     */
    protected function prepareMeteringData($deviceId)
    {
    }

    /**
     * @param $item
     * @param $result
     * @param $deviceId
     * @param $date
     */
    protected function prepareHourlyData($item, &$result, $deviceId, $date)
    {
        for ($i = 0; $i < 24; $i++) {
            $sum = bcadd($item['values'][$i * 2], $item['values'][($i * 2) + 1]);
            $result[$deviceId][$item['type']][$date][$i] = self::convertWattsToKilowatts($sum);
        }
    }

    /**
     * @param $value
     * @param $result
     * @param $deviceId
     * @param $date
     */
    protected function prepareDailyData($value, &$result, $deviceId, $date)
    {
        foreach ($value as $item) {
            if ($item['type'] !== 'A-') {
                $result[$deviceId][$item['type']][$date] = [];
                if (count($item['values']) === 48) {
                    $this->prepareHourlyData($item, $result, $deviceId, $date);

                    $this->incompleteFlags[$deviceId][$item['type']] = false;
                } else {
                    //set an incomplete flag to this type of XLS report
                    $this->incompleteFlags[$deviceId][$item['type']] = true;
                }
            }
        }
    }

    /**
     * @param \PHPExcel_Worksheet $sheet
     * @param int                 $column
     * @param int                 $row
     * @param string              $value
     *
     * @throws \PHPExcel_Exception
     */
    protected function createDataCell(\PHPExcel_Worksheet $sheet, int $column, int $row, string $value)
    {
        $sheet->setCellValueByColumnAndRow($column, $row, $value);
        $sheet->getStyleByColumnAndRow($column, $row)
            ->applyFromArray([
                'alignment' => ['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER],
                'borders' => $this->bordersStyle
            ]);
    }
}