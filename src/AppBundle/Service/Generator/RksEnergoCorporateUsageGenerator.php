<?php

namespace AppBundle\Service\Generator;

use AppBundle\Entity\ReportResult;
use PHPExcel_Worksheet;
use Symfony\Component\Intl\DateFormatter\IntlDateFormatter;

class RksEnergoCorporateUsageGenerator extends AbstractReportGenerator
{
    /**
     * @var string
     */
    private $preparedCompanyTitle;

    /**
     * Preparing data for report.
     *
     * @return array
     */
    protected function prepareData()
    {
        $data = $this->prepareAllMeteringData();
        $project = reset($this->projectData);

        $this->preparedCompanyTitle = $this->prepareCompanyTitle($project['settings']['company']['name']);

        $contractNumber = $project['settings']['electricity_provider']['contract_number'];
        $contractDate = null;
        if (!empty($project['settings']['electricity_provider']['contract_date'])) {
            $contractDate = \DateTime::createFromFormat(
                DATE_ISO8601,
                $project['settings']['electricity_provider']['contract_date']
            )->format('d.m.Y');
        }

        return [
            'title' => $project['title'],
            'address' => $project['settings']['company']['address'],
            'contract_number' => $contractNumber,
            'contract_date' => $contractDate,
            'data' => $data,
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function prepareAllMeteringData()
    {
        $result = [];

        foreach ($this->usageData as $deviceId => $values) {
            $result[$deviceId] = [];
            foreach ($values as $date => $value) {
                $date = \DateTime::createFromFormat(DATE_ISO8601, $date)->format('d.m.Y');
                foreach ($value as $item) {
                    $result[$deviceId][$item['type']][$date] = [];
                    if (count($item['values']) === 48) {
                        $data = $this->prepareHourlyData($item);
                        $result[$deviceId][$item['type']][$date] = $data;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $item
     * @return array
     */
    protected function prepareHourlyData($item): array
    {
        $result = [];
        for ($i = 0; $i < 24; $i++) {
            $sum = bcadd($item['values'][$i * 2], $item['values'][($i * 2) + 1]);
            $result[$i] = self::convertWattsToKilowatts($sum);
        }

        return $result;
    }

    /**
     * Create XLS file from data and save it to directory
     *
     * @param string $saveDir
     * @param array $data
     */
    protected function createXLSFiles(string $saveDir, array $data)
    {
        $monthName = $this->getMonthName($this->reportSettings->getDate());
        $year = $this->reportSettings->getDate()->format('Y');

        foreach ($data['data'] as $deviceId => $values) {
            $device = $this->devicesData[$deviceId];

            $phpExcelObject = $this->createPhpExcelObject();
            $sheet = $phpExcelObject->getActiveSheet();
            $sheet->setTitle(self::XLS_DEFAULT_WORKSHEET_TITLE);
            $sheet->freezePane('A16');

            $narrowRows = [5, 7, 10, 11, 12];
            foreach ($narrowRows as $narrowRow) {
                $sheet->getRowDimension($narrowRow)->setRowHeight(13);
            }

            $this->createTableHeader($sheet, $data, $device, $monthName, $year);
            $this->createTableHeaderTimeRange($sheet);

            $sheet->setCellValue('Z13', 'Итого');

            $row = 16;
            $overAllSum = 0;
            $isIncomplete = false;

            //check for the data completeness and set the flags
            foreach ($values['A+'] as $date => $items) {
                $sum = 0;
                $sheet->setCellValueByColumnAndRow(0, $row, $date);
                for ($i = 0; $i < 24; $i++) {
                    if (array_key_exists($i, $items)) {
                        $value = $items[$i];
                        $sum = bcadd($sum, $value);
                    } else {
                        $value = '-';
                        $isIncomplete = true;
                        $this->setIncompleteFlagForZipAndJson();
                    }

                    $sheet->setCellValueByColumnAndRow($i + 1, $row, $value);
                }
                $sheet->setCellValue('Z' . $row, $sum);
                $overAllSum = bcadd($overAllSum, $sum);
                $row++;
            }
            $sheet->setCellValue('Z' . $row, $overAllSum);

            $fileTitleEnding = ' активная энергия прием';
            $fileNameEnding = $this->transliterator->transliterate($fileTitleEnding);
            $fileTitle = $device['settings']['serial_number'] . $fileTitleEnding . '.' . ReportResult::TYPE_XLS;
            $fileName = $device['settings']['serial_number'] . $fileNameEnding;

            $sheet->setSelectedCells('A1');
            $this->saveXLSFile($phpExcelObject, $saveDir, $fileTitle, $isIncomplete, $fileName);
        }
    }

    /**
     * @inheritdoc
     */
    protected function setIncompleteFlagForZipAndJson()
    {
        if ($this->incompleteFlagZipJson !== true) {
            $this->incompleteFlagZipJson = true;
        }
    }

    /**
     * @param string $rawTitle
     * @return string
     */
    private function prepareCompanyTitle($rawTitle)
    {
        return substr($this->transliterator->transliterate($rawTitle), 0, 25);
    }

    /**
     * @inheritdoc
     */
    protected function assignZipFileName()
    {
        return $this->preparedCompanyTitle . '.' . ReportResult::TYPE_ZIP;
    }

    /**
     * @inheritdoc
     */
    protected function assignJsonFileName()
    {
        return $this->preparedCompanyTitle . '.' . ReportResult::TYPE_JSON;
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     * @param array $data
     * @param $device
     * @param $monthName
     * @param $year
     * @throws \PHPExcel_Exception
     */
    private function createTableHeader(PHPExcel_Worksheet $sheet, array $data, $device, $monthName, $year)
    {
        $sheet->setCellValue('V1', 'ПРИЛОЖЕНИЕ 4.1.');
        $sheet->mergeCells('V1:W1');

        $sheet->setCellValue('V2', 'К');
        $sheet->setCellValue('W2', $data['contract_number']);
        $sheet->setCellValue('V3', 'oт');
        $sheet->setCellValue('W3', $data['contract_date']);
        $sheet->setCellValue('V4', 'для');

        $project = reset($this->projectData);
        $sheet->setCellValue('W4', $project['settings']['electricity_provider']['contract_number']);

        $sheet->setCellValue('D6', 'Энергоснабжаемый объект:');
        $sheet->setCellValue('E6', $device['settings']['address']);
        $sheet->mergeCells('E6:L6');
        $sheet->setCellValue('M6', '№ счетчика сумматора');
        $sheet->mergeCells('M6:O6');
        $sheet->setCellValue('P6', $device['settings']['serial_number']);

        $sheet->setCellValue('M8', 'Отчет о почасовом потреблении электроэнергии');
        $sheet->setCellValue('K9', 'за');
        $sheet->setCellValue('L9', mb_strtolower($monthName));
        $sheet->setCellValue('N9', $year);

        $sheet->setCellValue('A13', 'Дата / Время');
    }

    /**
     * @param \DateTime $date
     * @return string
     */
    private function getMonthName(\DateTime $date): string
    {
        $monthFormater = \IntlDateFormatter::create(
            'ru_RU',
            \IntlDateFormatter::FULL,
            \IntlDateFormatter::FULL,
            null,
            null,
            'LLLL'
        );

        return $monthFormater->format($date);
    }

    /**
     * Prepare data from metering devices
     * @deprecated
     * Use prepareAllMeteringData instead
     *
     * @param $deviceId
     * @return array
     */
    protected function prepareMeteringData($deviceId)
    { }

    /**
     * @param PHPExcel_Worksheet $sheet
     */
    private function createTableHeaderTimeRange(PHPExcel_Worksheet $sheet)
    {
        for ($i = 0; $i < 24; $i++) {
            $first = $i;
            $second = $i + 1;

            if ($first === 23) {
                $second = 0;
            }

            $sheet->setCellValueByColumnAndRow($i + 1, 13, $first . ':00');
            $sheet->setCellValueByColumnAndRow($i + 1, 14, '-');
            $sheet->setCellValueByColumnAndRow($i + 1, 15, $second . ':00');
        }
    }
}