<?php

namespace AppBundle\Service\Generator;

use AppBundle\Entity\ReportResult;
use AppBundle\Exceptions\ApiException;
use AppBundle\Model\ReportSettingsInterface;

class GroupUsageGenerator extends AbstractReportGenerator
{
    const REPORT_TITLE_ENDING = 'Потребление групп ПУ';

    protected function prepareData()
    {
        $reportTitle = $this->reportSettings->getTitle() . ' – ' . self::REPORT_TITLE_ENDING;

        $viewType = $this->viewTypeNames[$this->reportSettings->getViewType()];
        $detailTypeId = $this->reportSettings->getDetailType();
        $detailType = $this->detailTypeNames[$detailTypeId];
        $groups = count($this->reportSettings->getGroups()) > 0
            ? 'группы'
            : 'не выбраны';
        $dateStart = $this->reportSettings->getDateStart()->format('d.m.Y');
        $dateEnd = $this->reportSettings->getDateEnd()->format('d.m.Y');

        $description = sprintf(
            'Тип данных: %s; Детализация: %s; Группы: %s; Дата с %s по %s',
            $viewType, $detailType, $groups,
            $dateStart, $dateEnd
        );

        $this->sortUsageDataByAddress();
        $data = [];

        foreach ($this->usageData as $deviceId => $item) {
            if ($detailTypeId == ReportSettingsInterface::DETAIL_MONTH) {
                $data[] = $this->prepareAllMeteringDataMonthlyByDays($deviceId);
            } elseif ($detailTypeId == ReportSettingsInterface::DETAIL_HALF_HOUR) {
                $data[$deviceId] = $this->prepareMeteringDataHalfHourly($deviceId);
            } else {
                $data[] = $this->prepareMeteringData($deviceId);
            }
        }

        return [
            'title' => $reportTitle,
            'description' => [
                'value' => $description,
                'items' => [
                    'view_type' => $viewType,
                    'detail_type' => $detailType,
                    'groups' => $groups,
                    'date_start' => $dateStart,
                    'date_end' => $dateEnd
                ]
            ],
            'data' => $data
        ];
    }

    /**
     * Sort the report data by device addresses
     *
     * @return void
     */
    private function sortUsageDataByAddress()
    {
        uksort($this->usageData, function($a, $b) {
            $firstAddress = $this->devicesData[$a]['settings']['address'];
            $secondAddress = $this->devicesData[$b]['settings']['address'];
            $placeNumberPrefix = 'Участок №';

            if (strpos($firstAddress, $placeNumberPrefix) !== false && strpos($secondAddress, $placeNumberPrefix) !== false) {
                $a = (int) trim($firstAddress, $placeNumberPrefix);
                $b = (int) trim($secondAddress, $placeNumberPrefix);
            }

            return $a <=> $b;
        });
    }

    /**
     * Create XLS file from data and save it to directory
     *
     * @param string $saveDir
     * @param array $data
     */
    protected function createXLSFiles(string $saveDir, array $data)
    {
        $detailType = $this->reportSettings->getDetailType();

        if ($detailType === ReportSettingsInterface::DETAIL_HALF_HOUR) {
            $this->createHalfHoursXLSFile($saveDir, $data);
        } else {
            $this->createDaysXLSFile($saveDir, $data);
        }
    }

    /**
     * Create XLS report detailed by half hours.
     *
     * @param string $saveDir
     * @param array  $data
     *
     * @throws \PHPExcel_Exception
     */
    private function createHalfHoursXLSFile(string $saveDir, array $data)
    {
        $phpExcelObject = $this->createPhpExcelObject();
        $sheet = $phpExcelObject->getActiveSheet();
        $sheet->setTitle(self::XLS_DEFAULT_WORKSHEET_TITLE);
        $sheet->freezePane('A7');

        foreach (range('a', 'd') as $item) {
            $sheet->getColumnDimension(strtoupper($item))->setAutoSize(true);
        }

        $tableTitles = [
            'адрес установки',
            'серийный номер',
            'MAC адрес',
            'Тип',
        ];

        $sheet->setCellValueByColumnAndRow(0, 1, $data['title']);
        $this->styleForTitleRow($sheet);

        $sheet->setCellValueByColumnAndRow(0, 3, $data['description']['value']);
        $this->styleForSubtitleRow($sheet);

        $sheet->fromArray($tableTitles, null, 'A6');
        $sheet->getStyle('A6:D6')->getFont()->setBold(true);

        $colNumber = 4;
        $rowNumber = 6;

        $dates = $this->createHalfHourDatesRange();
        foreach ($dates as $date) {
            $cell = $sheet->setCellValueByColumnAndRow($colNumber, $rowNumber, '=TEXT("' . $date . '", "@")', $returnCell = true);
            $cell->getStyle()->getFont()->setBold(true);
            $colNumber++;
        }

        $rowNumber = 7;
        $colNumber = 4;

        foreach ($data['data'] as $deviceId => $item) {
            $sheet->setCellValueByColumnAndRow(0, $rowNumber, $item['address']);
            $cell = $sheet->setCellValueByColumnAndRow(1, $rowNumber, $item['serial_number'], $returnCell = true);
            $cell->getStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->setCellValueByColumnAndRow(2, $rowNumber, $item['zigbee_mac']);

            if ($rowCount = count($item['data']) > 1) {
                $this->mergeAndVerticalAlignStyle($sheet, 'A', $rowNumber, $rowNumber + $rowCount);
                $this->mergeAndVerticalAlignStyle($sheet, 'B', $rowNumber, $rowNumber + $rowCount);
                $this->mergeAndVerticalAlignStyle($sheet, 'C', $rowNumber, $rowNumber + $rowCount);
            }

            foreach ($item['data'] as $type => $values) {
                $sheet->setCellValueByColumnAndRow(3, $rowNumber, $type);
                $col = 0;
                foreach ($dates as $date) {
                    if (array_key_exists($date, $values)) {
                        $sheet->setCellValueByColumnAndRow($colNumber + $col, $rowNumber, $values[$date]);
                    } else {
                        $sheet->setCellValueByColumnAndRow($colNumber + $col, $rowNumber, '-');
                    }
                    $col++;
                }
                $rowNumber++;
            }
        }

        $fileName = $this->transliterator->transliterate($data['title']);

        $this->saveXLSFile($phpExcelObject, $saveDir, $data['title'].'.'.ReportResult::TYPE_XLS, false, $fileName);
    }

    /**
     * Create XLS report detailed by days/month.
     *
     * @param string $saveDir
     * @param array $data
     * @throws \PHPExcel_Exception
     */
    private function createDaysXLSFile(string $saveDir, array $data)
    {
        $detailType = $this->reportSettings->getDetailType();

        $phpExcelObject = $this->createPhpExcelObject();
        $sheet = $phpExcelObject->getActiveSheet();
        $sheet->setTitle(self::XLS_DEFAULT_WORKSHEET_TITLE);
        $sheet->freezePane('A7');

        foreach (range('a', 'd') as $item) {
            $sheet->getColumnDimension(strtoupper($item))->setAutoSize(true);
        }

        $tableTitles = [
            'адрес установки',
            'серийный номер',
            'MAC адрес',
            'Тип',
            'Тариф'
        ];

        /** @var \DateTime $start */
        $start = $this->reportSettings->getDateStart();
        /** @var \DateTime $end */
        //adding the minute is required to include the end of period to itself
        $end = $this->reportSettings->getDateEnd()->modify('+ 1 minute');

        if ($detailType == ReportSettingsInterface::DETAIL_MONTH) {
            $period = new \DatePeriod($start, \DateInterval::createFromDateString('1 month'), $end);
        } else {
            $period = new \DatePeriod($start, \DateInterval::createFromDateString('1 day'), $end);
        }

        $dateRange = [];

        // Set table titles
        $sheet->setCellValueByColumnAndRow(0, 1, $data['title']);
        $this->styleForTitleRow($sheet);
        $sheet->setCellValueByColumnAndRow(0, 3, $data['description']['value']);
        $this->styleForSubtitleRow($sheet);

        $sheet->fromArray($tableTitles, null, 'A6');
        $sheet->getStyle('A6:E6')->getFont()->setBold(true);

        $colNumber = 5;
        $rowNumber = 6;

        foreach ($period as $key => $date) {
            if ($detailType == ReportSettingsInterface::DETAIL_MONTH) {
                $date = $date->format('m.Y');
            } else {
                $date = $date->format('d.m.Y');
            }

            $dateRange[] = $date;

            $cell = $sheet->setCellValueByColumnAndRow($colNumber, $rowNumber, '=TEXT("' . $date . '", "@")', $returnCell = true);
            $cell->getStyle()->getFont()->setBold(true);
            $colNumber++;
        }

        $rowNumber = 7;

        $sumData = [];

        // Show data rows
        // In this reports we show groups of data.
        foreach ($data['data'] as $key => $item) {

            $this->addTotalValues($item, $sumData);

            // First we show info about device
            $sheet->setCellValueByColumnAndRow(0, $rowNumber, $item['address']);
            $this->mergeAndVerticalAlignStyle($sheet, 'A', $rowNumber, $rowNumber + 11);

            $sheet->setCellValueByColumnAndRow(1, $rowNumber, $item['serial_number']);
            $sheet->getCellByColumnAndRow(1, $rowNumber)->getStyle()
                ->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->mergeAndVerticalAlignStyle($sheet, 'B', $rowNumber, $rowNumber + 11);

            $sheet->setCellValueByColumnAndRow(2, $rowNumber, $item['zigbee_mac']);
            $this->mergeAndVerticalAlignStyle($sheet, 'C', $rowNumber, $rowNumber + 11);


            // Second we set titles for variants
            // And they go in groups of 3 elements
            $sheet->setCellValueByColumnAndRow(3, $rowNumber, 'A+');
            $this->mergeAndVerticalAlignStyle($sheet, 'D', $rowNumber, $rowNumber + 2);

            $sheet->setCellValueByColumnAndRow(3, $rowNumber + 3, 'A-');
            $this->mergeAndVerticalAlignStyle($sheet, 'D', $rowNumber + 3, $rowNumber + 5);

            $sheet->setCellValueByColumnAndRow(3, $rowNumber + 6, 'R+');
            $this->mergeAndVerticalAlignStyle($sheet, 'D', $rowNumber + 6, $rowNumber + 8);

            $sheet->setCellValueByColumnAndRow(3, $rowNumber + 9, 'R-');
            $this->mergeAndVerticalAlignStyle($sheet, 'D', $rowNumber + 9, $rowNumber + 11);

            $this->drawTariffsColumn($sheet, $rowNumber);

            $startDataCol = 5;
            $colNumber = $startDataCol;

            // Now set data
            foreach ($item['data'] as $dateKey => $values) {
                if (!in_array($dateKey, $dateRange, true)) {
                    continue;
                }
                $tmpRow = $rowNumber;

                $aPlus = isset($values['A+']) ? $values['A+'] : [];
                $aMinus = isset($values['A-']) ? $values['A-'] : [];
                $rPlus = isset($values['R+']) ? $values['R+'] : [];
                $rMinus = isset($values['R-']) ? $values['R-'] : [];

                // Each group separately
                $this->writeMetteringDataToSheet($aPlus, $sheet, $colNumber, $tmpRow);
                $this->writeMetteringDataToSheet($aMinus, $sheet, $colNumber, $tmpRow+3);
                $this->writeMetteringDataToSheet($rPlus, $sheet, $colNumber, $tmpRow+6);
                $this->writeMetteringDataToSheet($rMinus, $sheet, $colNumber, $tmpRow+9);

                $colNumber++;
            }
            $rowNumber += 12;
        }

        //print the total rows
        $sheet->setCellValueByColumnAndRow(0, $rowNumber, 'Суммарное потребление');
        $this->mergeAndVerticalAlignStyle($sheet, 'A', $rowNumber, $rowNumber + 11);

        $this->mergeAndVerticalAlignStyle($sheet, 'B', $rowNumber, $rowNumber + 11);
        $this->mergeAndVerticalAlignStyle($sheet, 'C', $rowNumber, $rowNumber + 11);

        $sheet->setCellValueByColumnAndRow(3, $rowNumber, 'A+');
        $this->mergeAndVerticalAlignStyle($sheet, 'D', $rowNumber, $rowNumber + 2);
        $sheet->setCellValueByColumnAndRow(3, $rowNumber + 3, 'A-');
        $this->mergeAndVerticalAlignStyle($sheet, 'D', $rowNumber + 3, $rowNumber + 5);
        $sheet->setCellValueByColumnAndRow(3, $rowNumber + 6, 'R+');
        $this->mergeAndVerticalAlignStyle($sheet, 'D', $rowNumber + 6, $rowNumber + 8);
        $sheet->setCellValueByColumnAndRow(3, $rowNumber + 9, 'R-');
        $this->mergeAndVerticalAlignStyle($sheet, 'D', $rowNumber + 9, $rowNumber + 11);

        $this->drawTariffsColumn($sheet, $rowNumber);
        $colNumber = 5;

        //print the total values for each data
        foreach ($sumData as $dateKey => $values) {
            if (!in_array($dateKey, $dateRange, true)) {
                continue;
            }

            $aPlus = isset($values['A+']) ? $values['A+'] : [];
            $aMinus = isset($values['A-']) ? $values['A-'] : [];
            $rPlus = isset($values['R+']) ? $values['R+'] : [];
            $rMinus = isset($values['R-']) ? $values['R-'] : [];



            // Each group separately
            $this->writeMetteringDataToSheet($aPlus, $sheet, $colNumber, $rowNumber);
            $this->writeMetteringDataToSheet($aMinus, $sheet, $colNumber, $rowNumber + 3);
            $this->writeMetteringDataToSheet($rPlus, $sheet, $colNumber, $rowNumber + 6);
            $this->writeMetteringDataToSheet($rMinus, $sheet, $colNumber, $rowNumber + 9);

            $colNumber++;
        }

        $sheet->setSelectedCells('A1');
        $fileName = $this->transliterator->transliterate($data['title']);

        $this->saveXLSFile($phpExcelObject, $saveDir, $data['title'], false, $fileName);
    }

    /**
     * Add the new data to array of total values by its reference.
     *
     * @param array $item
     * @param array &$sumData
     */
    private function addTotalValues(array $item, array &$sumData)
    {
        $energyTypes = ['A+', 'A-', 'R+', 'R-'];

        //generate the rows with total values
        foreach ($item['data'] as $dateKey => $typeData) {
            if (!array_key_exists($dateKey, $sumData)) {
                $sumData[$dateKey] = [];
            }

            for ($i = 0; $i < 3; $i++) {
                foreach ($energyTypes as $energyType) {
                    if (!array_key_exists($energyType, $sumData[$dateKey])) {
                        $sumData[$dateKey][$energyType] = [];
                    }

                    if (!array_key_exists($i, $sumData[$dateKey][$energyType])) {
                        $sumData[$dateKey][$energyType][$i] = 0;
                    }
                }
            }

            for ($i = 0; $i < 3; $i++) {
                foreach ($energyTypes as $energyType) {
                    if (array_key_exists($energyType, $typeData) && array_key_exists($i, $typeData[$energyType])) {
                        $sumData[$dateKey][$energyType][$i] = bcadd($sumData[$dateKey][$energyType][$i], $typeData[$energyType][$i]);
                    }
                }
            }
        }
    }

    /**
     * Draw the tariffs column in a given sheet.
     *
     * @param \PHPExcel_Worksheet $sheet
     * @param int $rowNumber
     */
    private function drawTariffsColumn(\PHPExcel_Worksheet $sheet, int $rowNumber)
    {
        $sheet->setCellValueByColumnAndRow(4, $rowNumber, 'Сумма');
        $sheet->setCellValueByColumnAndRow(4, $rowNumber + 1, 'День');
        $sheet->setCellValueByColumnAndRow(4, $rowNumber + 2, 'Ночь');
        $sheet->setCellValueByColumnAndRow(4, $rowNumber + 3, 'Сумма');
        $sheet->setCellValueByColumnAndRow(4, $rowNumber + 4, 'День');
        $sheet->setCellValueByColumnAndRow(4, $rowNumber + 5, 'Ночь');
        $sheet->setCellValueByColumnAndRow(4, $rowNumber + 6, 'Сумма');
        $sheet->setCellValueByColumnAndRow(4, $rowNumber + 7, 'День');
        $sheet->setCellValueByColumnAndRow(4, $rowNumber + 8, 'Ночь');
        $sheet->setCellValueByColumnAndRow(4, $rowNumber + 9, 'Сумма');
        $sheet->setCellValueByColumnAndRow(4, $rowNumber + 10, 'День');
        $sheet->setCellValueByColumnAndRow(4, $rowNumber + 11, 'Ночь');
    }

    protected function prepareMeteringData($deviceId)
    {
        $result = [];

        $data = $this->usageData[$deviceId];
        $device = $this->devicesData[$deviceId];

        $result['address'] = $device['settings']['address'];
        $result['serial_number'] = $device['settings']['serial_number'];
        $result['zigbee_mac'] = $device['settings']['zigbee_mac'];

        foreach ($data as $date => $item) {
            $row = [];
            foreach ($item as $value) {
                $row[$value['type']] = self::convertArrayOfWattsToKilowatts($value['values']);
            }

            $result['data'][\DateTime::createFromFormat(DATE_ISO8601, $date)->format('d.m.Y')] = $row;
        }

        return $result;
    }

    /**
     * Prepare all metering data for the months by given days.
     *
     * @param int $deviceId
     * @return array
     */
    protected function prepareAllMeteringDataMonthlyByDays($deviceId)
    {
        $result = [];

        $data = $this->usageData[$deviceId];
        $device = $this->devicesData[$deviceId];

        $result['data'] = [];
        $result['address'] = $device['settings']['address'];
        $result['serial_number'] = $device['settings']['serial_number'];
        $result['zigbee_mac'] = $device['settings']['zigbee_mac'];

        foreach ($data as $date => $item) {
            $monthYearDate = \DateTime::createFromFormat(DATE_ISO8601, $date)->format('m.Y');
            $tmp = [];

            foreach ($item as $value) {
                $tmp[$value['type']] = self::convertArrayOfWattsToKilowatts($value['values']);
            }

            $isNewMonth = false;

            foreach ($tmp as $type => $typeData) {
                for ($i = 0; $i < 3; $i++) {
                    if (array_key_exists($i, $typeData)) {
                        if (!$isNewMonth && array_key_exists($monthYearDate, $result['data'])) {
                            $result['data'][$monthYearDate][$type][$i] += $typeData[$i];
                        } else {
                            $isNewMonth = true;
                            $result['data'][$monthYearDate][$type]['type'] = $type;
                            $result['data'][$monthYearDate][$type][$i] = $typeData[$i];

                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $values
     * @param $sheet
     * @param $colNumber
     * @param $rowNumber
     *
     * @return array
     */
    protected function writeMetteringDataToSheet($values, $sheet, $colNumber, $rowNumber)
    {
        for ($i = 0; $i < 3; $i++) {
            $value = isset($values[$i])
                ? $values[$i]
                : '-';

            $sheet->setCellValueByColumnAndRow($colNumber, $rowNumber + $i, $value);
        }
    }

    /**
     * @inheritdoc
     */
    protected function styleForSubtitleRow(\PHPExcel_Worksheet $sheet)
    {
        $sheet->mergeCells('A3:H3');
        $sheet->getStyle('A3:H3')->getFont()->setItalic(true);
    }


    /**
     * @param int $deviceId
     * @throws ApiException
     * @return array
     */
    protected function prepareMeteringDataHalfHourly($deviceId)
    {
        if ($this->reportSettings->getViewType() == ReportSettingsInterface::VIEW_TYPE_BY_INDICATION) {
            throw new ApiException(self::ERROR_UNSUPPORTED_HALF_HOUR_REPORT, self::ERROR_UNSUPPORTED_HALF_HOUR_REPORT);
        }

        $device = $this->devicesData[$deviceId];

        $result = [];
        $result['address'] = $device['settings']['address'];
        $result['serial_number'] = $device['settings']['serial_number'];
        $result['zigbee_mac'] = $device['settings']['zigbee_mac'];

        $result['data'] = [];
        $halfHourDates = $this->createHalfHourDatesRange();

        $data = $this->usageData[$deviceId];
        $halfHoursDailyStart = 0;

        foreach ($data as $dayDate => $items) {
            foreach ($items as $item) {
                if (count($item['values'])) {
                    for ($i = 0; $i < 48; $i++) {
                        $handledValue = isset($item['values'][$i]) ? $this::convertWattsToKilowatts($item['values'][$i]) : 0;

                        if ($halfHoursDailyStart == count($halfHourDates)) {
                            $result['data'][$item['type']][$halfHourDates[$halfHoursDailyStart - 1]] = $handledValue;
                        } elseif ($halfHoursDailyStart > count($halfHourDates)) {
                            break;
                        } else {
                            $result['data'][$item['type']][$halfHourDates[$halfHoursDailyStart + $i]] = $handledValue;
                        }

                        if ($i === 47) {
                            $halfHoursDailyStart += 48;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Create an array with dates by report's detail type.
     *
     * @return array
     */
    protected function createHalfHourDatesRange()
    {
        if (!$this->reportSettings->getDateStart() || !$this->reportSettings->getDateEnd()) {
            return null;
        }

        $dateStart = $this->reportSettings->getDateStart();
        $dateEnd = $this->reportSettings->getDateEnd();

        $dateInterval = new \DatePeriod($dateStart, new \DateInterval('PT30M'), $dateEnd);
        $dates = [];

        /** @var \DateTime $date */
        foreach ($dateInterval as $date) {
            $dateLabel = $date->format('d.m H:i');
            $date->modify('+30 minutes');

            $dates[] = $dateLabel.' - '.$date->format('H:i');
        }

        return $dates;
    }
}
