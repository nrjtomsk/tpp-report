<?php

namespace AppBundle\Service;

use AppBundle\Entity\ReportRequest;
use AppBundle\Entity\ReportFile;
use AppBundle\Exceptions\ReportFileNotExistsException;
use AppBundle\Exceptions\ReportNotReadyException;
use AppBundle\Exceptions\UnzippingException;
use AppBundle\Service\Generator\ReportGeneratorFactory;
use AppBundle\Service\Generator\ReportGeneratorInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Filesystem\Filesystem;

class GenerateReportService
{
    /**
     * @var ObjectManager
     */
    private $manager;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var ReportGeneratorFactory
     */
    private $reportGeneratorFactory;

    /**
     * GenerateReportService constructor.
     *
     * @param ObjectManager $manager
     * @param Filesystem $filesystem
     * @param ReportGeneratorFactory $reportGeneratorFactory
     */
    public function __construct(
        ObjectManager $manager,
        Filesystem $filesystem,
        ReportGeneratorFactory $reportGeneratorFactory
    )
    {
        $this->manager = $manager;
        $this->filesystem = $filesystem;
        $this->reportGeneratorFactory = $reportGeneratorFactory;
    }

    /**
     * Generates reportRequest files for ready reportRequest
     *
     * @param ReportRequest $report
     *
     *@throws \Exception
     */
    public function generate(ReportRequest $report)
    {
        if (!$report->hasData()) {
            throw new ReportNotReadyException($report);
        }

        /** @var ReportFile $file */
        $file = $report->getDataFile();
        $this->prepareFiles($file);

        /** @var ReportGeneratorInterface $generator */
        $generator = $this->reportGeneratorFactory->create($report->getCode());
        $generator->generate($report);
    }

    /**
     * Extracting data files for reportRequest in data folder
     *
     * @param ReportFile $file
     *
     * @throws \Exception
     */
    protected function prepareFiles(ReportFile $file)
    {
        if (!$this->filesystem->exists($file->getFilePath())) {
            throw new ReportFileNotExistsException($file, $file->getReportRequest());
        }

        $fileInfo = pathinfo($file->getFilePath());
        $extractDir = $fileInfo['dirname'] . DIRECTORY_SEPARATOR . $fileInfo['filename'];

        /** TODO: now we handle tar.gz. If we need zip also, do it next iteration */
        try {
            $dataPhar = new \PharData($file->getFilePath());
            $dataPhar->decompress();
            $dataPhar->extractTo($extractDir);
        } catch (\Exception $e) {
            /** TODO: throw specific exception */
            throw new UnzippingException($e, $file->getReportRequest());
        }

        unset($dataPhar);
    }
}