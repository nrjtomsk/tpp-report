<?php

namespace AppBundle\Service;

use AppBundle\Entity\ReportRequest;
use AppBundle\Exceptions\ValidationException;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use AppBundle\DTO;

class ReportRequestManager
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var ReportSettingsModelFactory
     */
    private $settingsModelFactory;
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * ReportManager constructor.
     *
     * @param ObjectManager              $manager
     * @param ValidatorInterface         $validator
     *
     * @param ReportSettingsModelFactory $settingsModelFactory
     *
     * @param RouterInterface            $router
     */
    public function __construct(
        ObjectManager $manager,
        ValidatorInterface $validator,
        ReportSettingsModelFactory $settingsModelFactory,
        RouterInterface $router
    )
    {
        $this->manager = $manager;
        $this->validator = $validator;
        $this->settingsModelFactory = $settingsModelFactory;
        $this->router = $router;
    }

    /**
     * @param string $code
     * @param string $project
     * @param string $externalId
     * @param array $settings
     *
     * @return ReportRequest
     * @throws \Exception
     */
    public function createReportRequest($code, $project, $externalId, array $settings = [])
    {
        $report = new ReportRequest();

        $settingsModel = $this->settingsModelFactory->create($code, $settings);

        $report->setCode($code);
        $report->setProject($project);
        $report->setExternalId($externalId);
        $report->setSettings($settingsModel);

        $errors = $this->validator->validate($report);

        if ($errors->count() !== 0) {
            throw new ValidationException($errors);
        }

        $this->manager->persist($report);
        $this->manager->flush();

        return $report;
    }
}