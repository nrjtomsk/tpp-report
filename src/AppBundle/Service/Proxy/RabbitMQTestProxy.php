<?php

namespace AppBundle\Service\Proxy;

use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;

class RabbitMQTestProxy implements RabbitMQProxyInterface
{
    /**
     * As we don't want rabbit mq when we run tests
     * we need this, as rabbit mq publisher service in
     * test env.
     *
     * Publish a message
     *
     * @param string $msgBody
     * @param string $routingKey
     * @param array $additionalProperties
     */
    public function publish($msgBody, $routingKey = '', $additionalProperties = [])
    { }

    /**
     * @param ProducerInterface $producer
     */
    public function setProducer(ProducerInterface $producer)
    { }
}