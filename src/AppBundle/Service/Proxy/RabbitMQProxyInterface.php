<?php

namespace AppBundle\Service\Proxy;

use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;

interface RabbitMQProxyInterface extends ProducerInterface
{
    /**
     * Publish a message
     *
     * @param string $msgBody
     * @param string $routingKey
     * @param array $additionalProperties
     */
    public function publish($msgBody, $routingKey = '', $additionalProperties = []);
}