<?php

namespace AppBundle\Service\Proxy;

use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;

class RabbitMQProxy implements RabbitMQProxyInterface
{
    /**
     * @var ProducerInterface
     */
    private $producer;

    /**
     * @param ProducerInterface $producer
     */
    public function setProducer(ProducerInterface $producer)
    {
        $this->producer = $producer;
    }

    /**
     * Publish a message
     *
     * @param string $msgBody
     * @param string $routingKey
     * @param array $additionalProperties
     */
    public function publish($msgBody, $routingKey = '', $additionalProperties = [])
    {
        $this->producer->publish($msgBody, $routingKey, $additionalProperties);
    }
}