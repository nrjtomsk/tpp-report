<?php

namespace AppBundle\Service;

use AppBundle\Entity\ReportRequest;
use AppBundle\DTO;
use AppBundle\Entity\ReportResult;
use Symfony\Component\Routing\RouterInterface;

class ReportResultMapper
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * ReportResultMapper constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param ReportRequest $reportRequest
     *
     * @return DTO\ReportResult
     */
    public function map(ReportRequest $reportRequest)
    {
        $filesDTO = $this->prepareFiles($reportRequest);
        $reportResultDTO = new DTO\ReportResult($reportRequest, $filesDTO);

        return $reportResultDTO;
    }

    /**
     * @param ReportRequest $reportRequest
     * @return DTO\ReportResultFiles
     */
    private function prepareFiles(ReportRequest $reportRequest)
    {
        $files = [];
        /** @var ReportResult $result */
        foreach ($reportRequest->getResults() as $result) {
            $element = [];

            $element['file'] = $this->router->generate('api_get_report_file', [
                'report' => $reportRequest->getId(),
                'file' => $result->getId()
            ]);

            $element['title'] = $result->getTitle();

            if (in_array($result->getReportRequest()->getCode(), ReportResultMapper::getReportCodesWithFullnessCheck())) {
                $element['is_incomplete'] = $result->isIncomplete();
            }

            $files[$result->getType()][] = $element;
        }

        return new DTO\ReportResultFiles($files);
    }

    /**
     * Get the report types with a fullness check required.
     *
     * @return array
     */
    public static function getReportCodesWithFullnessCheck()
    {
        return [
            ReportRequest::CODE_PESC_CORPORATE_USAGE,
            ReportRequest::CODE_PESC_CORPORATE_XML,
            ReportRequest::CODE_RKS_ENERGO_CORPORATE_USAGE,
        ];
    }
}