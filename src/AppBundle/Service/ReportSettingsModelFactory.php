<?php

namespace AppBundle\Service;

use AppBundle\Entity\ReportRequest;
use AppBundle\Exceptions\NoSuchTypeException;
use AppBundle\Exceptions\ValidationException;
use AppBundle\Model;
use AppBundle\Model\ReportSettingsInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ReportSettingsModelFactory
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * ReportSettingsModelFactory constructor.
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function create($type, array $settings)
    {
        $classes = [
            ReportRequest::CODE_DEVICE_USAGE => Model\DeviceUsage::class,
            ReportRequest::CODE_GROUP_USAGE => Model\GroupUsage::class,
            ReportRequest::CODE_TRANSFORM_STATION_BALANCE => Model\TransformStationBalance::class,
            ReportRequest::CODE_GROUP_USAGE_COSTS => Model\GroupUsageCosts::class,
            ReportRequest::CODE_GROUP_CORPORATE_USAGE => Model\GroupCorporateUsage::class,
            ReportRequest::CODE_PESC_CORPORATE_USAGE => Model\PescCorporateUsage::class,
            ReportRequest::CODE_PESC_CORPORATE_XML => Model\PescCorporateXml::class,
            ReportRequest::CODE_RKS_ENERGO_CORPORATE_USAGE => Model\RksEnergoCorporateUsage::class
        ];

        if (!array_key_exists($type, $classes)) {
            throw new NoSuchTypeException($type);
        }

        /** @var ReportSettingsInterface $object */
        $object = new $classes[$type]($settings);
        $errors = $this->validator->validate($object);

        if ($errors->count() !== 0) {
            throw new ValidationException($errors);
        }

        return $object;
    }
}