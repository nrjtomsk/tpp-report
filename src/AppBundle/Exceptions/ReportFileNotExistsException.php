<?php

namespace AppBundle\Exceptions;

use AppBundle\Entity\ReportFile;
use AppBundle\Entity\ReportRequest;

class ReportFileNotExistsException extends AppException
{

    /**
     * ReportFileNotExistsException constructor.
     *
     * @param ReportFile    $file
     * @param ReportRequest $reportRequest
     */
    public function __construct(ReportFile $file, ReportRequest $reportRequest)
    {
        $message = sprintf(
            'Report file %s for report %s not exists',
            $file->getFilePath(),
            $reportRequest->getId()
        );

        parent::__construct($message);
    }
}