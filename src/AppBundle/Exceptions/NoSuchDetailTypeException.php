<?php

namespace AppBundle\Exceptions;

use AppBundle\Entity\ReportRequest;

class NoSuchDetailTypeException extends AppException
{

    /**
     * NoSuchDetailTypeException constructor.
     *
     * @param string        $type
     * @param ReportRequest $reportRequest
     */
    public function __construct(string $type, ReportRequest $reportRequest)
    {
        $message = sprintf(
            'No such detail type %s for report request %d',
            $type,
            $reportRequest->getId()
        );

        parent::__construct($message);
    }
}