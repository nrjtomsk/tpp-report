<?php

namespace AppBundle\Exceptions;

class ErrorOnSaveDirectoryCreationException extends AppException
{

    /**
     * ErrorOnSaveDirectoryCreationException constructor.
     *
     * @param \Exception $exception
     */
    public function __construct(\Exception $exception)
    {
        $message = sprintf(
            'Exception on creating save directory for report request: %s',
            $exception->getMessage()
        );

        parent::__construct($message);
    }
}