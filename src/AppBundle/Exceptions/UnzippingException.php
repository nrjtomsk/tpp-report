<?php

namespace AppBundle\Exceptions;

use AppBundle\Entity\ReportRequest;

class UnzippingException extends AppException
{

    /**
     * UnzipingException constructor.
     *
     * @param \Exception    $exception
     * @param ReportRequest $reportRequest
     */
    public function __construct(\Exception $exception, ReportRequest $reportRequest)
    {
        $message = sprintf(
            'While unziping data file for report %d, we catch exception %s',
            $reportRequest->getId(),
            $exception->getMessage()
        );

        parent::__construct($message);
    }
}