<?php

namespace AppBundle\Exceptions;

class NoSuchFileOrDirException extends AppException
{

    /**
     * NoSuchFileOrDirException constructor.
     *
     * @param string $fileOrDirPath
     */
    public function __construct(string $fileOrDirPath)
    {
        $message = sprintf(
            'We try to get not existed file or directory: %s',
            $fileOrDirPath
        );

        parent::__construct($message);
    }
}