<?php

namespace AppBundle\Exceptions;

class JsonParsingException extends ApiException
{
    /**
     * JsonParsingException constructor.
     *
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct(
            'Wrong json string was send. Error: ' . $message,
            'bad_json',
            400
        );
    }
}