<?php

namespace AppBundle\Exceptions;

use AppBundle\Entity\ReportRequest;

class ReportNotReadyException extends AppException
{
    /**
     * ReportNotReadyException constructor.
     *
     * @param ReportRequest $reportRequest
     */
    public function __construct(ReportRequest $reportRequest)
    {
        $message = sprintf(
            'Report %d not ready for generation. Report status: %s',
            $reportRequest->getId(),
            $reportRequest->getStatus()
        );

        parent::__construct($message);
    }
}