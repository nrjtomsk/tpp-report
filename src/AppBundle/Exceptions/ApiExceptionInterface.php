<?php

namespace AppBundle\Exceptions;

interface ApiExceptionInterface
{
    /**
     * Get short error message
     *
     * @return string
     */
    public function getShortErrorMessage(): string;

    /**
     * Get error code of exception
     *
     * @return int
     */
    public function getErrorCode(): int;

    /**
     * Get error message
     *
     * @return string
     */
    public function getMessage();
}