<?php

namespace AppBundle\Exceptions;

class NoReportsException extends ApiException
{
    /**
     * NoReportsException constructor.
     */
    public function __construct()
    {
        parent::__construct(
            'We don\'t have reports in request',
            'no_reports_in_request',
            400
        );
    }
}