<?php

namespace AppBundle\Exceptions;

class AppException extends \Exception
{
    /**
     * AppException constructor.
     *
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}