<?php

namespace AppBundle\Exceptions;

class NoSuchTypeException extends ApiException
{

    /**
     * NoSuchTypeException constructor.
     *
     * @param string $type
     */
    public function __construct(string $type)
    {
        parent::__construct(
            'We don\'t have class for such type: ' . $type,
            'no_such_type'
        );
    }
}