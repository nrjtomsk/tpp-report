<?php

namespace AppBundle\Exceptions;

class ApiException extends \Exception implements ApiExceptionInterface
{
    /**
     * @var string
     */
    protected $shortErrorMessage;

    /**
     * @var int
     */
    protected $errorCode;

    /**
     * AppException constructor.
     *
     * @param string $message
     * @param string $shortErrorMessage
     * @param int    $errorCode
     */
    public function __construct(string $message, string $shortErrorMessage, int $errorCode = 400)
    {
        parent::__construct($message);
        $this->shortErrorMessage = $shortErrorMessage;
        $this->errorCode = $errorCode;
    }

    /**
     * Get short error message
     *
     * @return string
     */
    public function getShortErrorMessage(): string
    {
        return $this->shortErrorMessage;
    }

    /**
     * Get error code of exception
     *
     * @return int
     */
    public function getErrorCode(): int
    {
        return $this->errorCode;
    }
}