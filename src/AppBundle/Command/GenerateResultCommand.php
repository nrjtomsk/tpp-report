<?php

namespace AppBundle\Command;

use AppBundle\Entity\ReportRequest;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateResultCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:reports:generate')
            ->setDescription('Generating reports');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $reportGenerator = $this->getContainer()->get('app.report_generator');

        $processedReports = 0;

        $readyReports = $em->getRepository(ReportRequest::class)->findBy([
            'status' => ReportRequest::STATUS_HAS_DATA,
        ]);

        try {
            foreach ($readyReports as $report) {
                $reportGenerator->generate($report);
                $processedReports++;
            }

            $message = sprintf('Processed report requests: %d', $processedReports);
        } catch (\Exception $e) {
            $message = sprintf('Error occurred: %s', $e->getMessage() . $e->getTraceAsString());
        }

        $output->writeln($message);
    }
}
