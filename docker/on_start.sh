#!/bin/bash
php /app/bin/console cache:clear -e prod
php /app/bin/console cache:warmup -e prod

php /app/bin/console doctrine:database:create -e prod
php /app/bin/console doctrine:schema:update --force -e prod

mkdir -p /app/var/logs /app/var/cache /app/var/data /app/var/results
chown www-data:www-data -R /app/var/logs /app/var/cache /app/var/data /app/var/results

/usr/bin/supervisord
