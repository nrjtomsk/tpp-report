[![Circle CI](https://circleci.com/gh/EvercodeLab/tpp-reports.svg?style=svg&circle-token=9aa5fc5c922e5eb7dba9e56ee53e12bfad1f6b73)](https://circleci.com/gh/EvercodeLab/tpp-reports)

# Сервис генерации отчётов

## Запуск через docker

1) Установить docker.

> На MacOS X можно ставить docker toolbox —
https://www.docker.com/products/docker-toolbox. Он сразу установит все необходимые компоненты.

2) После запуска docker-machine выполнить

```bash
$ docker-machine create --driver virtualbox default
$ docker-machine env default
```

3) Выбрать окружение для работы

`docker-compose.yml` сконфигурирован для работы в `prod` режиме. Для работы в `dev` режиме нужно скопировать `docker-compose.override.yml.dist` в `docker-compose.override.yml`. docker-compose распознает наличие этого файла сам.

В `dev` в контейнер мапится вся локальная директория с приложением на всю директорию в контейнере `- .:/app`. Так мы можем редактировать код локально и он автоматом будет обновляться в контейнере.

В `prod` окружении «мапится» только папка `var` `- ./var:/app/var`.

4) Запустить проект

`docker-compose up`

5) Настроить возможность делать запросы к серверу

```bash
$ docker-machine ip default
192.168.99.100
```

Теперь мы можем подключаться по адресу `192.168.99.100:8010`

6) Получить доступ к консоли контейнера

```bash
$ docker exec -it tpp-reports-php-fpm /bin/bash
root@cbe38e9ea569:/var/www/tpp-reports#
```

## Тесты

```
bin/phpunit
```
