**Технические требования на реализацию сервиса генерации отчетов**

Содержание:

[TOC]

# Введение

Документ содержит информацию, необходимую для реализации сервиса по построению отчетов.

В документе перечислены:

- Основные сущности системы, таблицы с набором данных, их типы и описания.

- Примеры API.

- Описания и примеры отчетов.

  ​

# Взаимодействие

Предусмотреть два варианта передачи исходных данных:

1. Запрос данных с из системы с помощью вызова API.
2. Передача системой заранее сгенерированных данных, в архиве.

На данный момент не решен вопрос безопасного доступа к определенным данным. Поэтому используем вариант 2. Сгенерированные файлы выглядят как заранее сделанные запросы к API, например:

- api/v1/projects.json
- api/v1/reports.json
- api/v1/projects/{project_id}/devices.json
- api/v1/projects/{project_id}/devices/{device_id}/capabilities/electricity_metering/data/usage_data.json


Предусмотреть быструю инсталляцию системы в виде контейнера, оформить Dockerfile с набором команд для установки, настройки и запуска. Доп. консультации со стороны заказчика.



Разработать API для взаимодействия с системой:

- Прием данных (архива с файлами) со стороны клиента.
- Контроль статуса задач и % выполнения.
- Передача готового результата.


# Основные сущности

*Ниже описаны основные сущности. Каждая из них может иметь одну или несколько вложенных сущностей.*

## Список сущностей

| Название                    | Описание                                 |
| --------------------------- | ---------------------------------------- |
| Проект                      | Одна из базовых сущностей в системе. Существует несколько типов проекта. В этом ТТ используются два типа: Садоводства и Юридические лица. |
| Устройство                  | Одна из базовых сущностей в системе. Существует большое количество устройств различных типов. Каждый тип устройства имеет общие поля для всех устройств, и свои собственные. |
| Трансформаторная подстанция | Трансформаторная подстанция используется для выделения определенного Устройства, для использования в специальных отчетах. Используется только в типе проекта "Садоводства". |
| Группа                      | Каждое устройство может иметь от 0 до N групп. Группы используются для построения отчетов. |
| Отчет                       | Разные типы проектов имеют разный набор отчетов. |

## Проекты

Путь и название файла имеет формат:

```
projects.json
```

В файле с находится список проектов.

### Пример файла

``` json
{
  "projects": [
    {
      "id": "564754028acb5049691838c7",
      "title": "Государственный Эрмитаж (Дворцовая пл., д.6-8 лит.А)",
      "type": 3,
      "type_description": "индивидуальные устройства – юридические лица",
      "settings": {
        "timezone": 3,
        "description": "",
        "price_category": 2,
        "contact": {
          "person": "Иванов С.Н.",
          "phone": "+7 911-123-45-67",
          "email": ""
        },
        "company": {
          "name": "Федеральное государственное бюджетное учреждение культуры \"Государственный Эрмитаж\"",
          "address": "Санкт-Петербург, Центральный р-н, Дворцовая пл., д.6-8 лит.А",
          "inn": "",
          "ogrn": "",
          "kpp": "",
          "account": "",
          "bik": "",
          "bank_account": "",
          "head": "",
          "head_title": ""
        },
        "electricity_provider": {
          "id": "564754028acb5049691838c7",
          "contract_number": "AБВ-123-ЖЗИ-789",
          "contract_date": "2014-07-25T12:00:00+03:00"
        }
      }
    }
  ]
}
```

### Основные поля

| Название | Тип    | Описание                                 |
| -------- | ------ | ---------------------------------------- |
| id       | string | Уникальный идентификатор проекта         |
| title    | string | Название проекта                         |
| type     | int    | Тип проекта: 2 – Садоводства и многоквартирные дома; 3 – Юридические лица |
| settings | object | Настройки проекта. Для каждого типа проекта применяется свой набор настроек. |

### Проект "Садоводства и многоквартирные дома"

#### Настройки проекта

| Название       | Тип    | Описание                 |
| -------------- | ------ | ------------------------ |
| timezone       | int    | Часовой пояс, -11 ... 12 |
| description    | string | Описание проекта         |
| contact_person | string | Контактное лицо          |
| contact_phone  | string | Телефон                  |
| contact_email  | string | Электронная почта        |

### Проект "Юридические лица"

#### Настройки проекта

*Основные поля аналогично проекту типа "Садоводства и многоквартирные дома"*

| Название             | Тип    | Описание                   |
| -------------------- | ------ | -------------------------- |
| timezone             | int    | Часовой пояс, -11 ... 12   |
| description          | string | Описание проекта           |
| contact_person       | string | Контактное лицо            |
| contact_phone        | string | Телефон                    |
| contact_email        | string | Электронная почта          |
| price_category       | int    | Ценовая категория, 1 ... 6 |
| electricity_provider | object | Сбытовая компания          |
| company              | object | Информация о компании (ЮЛ) |

##### Сбытовая компания

| Название        | Тип    | Описание                                 |
| --------------- | ------ | ---------------------------------------- |
| id              | string | Идентификатор сбытовой компании, например 564754028acb5049691838c7 |
| contract_number | string | Номер договора со сбытовой компанией, например AБВ-123-ЖЗИ-789 |
| contract_date   | string | Дата договора, пример 2014-07-25T12:00:00+03:00 |

##### Информация о компании

| Название     | Тип    | Описание             |
| ------------ | ------ | -------------------- |
| name         | string | Название организации |
| address      | string | Адрес организации    |
| inn          | string | ИНН                  |
| ogrn         | string | ОГРН                 |
| kpp          | string | КПП                  |
| account      | string | Номер счета          |
| bik          | string | БИК                  |
| bank_account | string | Номер корр. счета    |
| head         | string | Руководитель         |
| head_title   | string | Должность            |

#####  

## Устройства

Путь и название файла имеет формат:

```
v1/projects/{project_id}/devices.json
```

В файле с находится список устройств.

### Пример файла

``` json
{
  "devices": [
    {
      "id": "564754028acb5049691838c7",
      "project_id": "564754028acb5049691838c8",
      "base_types": [
        "device",
        "network_device",
        "el_meter",
        "transformer_el_meter"
      ],
      "data": {
        "serial_number": "24101886",
        "mac_address": "418A3604006F0D00",
        "tariffs_count": 3,
        "address": "Участок №014",
        "electricity_metering_dataset": [
          "A+",
          "A-",
          "R+",
          "R-"
        ],
        "current_transformation_ratio": "0.8",
        "voltage_transformation_ratio": "0.8",
        "percent_of_line_loss_ap": "0.45",
        "percent_of_line_loss_am": "0.45",
        "percent_of_line_loss_rp": "0.45",
        "percent_of_line_loss_rm": "0.45"

      }
    }
  ]
}
```

### Типы устройств

Существует множество различных типов устройств. Каждое устройство имеет свои дополнительные поля. Конечная модель устройства может наследовать поля одного или нескольких базовых моделей. Например устройство "Прибор учета" конкретной модели наследует устройство "Базовый прибор учета".

| Название                                 | Тип устройства       | Комментарии                              |
| ---------------------------------------- | -------------------- | :--------------------------------------- |
| Базовое устройство                       | device               | Имеет базовые поля                       |
| Базовое сетевое устройство               | network_device       | Имеет поле MAC адрес                     |
| Базовый интернет шлюз                    | internet_gateway     | Имеет поле IMEI                          |
| Базовый прибор учета                     | el_meter             | Имеет базовые поля для прибора учета     |
| Прибор учета с трансформаторным включением | transformer_el_meter | Имеет дополнительные поля коэффициентов трансформации |

### Основные поля

| Название   | Тип    | Описание                                 |
| ---------- | ------ | ---------------------------------------- |
| id         | string | Уникальный идентификатор проекта         |
| base_types | list   | Набор наследуемых устройств              |
| project_id | string | Идентификатор проекта                    |
| data       | object | Дополнительные поля устройства, а зависимости от набора наследуемых устройств |

Различные типы устройств имеют различный набор настроек.

### Базовое устройство

#### Дополнительные поля

| Название        | Тип    | Описание        |
| --------------- | ------ | --------------- |
| address         | string | Адрес установки |
| contract_number | string | Номер договора  |

### Базовый прибор учета

#### Дополнительные поля

| Название                     | Тип    | Описание                                 |
| ---------------------------- | ------ | ---------------------------------------- |
| serial_number                | string | Серийный номер                           |
| tariffs_count                | int    | Количество тарифов                       |
| electricity_metering_dataset | list   | Типы данных возможности учета электроэнергии |

### Базовый прибор учета с трансформаторным включением

#### Дополнительные поля

| Название                     | Тип    | Описание                             |
| ---------------------------- | ------ | ------------------------------------ |
| current_transformation_ratio | string | Коэффициент трансформации тока       |
| voltage_transformation_ratio | string | Коэффициент трансформации напряжения |
| percent_of_line_loss_ap      | string | Процент потерь в линии A+            |
| percent_of_line_loss_am      | string | Процент потерь в линии A-            |
| percent_of_line_loss_rp      | string | Процент потерь в линии R+            |
| percent_of_line_loss_rm      | string | Процент потерь в линии R-            |

## Данные

Путь и название файла имеет формат:

```
v1/projects/{project_id}/devices/{device_id}/capabilities/electricity_metering/data/daily_data.json
```

Где electricity_metering это название возможности, а daily_data название файла с конкретным типом данных.

### Типы данных

Файлы с данными имеют разный формат.

| Возможность         | Код                  | Данные                                   | Код            |
| ------------------- | -------------------- | ---------------------------------------- | :------------- |
| Учет электроэнергии | electricity_metering | Показания электроэнергии – суточные данные нарастающим итогом | daily_data     |
| Учет электроэнергии | electricity_metering | Потребление электроэнергии – получасовые данные (профиль нагрузки) | half_hour_data |

### Показания электроэнергии

Показания электроэнергии предоставляются прибором учета раз в сутки, с 00:00 по 01:00 следующего дня.

#### Пример файла

``` json
{
  "items": [
    {
      "date": "2015-07-12T00:00:00+03:00",
      "data": [
        {
          "type": "A+",
          "values": [
            400,
            250.48,
            149.52
          ]
        },
        {
          "type": "R+",
          "values": [
            10.02,
            null,
            10.02
          ]
        },
        {
          "type": "R-",
          "values": [
            1,
            null,
            1
          ]
        }
      ]
    }
  ]
}
```

#### Описание

| Название | Тип                 | Описание                                 |
| -------- | ------------------- | ---------------------------------------- |
| date     | string              | Дата                                     |
| A+       | list of string      | Показания активной положительной электроэнергии |
| A-       | list of string      | Показания активной отрицательной электроэнергии |
| R+       | list of string      | Показания реактивной положительной электроэнергии |
| R-       | list of string      | Показания реактивной отрицательной электроэнергии |
| values   | list of float\|null | Показания [T0 (сумма показаний), T1, T2, T3, T4] |

Количество показаний внутри списка может быть разное, и зависит от текущего количества тарифов в устройстве, а так же от наличия данных в системе. Количество тарифов смотрим в описании устройства. Максимальное количество тарифов – 5 (T0…T4).

### Потребление электроэнергии

Данные о потреблении электроэнергии предоставляются прибором учета раз в 30 минут.

#### Пример файла

``` json
{
  "items": [
    {
      "date": "2015-07-12T00:00:00+03:00",
      "data": [
        {
          "type": "A+",
          "values": [
            1,
            2.01,
            1.17,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            5.08,
            3.7,
            3.2,
            3.2,
            0,
            0,
            0,
            0,
            0,
            0,
            0
          ]
        }
      ]
    }
  ]
}
```

#### Описание

| Название | Тип                 | Описание                                 |
| -------- | ------------------- | ---------------------------------------- |
| date     | string              | Дата                                     |
| A+       | string              | Показания активной положительной электроэнергии |
| A-       | string              | Показания активной отрицательной электроэнергии |
| R+       | string              | Показания реактивной положительной электроэнергии |
| R-       | string              | Показания реактивной отрицательной электроэнергии |
| values   | list of float\|null | 0-48 получасовых показаний: [00:00, 00:30, 01:00 … 23:30] |

Набор данных зависит от возможности устройства.

Количество показаний определенного типа данных не может быть более 48. Точное количество зависит от наличия данных в системе.

###  

## Отчеты

### Форматы отчетов

| Формат     | Комментарий                              |
| ---------- | :--------------------------------------- |
| Excel 2007 | Предыдущие версии имеют ограничения на количество столбцов |
| JSON       | Для отображения на web странице          |
| ZIP        | Если в результате выполнения отчета создано более 1 файла (например отчет сбытовой компании) необходимо предусмотреть загрузку в ZIP. |
| XML        | Только для отчётов XML 80020              |

В схеме взаимодействия, при которой в систему построения отчетов передаются заранее сгенерированные файлы, reports.json это стартовый файл. По содержимому этого файла система определяет, какие отчеты необходимы.

В файле reports.json находится список отчетов и их настроек.

### Пример файла

``` json
{
  "reports": [
    {
      "id": "564754028acb5049691838c7",
      "project_id": "564754028acb5049691838c7",
      "report_code": "group_usage_v2",
      "settings": {
        "title": "Новый отчет от 09.12.2015 15:20:08",
        "view_type": 1,
        "detail_type": 1,
        "groups": [
          "564754028acb5049691838c1",
          "564754028acb5049691838c2",
          "564754028acb5049691838c3"
        ],
        "date_start": "2015-12-09T00:00:00+03:00",
        "date_end": "2015-12-09T23:59:59+03:00"
      }
    }
  ]
}
```

### Типы отчетов

| Название отчета         | Код отчета                     | Тип проекта      |
| ----------------------- | ------------------------------ | :--------------- |
| Потребление ПУ          | device_usage_v1                | Садоводства      |
| Потребление групп ПУ    | group_usage_v2                 | Садоводства      |
| Баланс подстанции       | transformer_station_balance_v1 | Садоводства      |
| Ведомость потребления   | group_usage_costs_v1           | Садоводства      |
| Потребление ПУ          | group_corp_usage_v2            | Юридические лица |
| Отчет сбытовая компания | pesc_corporate_usage_v2        | Юридические лица |
| Отчет XML 80020         | pesc_corporate_xml_80020_v1    | Юридические лица |
| Отчет РКС-Энерго        | rks_energo_corporate_usage_v1  | Юридические лица |

### Основные поля

| Название    | Тип    | Описание                                 |
| ----------- | ------ | ---------------------------------------- |
| id          | string | Уникальный идентификатор отчета          |
| project_id  | string | Идентификатор родительского проекта      |
| report_code | string | Код отчета                               |
| settings    | object | Настройки отчета (в зависимости от report_code) |


### Потребление ПУ

Код отчета: device_usage_v1

#### Настройки отчета

| Название    | Тип    | Описание                                 |
| ----------- | ------ | ---------------------------------------- |
| title       | string | Название отчета                          |
| view_type   | int    | Тип данных: 1. По потреблению; 2. По показанию |
| detail_type | int    | Детализация: 1. По 30 мин. 3. По дням. 5. По месяцам. |
| device_id   | string | Идентификатор устройств                  |
| date_start  | string | Дата начала                              |
| date_end    | string | Дата окончания                           |

#### Ожидаемый результат

![](./reports/device_usage_v1/image.png)

[Пример Excel](./reports/device_usage_v1/novyi-otchet-ot-01-08-2015-11-44-16.xls)

[Пример HTML](./reports/device_usage_v1/report.html)



### Потребление групп ПУ

Код отчета: group_usage_v2

#### Настройки отчета

| Название    | Тип    | Описание                                 |
| ----------- | ------ | ---------------------------------------- |
| title       | string | Название отчета                          |
| view_type   | int    | Тип данных: 1. По потреблению; 2. По показанию |
| detail_type | int    | Детализация: 1. По 30 мин. 3. По дням. 5. По месяцам. |
| groups      | list   | Список идентификаторов групп             |
| date_start  | string | Дата начала                              |
| date_end    | string | Дата окончания                           |

#### Ожидаемый результат

**Пример отчета: Тип данных: по потреблению; Детализация: по дням; Группы: ТП-878; Дата с 01.12.2015 по 08.12.2015**

![](./reports/group_usage_v2/by_day/image.png)

[Пример HTML](./reports/group_usage_v2/by_day/report.html)

[Пример Excel](./reports/group_usage_v2/by_day/novyi-otchet-ot-08-12-2015-18-07-20.xls)



**Пример отчета: Тип данных: по потреблению; Детализация: по 30 минутам; Группы: не выбраны; Дата с 09.12.2015 по 09.12.2015**

![](./reports/group_usage_v2/by_30min/image.png)

[Пример HTML](./reports/group_usage_v2/by_30min/report.html)

[Пример Excel](./reports/group_usage_v2/by_30min/novyi-otchet-ot-09-12-2015-15-20-08.xls)



### Баланс подстанции

Код отчета: transformer_station_balance_v1

#### Настройки отчета

| Название               | Тип    | Описание                                 |
| ---------------------- | ------ | ---------------------------------------- |
| title                  | string | Название отчета                          |
| detail_type            | int    | Детализация: 1. По 30 мин. 3. По дням.   |
| transformer_station_id | string | Идентификатор трансформаторной подстанции |
| date_start             | string | Дата начала                              |
| date_end               | string | Дата окончания                           |

#### Ожидаемый результат

![](./reports/transformer_station_balance_v1/image.png)

[Пример HTML](./reports/transformer_station_balance_v1/report.html)

[Пример Excel](./reports/transformer_station_balance_v1/novyi-otchet-ot-10-12-2015-12-07-02.xls)



### Ведомость потребления

Код отчета: group_usage_costs_v1

#### Настройки отчета

| Название   | Тип    | Описание               |
| ---------- | ------ | ---------------------- |
| title      | string | Название отчета        |
| date_start | string | Дата начала            |
| date_end   | string | Дата окончания         |
| tp_1_t1    | string | 1 тарифный ПУ, Тариф 1 |
| tp_2_t1    | string | 2 тарифный ПУ, Тариф 1 |
| tp_2_t2    | string | 2 тарифный ПУ, Тариф 2 |
| tp_3_t1    | string | 3 тарифный ПУ, Тариф 1 |
| tp_3_t2    | string | 3 тарифный ПУ, Тариф 2 |
| tp_3_t3    | string | 3 тарифный ПУ, Тариф 3 |

#### Ожидаемый результат

![](./reports/group_usage_costs_v1/image.png)

[Пример HTML файла.](./reports/group_usage_costs_v1/report.html)

[Пример Excel файла.](./reports/group_usage_costs_v1/novyi-otchet-ot-10-12-2015-14-32-26.xls)

Показания должны выводиться с точностью до целых. Точность суммы к оплате должна быть до 2 знаков после запятой.



### Потребление групп ПУ (ЮЛ)

Код отчета: group_corp_usage_v2

#### Изменения

*Дата*: 24 фев 2016
*Тикет*: SUPPORT-2028
*Описание:*

Текущие столбцы:
Название организации IMEI    Серийный номер
Нужные столбцы:
Название устройства Адрес установки Серийный номер

![image](./reports/changes/SUPPORT-2028/image1.png)

![image](./reports/changes/SUPPORT-2028/image2.png)

#### Настройки отчета

| Название    | Тип    | Описание                                 |
| ----------- | ------ | ---------------------------------------- |
| title       | string | Название отчета                          |
| view_type   | int    | Тип данных: 1. По потреблению; 2. По показанию |
| detail_type | int    | Детализация: 1. По 30 мин. 3. По дням. 5. По месяцам. |
| date_start  | string | Дата начала                              |
| date_end    | string | Дата окончания                           |

#### Ожидаемый результат

**Пример отчета: Тип данных: по потреблению; Детализация: по 30 минутам; Дата с 19.06.2015 по 26.06.2015**

![](./reports/group_corp_usage_v2/image.png)

[Пример HTML файла.](./reports/group_corp_usage_v2/report.html)

[Пример Excel файла.](./reports/group_corp_usage_v2/novyi-otchet-ot-26-06-2015-12-05-22.xls)



### Сбытовая компания

Код отчета: pesc_corporate_usage_v2

#### Изменения

*Дата*: 15 фев 2016
*Тикет*: SUPPORT-2018
*Описание:*

в строке группа точек должно быть:
Группа точек: 1700030176 ПАО МТС СН (то, что прописывается в названии)
в строке прибор учёта:
Прибор учета: 24389349 (номер счётчика)

![image](./reports/changes/SUPPORT-2018/image.png)

[]()

[Пример отчета](./reports/changes/SUPPORT-2018/mezhdugorodnoi-i-mezhdunaro-007740027-grshch1-vvod-1-21767475-aktivnaia-energiia-priem.xls)



#### Настройки отчета

| Название | Тип    | Описание                                |
| -------- | ------ | --------------------------------------- |
| title    | string | Название отчета                         |
| date     | string | Дата (год, месяц) для построения отчета |

#### Ожидаемый результат

Результатом будет список Excel файлов:

- Общий файл с отчетом по ЮЛ
- Файлы с часовыми данными по каждому ПУ, отдельно по каждому виду энергии (A+, R+, R-, согласно возможностям ПУ)
- Файлы с суммой часовых данных по всем ПУ, отдельно по каждому виду энергии (A+, R+, R-, согласно возможностям ПУ)

Так же необходим отдельный ZIP архив с этими файлами.

**Примеры файлов**

[Междугородной и междунаро... отчет](./reports/pesc_corporate_usage_v2/mezhdugorodnoi-i-mezhdunaro-otchet.xls)

![](./reports/pesc_corporate_usage_v2/mezhdugorodnoi-i-mezhdunaro-otchet.png)

[ТП 635, ввод 2 21755339 Активная энергия прием](./reports/pesc_corporate_usage_v2/mezhdugorodnoi-i-mezhdunaro-tp-635-vvod-2-21755339-aktivnaia-energiia-priem.xls)

![](./reports/pesc_corporate_usage_v2/mezhdugorodnoi-i-mezhdunaro-tp-635-vvod-2-21755339-aktivnaia-energiia-priem.png)

[ТП 635, ввод 2 21755339 Реактивная энергия прием](./reports/pesc_corporate_usage_v2/mezhdugorodnoi-i-mezhdunaro-tp-635-vvod-2-21755339-reaktivnaia-energiia-priem.xls)

[ТП 635, ввод 2 21755339 Реактивная энергия отдача](./reports/pesc_corporate_usage_v2/mezhdugorodnoi-i-mezhdunaro-tp-635-vvod-2-21755339-reaktivnaia-energiia-otdacha.xls)

ТП 634, ввод 1 21755371 Активная энергия прием

ТП 634, ввод 1 21755371 Реактивная энергия прием

ТП 634, ввод 1 21755371 Реактивная энергия отдача

[Сумма Активная энергия прием](./reports/pesc_corporate_usage_v2/mezhdugorodnoi-i-mezhdunaro-summa-aktivnaia-energiia-priem.xls)

[Сумма Реактивная энергия прием](./reports/pesc_corporate_usage_v2/mezhdugorodnoi-i-mezhdunaro-summa-reaktivnaia-energiia-otdacha.xls)

[Сумма Реактивная энергия отдача](./reports/pesc_corporate_usage_v2/mezhdugorodnoi-i-mezhdunaro-summa-reaktivnaia-energiia-priem.xls)

[ZIP архив](./reports/pesc_corporate_usage_v2/mezhdugorodnoi-i-mezhdunaro.zip)



**Важно**

При запросе готового результата кроме списка файлов необходимо отображать данные о целостности каждого файла. Это информация о том, все ли временные интервалы содержат данные, или в каком то интервале данные отсутствовали.

Пример выдачи:

``` json
{
  "items": [
    {
      "status": "ready",
      "report": {
        "id": "564754028acb5049691838c7",
        "project_id": "564754028acb5049691838c7",
        "report_code": "pesc_corporate_usage_v2",
        "settings": {
          "title": "Новый отчет от 01.12.2015 09:01:12 – Отчет сбытовая компания",
          "date": "2015-12-01T00:00:00+03:00"
        }
      },
      "result": {
        "files": [
          {
            "title": "Сумма Активная энергия прием",
            "filename": "... mezhdugorodnoi-i-mezhdunaro-summa-aktivnaia-energiia-priem.xls",
            "full": true
          },
          {
            "title": "Сумма Реактивная энергия отдача",
            "filename": "... mezhdugorodnoi-i-mezhdunaro-summa-reaktivnaia-energiia-otdacha.xls",
            "full": false
          },
          {
            "title": "ZIP архив",
            "filename": "... mezhdugorodnoi-i-mezhdunaro.zip",
            "full": false
          }
        ]
      }
    }
  ]
}
```


### Отчет XML 80020

Код отчёта: pesc_corporate_xml_80020_v1

#### Настройки отчета

| Название    | Тип    | Описание                                 |
| ----------- | ------ | ---------------------------------------- |
| title       | string | Название отчета                          |
| detail_type | int    | Детализация: 1. По 30 мин. 2. По 60 мин  |
| date        | string | Дата (год, месяц) для построения отчета  |

#### Ожидаемый результат

Результатом будет список XML файлов по одному на каждый день месяца с данными по всем ПУ в рамках одного дня.

Так же необходим отдельный ZIP архив с этими файлами.

**Примеры файлов**

- [80020_7740000076_20160301_1.xml](./reports/pesc_corporate_xml_80020_v1/80020_7740000076_20160301_1.xml)
- [80020_7740000076_20160302_2.xml](./reports/pesc_corporate_xml_80020_v1/80020_7740000076_20160302_2.xml)
- [80020_7740000076_20160303_3.xml](./reports/pesc_corporate_xml_80020_v1/80020_7740000076_20160303_3.xml)
- [80020_7740000076_20160304_4.xml](./reports/pesc_corporate_xml_80020_v1/80020_7740000076_20160304_4.xml)
- [ZIP архив](./reports/pesc_corporate_xml_80020_v1/80020_7740000076_201603_1.zip)

### Отчет РКС-Энерго

Код отчёта: rks_energo_corporate_usage_v1

#### Настройки отчета

| Название    | Тип    | Описание                                 |
| ----------- | ------ | ---------------------------------------- |
| title       | string | Название отчета                          |
| date        | string | Дата (год, месяц) для построения отчета  |


#### Ожидаемый результат

Результатом будет список Excel файлов:

- Файлы с часовыми данными по каждому ПУ по активной энергии

Так же необходим отдельный ZIP архив с этими файлами.

**Примеры файлов**

- [25625503 Активная энергия прием](./reports/rks_energo_corporate_usage_v1/25625503-aktivnaia-energiia-priem.xls)
- [24389386 Активная энергия прием](./reports/rks_energo_corporate_usage_v1/24389386-aktivnaia-energiia-priem.xls) —  неполный отчёт
- [25625535 Активная энергия прием](./reports/rks_energo_corporate_usage_v1/25625535-aktivnaia-energiia-priem.xls)
- [Приложение 4.1 ПАО МТС 890610002 24389350 Активная энергия прием](./reports/rks_energo_corporate_usage_v1/prilozhenie-4-1-pao-mts-890610002-24389350-aktivnaia-energiia-priem.xls)
- [Приложение 4.1 ПАО МТС 890610003 24389361 Активная энергия прием](./reports/rks_energo_corporate_usage_v1/prilozhenie-4-1-pao-mts-890610003-24389361-aktivnaia-energiia-priem.xls)
- [ZIP архив](./reports/rks_energo_corporate_usage_v1/filial-pao-mobilnye-tel.zip))


**Важно**

При запросе готового результата кроме списка файлов необходимо отображать данные о целостности каждого файла. Это информация о том, все ли временные интервалы содержат данные, или в каком то интервале данные отсутствовали.


## Группы

Группы используются для некоторых отчетов.

Путь и название файла имеет формат:

```
v1/projects/{project_id}/groups.json
```

### Пример файла

``` json
{
  "groups": [
    {
      "id": "564754028acb5049691838c7",
      "title": "Название группы",
      "description": "Описание группы"
    }
  ]
}
```

### Основные поля

| Название    | Тип    | Описание                        |
| ----------- | ------ | ------------------------------- |
| id          | string | Уникальный идентификатор группы |
| title       | string | Название группы                 |
| description | string | Описание группы                 |

## Трансформаторные подстанции

Трансформаторные подстанции используются для некоторых отчетов.

Наличие сущности зависит от типа проекта.

Путь и название файла имеет формат:

```
v1/projects/{project_id}/transformer_stations.json
```

### Пример файла

``` json
{
  "transformer_stations": [
    {
      "id": "564754028acb5049691838c7",
      "title": "Название подстанции",
      "description": "Описание подстанции",
      "device_id": "564754028acb5049691838c8",
      "groups": [
          "564754028acb5049691838c1",
          "564754028acb5049691838c2",
          "564754028acb5049691838c3"
      ]
    }
  ]
}
```

### Основные поля

| Название    | Тип    | Описание                            |
| ----------- | ------ | ----------------------------------- |
| id          | string | Уникальный идентификатор подстанции |
| title       | string | Название подстанции                 |
| description | string | Описание подстанции                 |
| device_id   | string | Идентификатор устройства            |
| groups      | list   | Список идентификаторов групп        |

####  
