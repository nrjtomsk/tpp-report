<?php

namespace Tests\Functional\Controller;

use AppBundle\Entity\ReportRequest;
use AppBundle\Entity\ReportResult;
use Symfony\Component\HttpFoundation\Response;
use Tests\ApiTestCase;

class ReportsControllerTest extends ApiTestCase
{
    /**
     * Check all reports creation
     *
     * @dataProvider reportsJson
     * @param $json
     */
    public function testReportCreateOk($json)
    {
        $response = $this->jsonRequestPost(
            '/api/v1/reports',
            $json
        );

        $this->assertReportRequestCreationIsOk($response);
    }

    /**
     * Check if file uploading for request is ok
     */
    public function testFileUploadOk()
    {
        $report = $this->createReportRequest(ReportsJsonList::DEVICE_USAGE_JSON);
        $file = $this->createFile($report);

        $response = $this->requestPost(
            '/api/v1/reports/' . $report->getId() . '/files',
            [],
            ['file' => $file]
        );

        $this->assertStatusCodeIs($response, Response::HTTP_NO_CONTENT);
    }

    /**
     * If we upload file second time we don't rise exception
     */
    public function testDoubleFileUpload()
    {
        $report = $this->createReportRequest(ReportsJsonList::DEVICE_USAGE_JSON);

        $this->requestPost('/api/v1/reports/' . $report->getId() . '/files', [], ['file' => $this->createFile($report)]);
        $response = $this->requestPost('/api/v1/reports/' . $report->getId() . '/files', [], ['file' => $this->createFile($report)]);

        $this->assertStatusCodeIs($response, Response::HTTP_NO_CONTENT);
    }

    /**
     * Check exception when we try to upload file for not existed report request
     */
    public function testFileUploadNoReportException()
    {
        $file = $this->createFile();

        $response = $this->requestPost(
            '/api/v1/reports/100500/files',
            [],
            ['file' => $file]
        );

        $this->assertStatusCodeIs($response, Response::HTTP_NOT_FOUND);
        $this->assertJsonResponse($response);
        /** TODO: Asserting error message */
    }

    public function testFileUploadNoFileException()
    {
        $report = $this->createReportRequest(ReportsJsonList::DEVICE_USAGE_JSON);

        $response = $this->requestPost(
            '/api/v1/reports/' . $report->getId() . '/files',
            [],
            []
        );

        $this->assertStatusCodeIs($response, Response::HTTP_BAD_REQUEST);
        $this->assertJsonResponse($response);
        /** TODO: Asserting error message */
    }

    public function testGetReportRequestWithOutResult()
    {
        $report = $this->createReportRequest(ReportsJsonList::DEVICE_USAGE_JSON);
        $response = $this->requestGet('/api/v1/reports/' . $report->getId());

        $data = json_decode($response->getContent(), true);
        $this->assertStatusCodeIs($response, Response::HTTP_OK);
        $this->assertJsonResponse($response);
        $this->assertEquals(ReportRequest::STATUS_WAITING_DATA, $data['status']);
        $this->assertEquals(4, count($data['result']));
        $this->assertEquals([], $data['result']['xls']);
        $this->assertEquals($report->getId(), $data['id']);
    }

    public function testGetReportRequestWithResult()
    {
        $reportRequest = $this->createReportRequest(ReportsJsonList::DEVICE_USAGE_JSON);
        $this->createReportResults($reportRequest);

        $response = $this->requestGet('/api/v1/reports/' . $reportRequest->getId());

        $data = json_decode($response->getContent(), true);

        $this->assertStatusCodeIs($response, Response::HTTP_OK);
        $this->assertJsonResponse($response);
        $this->assertEquals(ReportRequest::STATUS_DONE, $data['status']);
        $this->assertEquals(4, count($data['result']));
        $this->assertArrayHasKey('xls', $data['result']);
        $this->assertArrayHasKey('json', $data['result']);
        $this->assertEquals($reportRequest->getId(), $data['id']);
    }

    public function testGetReportResultFile()
    {
        $reportRequest = $this->createReportRequest(ReportsJsonList::DEVICE_USAGE_JSON);
        $this->createReportResults($reportRequest);

        $response = $this->requestGet('/api/v1/reports/'.$reportRequest->getId().'/files/1');

        $this->assertStatusCodeIs($response, Response::HTTP_OK);
        $this->assertInstanceOf(
            'Symfony\Component\HttpFoundation\BinaryFileResponse',
            $response
        );
    }

    public function testGetReportResultFileErrorIfNoFile()
    {
        $reportRequest = $this->createReportRequest(ReportsJsonList::DEVICE_USAGE_JSON);
        $response = $this->requestGet('/api/v1/reports/'.$reportRequest->getId().'/files/1');

        $this->assertStatusCodeIs($response, Response::HTTP_NOT_FOUND);
    }

    public function testTryGetReportResultFileForWrongReport()
    {
        $reportRequest = $this->createReportRequest(ReportsJsonList::DEVICE_USAGE_JSON);
        $response = $this->requestGet('/api/v1/reports/'.$reportRequest->getId().'/files/100');

        $this->assertStatusCodeIs($response, Response::HTTP_NOT_FOUND);
    }

    private function createReportResults(ReportRequest $reportRequest)
    {
        $reportRequest->setStatus(ReportRequest::STATUS_DONE);

        $result = new ReportResult();
        $result->setType(ReportResult::TYPE_XLS);
        $result->setFile('test/test.xls');
        $result->setReportRequest($reportRequest);
        $this->em->persist($result);
        $reportRequest->addResult($result);

        $result = new ReportResult();
        $result->setType(ReportResult::TYPE_JSON);
        $result->setFile('test/test.json');
        $result->setReportRequest($reportRequest);
        $this->em->persist($result);
        $reportRequest->addResult($result);

        $this->setCopyTestFilesToResultDirectory();

        $this->em->flush();
    }

    private function setCopyTestFilesToResultDirectory()
    {
        $resultDir = __DIR__.'/../../../var/results/test';
        $dataDirectory = __DIR__.'/../../data';
        $this->getContainer()->get('filesystem')->mkdir($resultDir);

        copy($dataDirectory.'/test.xls', $resultDir.'/test.xls');
        copy($dataDirectory.'/test.json', $resultDir.'/test.json');
    }

    public function reportsJson()
    {
        return [
            [ReportsJsonList::DEVICE_USAGE_JSON],
            [ReportsJsonList::DEVICE_USAGE_30_MIN],
            [ReportsJsonList::DEVICE_USAGE_MONTH],
            [ReportsJsonList::GROUP_CORP_USAGE],
            [ReportsJsonList::GROUP_CORP_USAGE_30_MIN],
            [ReportsJsonList::GROUP_CORP_USAGE_MONTH],
            [ReportsJsonList::GROUP_USAGE],
            [ReportsJsonList::GROUP_USAGE_30_MIN],
            [ReportsJsonList::GROUP_USAGE_MONTH],
            [ReportsJsonList::GROUP_USAGE_COSTS],
            [ReportsJsonList::PESC_CORPORATE_USAGE],
            [ReportsJsonList::TRANSFORMER_STATION_BALANCE],
            [ReportsJsonList::TRANSFORMER_STATION_BALANCE_30_MIN],
            [ReportsJsonList::PESC_CORPORATE_XML_80020],
            [ReportsJsonList::RKS_ENERGO_CORPORATE_USAGE],
        ];
    }
}