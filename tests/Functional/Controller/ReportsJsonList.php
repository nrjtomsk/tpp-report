<?php

namespace Tests\Functional\Controller;

class ReportsJsonList
{
    const DEVICE_USAGE_JSON = '{
      "pagination": {
        "current": 1,
        "total_items": 1,
        "total_pages": 1
      },
      "reports": [
        {
          "report_code": "device_usage_v1",
          "project_id": "567beada8acb504d145caef5",
          "id": "56a453668acb505966be2e26",
          "settings": {
            "date_start": "2015-12-10T00:00:00+03:00",
            "view_type_description": "по потреблению",
            "detail_type": 3,
            "detail_type_description": "по дням",
            "title": "Новый отчет от 24.01.2016 07:30:00",
            "date_end": "2016-01-24T00:00:00+03:00",
            "view_type": 1,
            "device_id": "553e03608acb502e3b077705"
          }
        }
      ]
    }';

    const DEVICE_USAGE_30_MIN = '{
      "pagination": {
        "current": 1,
        "total_items": 1,
        "total_pages": 1
      },
      "reports": [
        {
          "report_code": "device_usage_v1",
          "project_id": "567beada8acb504d145caef5",
          "id": "56a453668acb505966be2e26",
          "settings": {
            "date_start": "2015-12-10T00:00:00+03:00",
            "view_type_description": "по потреблению",
            "detail_type": 1,
            "detail_type_description": "по 30 минут",
            "title": "Новый отчет от 24.01.2016 07:30:00",
            "date_end": "2016-01-24T00:00:00+03:00",
            "view_type": 1,
            "device_id": "553e03608acb502e3b077705"
          }
        }
      ]
    }';

    const DEVICE_USAGE_MONTH = '{
      "pagination": {
        "current": 1,
        "total_items": 1,
        "total_pages": 1
      },
      "reports": [
        {
          "report_code": "device_usage_v1",
          "project_id": "567beada8acb504d145caef5",
          "id": "56a453668acb505966be2e26",
          "settings": {
            "date_start": "2015-12-10T00:00:00+03:00",
            "view_type_description": "по потреблению",
            "detail_type": 5,
            "detail_type_description": "по месяцам",
            "title": "Новый отчет от 24.01.2016 07:30:00",
            "date_end": "2016-01-24T00:00:00+03:00",
            "view_type": 1,
            "device_id": "553e03608acb502e3b077705"
          }
        }
      ]
    }';

    const GROUP_CORP_USAGE = '{
      "reports": [
        {
          "report_code": "group_corp_usage_v2",
          "project_id": "55c497f08acb5010e2025911",
          "id": "56af23f48acb506bcf8eba81",
          "settings": {
            "detail_type_description": "по дням",
            "view_type": 1,
            "date_end": "2016-01-31T00:00:00+03:00",
            "date_start": "2016-01-01T00:00:00+03:00",
            "view_type_description": "по потреблению",
            "detail_type": 3,
            "title": "Новый отчет от 01.02.2016 12:22:37"
          }
        }
      ]
    }';

    const GROUP_CORP_USAGE_MONTH = '{
      "reports": [
        {
          "report_code": "group_corp_usage_v2",
          "project_id": "55c497f08acb5010e2025911",
          "id": "56af23f48acb506bcf8eba81",
          "settings": {
            "detail_type_description": "по месяцам",
            "view_type": 1,
            "date_end": "2016-03-31T00:00:00+03:00",
            "date_start": "2016-01-01T00:00:00+03:00",
            "view_type_description": "по потреблению",
            "detail_type": 5,
            "title": "Новый отчет от 01.02.2016 12:22:37"
          }
        }
      ]
    }';

    const GROUP_CORP_USAGE_30_MIN = '{
      "reports": [
        {
          "report_code": "group_corp_usage_v2",
          "project_id": "55c497f08acb5010e2025911",
          "id": "56af23f48acb506bcf8eba81",
          "settings": {
            "detail_type_description": "по 30 минут",
            "view_type": 1,
            "date_end": "2016-01-03T00:00:00+03:00",
            "date_start": "2016-01-01T00:00:00+03:00",
            "view_type_description": "по потреблению",
            "detail_type": 1,
            "title": "Новый отчет от 01.02.2016 12:22:37"
          }
        }
      ]
    }';

    const GROUP_USAGE_COSTS = '{
      "reports": [
        {
          "report_code": "group_usage_costs_v1",
          "project_id": "55e473c58acb50761c530819",
          "id": "56af2a0f8acb506bcf8eba9a",
          "settings": {
            "tp_3_t1": 0.0,
            "tp_2_t1": 0.0,
            "title": "Новый отчет от 01.02.2016 12:48:51",
            "tp_3_t2": 0.0,
            "tp_3_t3": 0.0,
            "date_end": "2016-02-01T00:00:00+03:00",
            "tp_2_t2": 0.0,
            "tp_1_t1": 0.0,
            "date_start": "2016-01-01T00:00:00+03:00"
          }
        }
      ]
    }';

    const GROUP_USAGE = '{
      "reports": [
        {
          "report_code": "group_usage_v2",
          "project_id": "55c8b2628acb50537626e3da",
          "id": "56ad9e858acb505622f29ea8",
          "settings": {
            "groups": [],
            "date_start": "2015-12-31T00:00:00+03:00",
            "view_type_description": "по показанию",
            "detail_type": 3,
            "detail_type_description": "по дням",
            "title": "Новый отчет от 31.01.2016 08:39:50",
            "date_end": "2016-01-31T00:00:00+03:00",
            "view_type": 2
          }
        }
      ]
    }';

    const GROUP_USAGE_30_MIN = '{
      "reports": [
        {
          "report_code": "group_usage_v2",
          "project_id": "55c8b2628acb50537626e3da",
          "id": "56ad9e858acb505622f29ea8",
          "settings": {
            "groups": [],
            "date_start": "2015-12-31T00:00:00+03:00",
            "view_type_description": "по показанию",
            "detail_type": 1,
            "detail_type_description": "по 30 минут",
            "title": "Новый отчет от 31.01.2016 08:39:50",
            "date_end": "2016-01-01T00:00:00+03:00",
            "view_type": 1
          }
        }
      ]
    }';

    const GROUP_USAGE_MONTH = '{
      "reports": [
        {
          "report_code": "group_usage_v2",
          "project_id": "55c8b2628acb50537626e3da",
          "id": "56ad9e858acb505622f29ea8",
          "settings": {
            "groups": [],
            "date_start": "2015-12-31T00:00:00+03:00",
            "view_type_description": "по показанию",
            "detail_type": 5,
            "detail_type_description": "по месяцам",
            "title": "Новый отчет от 31.01.2016 08:39:50",
            "date_end": "2016-02-01T00:00:00+03:00",
            "view_type": 1
          }
        }
      ]
    }';

    const PESC_CORPORATE_USAGE = '{
      "reports": [
        {
          "report_code": "pesc_corporate_usage_v2",
          "project_id": "559245688acb505fa8302e78",
          "id": "56b06dd28acb500d4b62a70d",
          "settings": {
            "date": "2016-01-01T00:00:00+03:00",
            "title": "Новый отчет от 02.02.2016 11:50:18"
          }
        }
      ]
    }';

    const TRANSFORMER_STATION_BALANCE = '{
      "reports": [
        {
          "report_code": "transformer_station_balance_v1",
          "project_id": "5603a5db8acb504eb839a28f",
          "id": "56a8c18f8acb50063c74c318",
          "settings": {
            "detail_type_description": "по дням",
            "title": "Новый отчет от 27.01.2016 12:07:14",
            "date_end": "2016-01-27T00:00:00+03:00",
            "date_start": "2016-01-20T00:00:00+03:00",
            "transformer_station_id": "560939638acb503d686f5aa2",
            "detail_type": 3
          }
        }
      ]
    }';

    const TRANSFORMER_STATION_BALANCE_30_MIN = '{
      "reports": [
        {
          "report_code": "transformer_station_balance_v1",
          "project_id": "5603a5db8acb504eb839a28f",
          "id": "56a8c18f8acb50063c74c318",
          "settings": {
            "detail_type_description": "по 30 минут",
            "title": "Новый отчет от 27.01.2016 12:07:14",
            "date_end": "2016-01-22T00:00:00+03:00",
            "date_start": "2016-01-20T00:00:00+03:00",
            "transformer_station_id": "560939638acb503d686f5aa2",
            "detail_type": 1
          }
        }
      ]
    }';

    const PESC_CORPORATE_XML_80020 = '{
      "reports": [
        {
          "report_code": "pesc_corporate_xml_80020_v1",
          "project_id": "55b09a788acb5077cbeb4d83",
          "id": "56aef7038acb5051811c49fa",
          "settings": {
            "date": "2016-01-01T00:00:00+03:00",
            "interval_description": "30 минут",
            "detail_type": 1,
            "title": "Новый отчет от 01.02.2016 09:11:03"
          }
        }
      ]
    }';

    const PESC_CORPORATE_XML_80020_60 = '{
      "reports": [
        {
          "report_code": "pesc_corporate_xml_80020_v1",
          "project_id": "55b09a788acb5077cbeb4d83",
          "id": "56aef7038acb5051811c49fa",
          "settings": {
            "date": "2016-01-01T00:00:00+03:00",
            "interval_description": "60 минут",
            "detail_type": 2,
            "title": "Новый отчет от 01.02.2016 09:11:03"
          }
        }
      ]
    }';

    const RKS_ENERGO_CORPORATE_USAGE = '{
      "reports": [
        {
          "report_code": "rks_energo_corporate_usage_v1",
          "project_id": "55b09a788acb5077cbeb4d83",
          "id": "56aef7038acb5051811c49fa",
          "settings": {
            "date": "2016-01-01T00:00:00+03:00",
            "title": "Новый отчет от 01.02.2016 09:11:03"
          }
        }
      ]
    }';
}