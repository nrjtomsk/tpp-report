<?php

namespace tests\Functional;

use AppBundle\Entity\ReportFile;
use AppBundle\Entity\ReportRequest;
use Tests\ApiTestCase;
use Tests\Functional\Controller\ReportsJsonList;

class ReportGenerationTest extends ApiTestCase
{
    /**
     * @dataProvider reportTypeProvider
     * @param string $type
     */
    public function testReportGenerates(string $type)
    {
        $report = $this->createReportRequest($type);
        $this->attachFiles($report);
        $this->setStatusHasData($report);

        $output = trim($this->runCommand('app:reports:generate'));
        $this->assertEquals('Processed report requests: 1', $output);
    }

    public function reportTypeProvider(): array
    {
        return [
            [ReportsJsonList::DEVICE_USAGE_JSON],
            [ReportsJsonList::GROUP_CORP_USAGE],
            [ReportsJsonList::GROUP_USAGE_COSTS],
            [ReportsJsonList::GROUP_USAGE],
            [ReportsJsonList::PESC_CORPORATE_USAGE],
            [ReportsJsonList::TRANSFORMER_STATION_BALANCE],
            [ReportsJsonList::PESC_CORPORATE_XML_80020],
            [ReportsJsonList::PESC_CORPORATE_XML_80020_60],
            [ReportsJsonList::RKS_ENERGO_CORPORATE_USAGE],
        ];
    }

    /**
     * @param ReportRequest $reportRequest
     */
    protected function attachFiles(ReportRequest $reportRequest)
    {
        $file = $this->createFile($reportRequest);

        $reportFile = new ReportFile();
        $reportFile->setUploadedFile($file);
        $reportFile->setReportRequest($reportRequest);
        $this->em->persist($reportFile);
        $this->em->flush();
    }

    /**
     * @param ReportRequest $reportRequest
     */
    protected function setStatusHasData(ReportRequest $reportRequest)
    {
        $reportRequest->setStatus(ReportRequest::STATUS_HAS_DATA);
        $this->em->persist($reportRequest);
        $this->em->flush();
    }
}