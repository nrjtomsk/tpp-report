<?php

namespace Tests\Unit;

use AppBundle\Entity\ReportRequest;
use AppBundle\Model\BaseReportSettingsInterface;
use AppBundle\Service\ReportRequestManager;
use AppBundle\Service\ReportSettingsModelFactory;
use Prophecy\Argument;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Tests\AppTestCase;

class ReportCreationTest extends AppTestCase
{
    public function testReportRequestCreation()
    {
        $reportRequestManage = $this->createReportRequestManager();

        $reportRequest = $reportRequestManage->createReportRequest(
            'device_usage_v1',
            '567beada8acb504d145caef5',
            []
        );

        $this->assertTrue($reportRequest instanceof ReportRequest);
    }

    /**
     * @expectedException \AppBundle\Exceptions\ValidationException
     */
    public function testReportRequestCreationValidationException()
    {
        $reportRequestManage = $this->createReportRequestManagerWithValidationErrors();

        $reportRequestManage->createReportRequest(
            'device_usage_v1',
            '567beada8acb504d145caef5',
            []
        );
    }

    protected function createReportRequestManager(): ReportRequestManager
    {
        $objectManagerStub = $this->createObjectManagerStub()->reveal();
        $validatorStub = $this->createValidationStubWithErrorsCount()->reveal();
        $settingsFactoryStub = $this->createReportSettingsFactoryStub();
        $routerStub = $this->createRouterStub();

        return new ReportRequestManager(
            $objectManagerStub,
            $validatorStub,
            $settingsFactoryStub,
            $routerStub
        );
    }

    protected function createReportRequestManagerWithValidationErrors(): ReportRequestManager
    {
        $objectManagerStub = $this->createObjectManagerStub()->reveal();
        $validatorStub = $this->createValidationStubWithErrorsCount(1)->reveal();
        $settingsFactoryStub = $this->createReportSettingsFactoryStub();
        $routerStub = $this->createRouterStub();

        return new ReportRequestManager(
            $objectManagerStub,
            $validatorStub,
            $settingsFactoryStub,
            $routerStub
        );
    }

    protected function createReportSettingsFactoryStub(): ReportSettingsModelFactory
    {
        $reportSettingsModelFactory = $this->prophesize(ReportSettingsModelFactory::class);
        $reportSettingsModelFactory
            ->create(Argument::any(), Argument::any())
            ->willReturn(
                $this->prophesize(BaseReportSettingsInterface::class)->reveal()
            );

        return $reportSettingsModelFactory->reveal();
    }

    protected function createRouterStub(): RouterInterface
    {
        return $this->prophesize(RouterInterface::class)->reveal();
    }

}