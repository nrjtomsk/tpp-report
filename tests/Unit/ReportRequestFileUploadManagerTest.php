<?php

namespace Tests\Unit;

use AppBundle\Entity\ReportRequest;
use AppBundle\Message\ReportMessage;
use AppBundle\Service\Proxy\RabbitMQProxyInterface;
use AppBundle\Service\ReportRequestFileUploadManager;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\HttpFoundation\File\File;
use Tests\AppTestCase;

class ReportRequestFileUploadManagerTest extends AppTestCase
{
    public function testFileUploadForReportRequest()
    {
        $fileUploadManager = $this->createManager();
        $reportRequest = $this->createReportRequest();
        $uploadedFile = $this->createUploadedFile()->reveal();

        $reportRequest->setStatus(Argument::any())->shouldBeCalled();

        $result = $fileUploadManager->handleUpload(
            $reportRequest->reveal(),
            $uploadedFile
        );

        $this->assertTrue($result);
    }

    /**
     * @expectedException \AppBundle\Exceptions\ValidationException
     */
    public function testFileUploadForReportRequestWithException()
    {
        $fileUploadManager = $this->createManagerWithValidationException();
        $reportRequest = $this->createReportRequest();
        $uploadedFile = $this->createUploadedFile()->reveal();

        $reportRequest->setStatus(Argument::any())->shouldNotBeCalled();

        $fileUploadManager->handleUpload(
            $reportRequest->reveal(),
            $uploadedFile
        );
    }

    /**
     * @return ReportRequestFileUploadManager
     */
    protected function createManager(): ReportRequestFileUploadManager
    {
        $objectManager = $this->createObjectManagerStub()->reveal();
        $validator = $this->createValidationStubWithErrorsCount()->reveal();
        $rabbitMq = $this->createRabbitMq()->reveal();

        return new ReportRequestFileUploadManager(
            $objectManager,
            $validator,
            $rabbitMq
        );
    }

    /**
     * @return ReportRequestFileUploadManager
     */
    protected function createManagerWithValidationException(): ReportRequestFileUploadManager
    {
        $objectManager = $this->createObjectManagerStub()->reveal();
        $validator = $this->createValidationStubWithErrorsCount(1)
            ->reveal();
        $rabbitMq = $this->createRabbitMqNoCalls()->reveal();

        return new ReportRequestFileUploadManager(
            $objectManager,
            $validator,
            $rabbitMq
        );
    }

    /**
     * @return ObjectProphecy
     */
    protected function createReportRequest(): ObjectProphecy
    {
        $report = $this->prophesize(ReportRequest::class);

        $report->getId()->willReturn(1);
        $report->getStatus()->willReturn(ReportRequest::STATUS_HAS_DATA);
        $report->getDataFile()->willReturn(null);
        $report->setStatus(Argument::any())->shouldBeCalled();

        return $report;
    }

    /**
     * @return ObjectProphecy
     */
    protected function createUploadedFile(): ObjectProphecy
    {
        return $this->prophesize(File::class);
    }

    /**
     * @return ObjectProphecy
     */
    protected function createRabbitMq(): ObjectProphecy
    {
        $publisher = $this->prophesize(RabbitMQProxyInterface::class);

        $publisher->publish(Argument::type(ReportMessage::class))
            ->shouldBeCalled();

        return $publisher;
    }

    /**
     * @return ObjectProphecy
     */
    protected function createRabbitMqNoCalls(): ObjectProphecy
    {
        $publisher = $this->prophesize(RabbitMQProxyInterface::class);

        $publisher->publish(Argument::type(ReportMessage::class))
            ->shouldNotBeCalled();

        return $publisher;
    }
}
