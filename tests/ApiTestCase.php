<?php

namespace Tests;

use AppBundle\Entity\ReportRequest;
use Doctrine\Common\Persistence\ObjectManager;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiTestCase extends WebTestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var ObjectManager
     */
    protected $em;

    public function setUp()
    {
        parent::setUp();

        $this->client = static::createClient();
        $this->em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $this->loadFixtures([]);
    }

    /**
     * @param       $path
     * @param array $query
     *
     * @return null|Response
     */
    protected function requestGet($path, array $query = [])
    {
        $this->client->request(Request::METHOD_GET, $path, $query);

        return $this->client->getResponse();
    }

    /**
     * @param       $path
     * @param array $parameters
     *
     * @return null|Response
     */
    protected function requestPost($path, array $parameters = [], array $files = [])
    {
        $this->client->request(Request::METHOD_POST, $path, $parameters, $files);

        return $this->client->getResponse();
    }

    /**
     * @param $path
     * @param $json
     *
     * @return null|Response
     */
    protected function jsonRequestPost($path, $json)
    {
        $this->client->request(
            Request::METHOD_POST,
            $path,
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'CONTENT_LENGTH' => mb_strlen($json)
            ],
            $json
        );

        return $this->client->getResponse();
    }

    /**
     * @param Response $response
     * @param          $code
     */
    protected function assertStatusCodeIs(Response $response, $code)
    {
        $this->assertEquals(
            $code,
            $response->getStatusCode(),
            sprintf('Expecting status code "%s", but "%s" given', $code, $response->getStatusCode())
        );
    }

    /**
     * @param Response $response
     */
    protected function assertJsonResponse(Response $response)
    {
        $this->assertTrue(
            $response->headers->contains('Content-Type', 'application/json'),
            sprintf(
                'Expecting Content-Type to be "application/json", but "%s" given',
                $response->headers->get('Content-Type', '<null>')
            )
        );
    }

    /**
     * @param $data
     *
     * @return ReportRequest
     *
     * @throws \Exception
     */
    protected function createReportRequest($data)
    {
        $reportData = json_decode($data, true)['reports'][0];

        $report = $this->getContainer()->get('app.manager.report_request')->createReportRequest(
            $reportData['report_code'],
            $reportData['project_id'],
            $reportData['id'],
            $reportData['settings']
        );

        return $report;
    }

    /**
     * @param ReportRequest $request
     * @return UploadedFile
     */
    protected function createFile(ReportRequest $request = null)
    {
        $fileName = 'data.tar.gz';
        $copiedFileName = 'copy_data.tar.gz';
        $dataDir = __DIR__ . '/data';

        $fileTypes = [
            ReportRequest::CODE_DEVICE_USAGE => 'device_usage.tar.gz',
            ReportRequest::CODE_GROUP_CORPORATE_USAGE => 'group_corp_usage.tar.gz',
            ReportRequest::CODE_GROUP_USAGE => 'group_usage.tar.gz',
            ReportRequest::CODE_GROUP_USAGE_COSTS => 'group_usage_costs.tar.gz',
            ReportRequest::CODE_PESC_CORPORATE_USAGE => 'pesc_corporate_usage.tar.gz',
            ReportRequest::CODE_PESC_CORPORATE_XML => 'pesc_corporate_xml.tar.gz',
            ReportRequest::CODE_RKS_ENERGO_CORPORATE_USAGE => 'pesc_corporate_usage.tar.gz',
            ReportRequest::CODE_TRANSFORM_STATION_BALANCE => 'transformer_station_balance.tar.gz',
        ];

        if ($request !== null && array_key_exists($request->getCode(), $fileTypes)) {
            $fileName = $fileTypes[$request->getCode()];
        }

        $dataFile = $dataDir . '/' . $fileName;
        $fileCopy = $dataDir . '/' . $copiedFileName;

        copy($dataFile, $fileCopy);

        $file = new UploadedFile(
            $fileCopy,
            $copiedFileName,
            null,
            null,
            null,
            $isTest = true
        );

        return $file;
    }

    protected function assertReportRequestCreationIsOk(Response $response)
    {
        $this->assertStatusCodeIs($response, Response::HTTP_OK);
        $this->assertJsonResponse($response);

        $data = json_decode($response->getContent(), true);

        $this->assertTrue(key_exists('reports', $data));
        $this->assertTrue(key_exists(0, $data['reports']));

        $report = $data['reports'][0];

        $this->assertTrue(key_exists('status', $report));
        $this->assertEquals(ReportRequest::STATUS_WAITING_DATA, $report['status']);
        $this->assertTrue(key_exists('id', $report));
        $this->assertInternalType('integer', $report['id']);
    }
}