<?php

namespace Tests;

use Doctrine\Common\Persistence\ObjectManager;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AppTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * @return ObjectProphecy
     */
    protected function createObjectManagerStub(): ObjectProphecy
    {
        return $this->prophesize(ObjectManager::class);
    }

    /**
     * @param int $errorsCount
     *
     * @return ObjectProphecy
     */
    protected function createValidationStubWithErrorsCount(int $errorsCount = 0): ObjectProphecy
    {
        $constraint = $this->prophesize(ConstraintViolationListInterface::class);
        $constraint->count()->willReturn($errorsCount);

        $validator = $this->prophesize(ValidatorInterface::class);
        $validator->validate(Argument::any())->willReturn($constraint->reveal());

        return $validator;
    }
}