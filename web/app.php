<?php

use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;

$loader = require __DIR__.'/../app/autoload.php';
require_once __DIR__.'/../app/ApiKernel.php';

$env = getenv('SYMFONY_ENV') ?: 'prod';
$debug = $env === 'dev';

$app = new ApiKernel($env, $debug);
if ($debug) {
    Debug::enable();
}
$app->loadClassCache();
$response = $app->handle(Request::createFromGlobals());

$response->send();
